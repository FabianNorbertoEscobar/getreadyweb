import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { MaterialModule } from '../shared/material.module';
import { HttpClientModule } from '@angular/common/http';
import { GetReadyRoutingModule } from './get-ready-routing.module';

@NgModule({
  declarations: [],
  imports: [
    GetReadyRoutingModule,
    CommonModule,
    MaterialModule,
    HttpClientModule,
  ],
  providers: [],
})
export class GetReadyModule {}