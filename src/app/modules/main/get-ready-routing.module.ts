import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('../module-home/home.module').then(m => m.HomeModule),
  },
  {
    path: 'profile',
    loadChildren: () => import('../module-profile/profile.module').then(m => m.ProfileModule)
  },
  {
    path: 'organization',
    loadChildren: () => import('../module-organization/organization.module').then(m => m.OrganizationModule)
  },
  {
    path: 'manage-users',
    loadChildren: () => import('../module-manage-users/manage-users.module').then(m => m.ManageUsersModule),
  },
  {
    path: 'competencias',
    loadChildren: () => import('../module-competencias/competencias.module').then(m => m.CompetenciasModule),
  },
  {
    path: 'courses',
    loadChildren: () => import('../module-courses/courses.module').then(m => m.CoursesModule),
  },
  {
    path: 'games',
    loadChildren: () => import('../module-games/games.module').then(m => m.GamesModule),
  },
  {
    path: 'achievements',
    loadChildren: () => import('../module-achievements/achievements.module').then(m => m.AchievementsModule),
  },
  {
    path: 'appraisals',
    loadChildren: () => import('../module-appraisals/appraisals.module').then(m => m.AppraisalsModule),
  },
  {
    path: 'development',
    loadChildren: () => import('../module-development/development.module').then(m => m.DevelopmentModule),
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GetReadyRoutingModule { }
