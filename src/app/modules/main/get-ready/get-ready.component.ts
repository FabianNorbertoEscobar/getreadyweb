import { Component, OnInit } from '@angular/core';
import { OverlayService } from '../../../services/overlay.service';
import { environment } from '../../../../environments/environment';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import { HeaderService } from 'src/app/services/header/header.service';
import { AchievementsSharedService } from 'src/app/services/achievements-shared.service';

@Component({
  selector: 'app-get-ready',
  templateUrl: './get-ready.component.html',
  styleUrls: ['./get-ready.component.scss'],
})
export class GetReadyComponent implements OnInit {

  public environment = environment;
  public isMobile: boolean = false;
  public title: string;

  constructor(
    private overlayService: OverlayService,
    private breakpointObserver: BreakpointObserver,
    private headerService: HeaderService,
    private achievementsSharedService: AchievementsSharedService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.breakpointObserver.observe(['(max-width: 599px)']).subscribe((state: BreakpointState) => {
      this.isMobile = state.matches;
    });

    this.headerService.currentTitle.subscribe(currentTitle => {
      this.title = currentTitle;
    });
  }

  testShit() {
    // this.overlayService.displayLoadingOverlay();
    // this.router.navigate(['./games/new-game/trivia']);
    this.overlayService.displayAchievement(this.achievementsSharedService.achievements.value[0]);
  }
}
