
export class ProfileImageDTO {
  id: number;
  displayname: string;
  base64Image: string;
}
