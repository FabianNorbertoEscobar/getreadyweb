import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LevelsService } from 'src/app/services/levels.service';
import { ProfileService } from 'src/app/services/profile.service';
import { UserService } from 'src/app/services/user.service';

@Injectable()
export class UserProfileResolver implements Resolve<any> {

    constructor(
        private levelsService: LevelsService,
        private profileService: ProfileService,
        private userService: UserService,
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let requests: Observable<any>[] = [
            this.userService.getUserById(route.queryParams.id),
            this.profileService.getProfileById(route.queryParams.id),
            this.levelsService.getUserLevel(route.queryParams.id),
        ];

        return forkJoin(requests).pipe(
            map(result => ({
                user: result[0],
                userProfile: result[1],
                userGetReady: result[2],
            })),
        );
    }
}