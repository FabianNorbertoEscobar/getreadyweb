import { Component, OnInit, Input } from '@angular/core';
import { ProfileData } from 'src/app/modules/module-profile/ProfileData';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import { SessionService } from '../../../services/session.service';
import { User } from '../../../services/user';
import { ProfileService } from '../../../services/profile.service';
import { Location } from '@angular/common';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-user-form',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})

export class EditProfileComponent implements OnInit {
  @Input()
  public imagePath;
  profile: ProfileData;
  levelProgressPercentage = 0;
  mouseOverProfilePicture = false;
  mouseOverProgressBar = false;
  selectedFile: File = null;
  imgFile;
  userForm;
  imgUrl: any;
  private user: User;
  private errorMessage = new BehaviorSubject<string>('');
  public errorMessage$ = this.errorMessage.asObservable();
  private imgBase64: string | ArrayBuffer;

  constructor(
    private formBuilder: FormBuilder,
    private location: Location,
    private sessionService: SessionService,
    private profileService: ProfileService,
  ) {
    this.user = this.sessionService.getCurrentUser();
    this.profileService.getProfileById(this.user.id)
      .subscribe(
        data =>
        {
          this.profile = data;
          this.userForm.get('id').setValue(this.profile.id);
          this.userForm.get('username').setValue(this.profile.username);
          this.userForm.get('displayname').setValue(this.profile.displayname);
          this.userForm.get('description').setValue(this.profile.description);
          this.userForm.get('address').setValue(this.profile.address);
          this.userForm.get('email').setValue(this.profile.email);
          this.userForm.get('phone').setValue(this.profile.phone);
          if (this.profile.image != null){
            this.imgBase64 = this.profile.image;
            this.imgUrl = this.profile.image;
          }
          this.userForm.enable();
        }
      );

    this.imgUrl = '../../../../assets/empty-avatar.png';

  }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      id: '',
      username: '',
      displayname: [ '', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(50) ]
      ],
      description: ['', [
        Validators.maxLength(200) ]
      ],
      address: [ '', [
        Validators.maxLength(300) ]
      ],
      email: [ '', [
        Validators.required,
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'),
        Validators.maxLength(100)]
      ],
      phone: ['', [
        Validators.pattern('^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\./0-9]*$'),
        Validators.maxLength(50)]
      ],
      image: '',
    });
    this.userForm.disable();
  }

  onFileSelected(event) {
    if (event.target.files && event.target.files[0]) {
      // Guardo la imagen
      this.selectedFile = (event.target.files[0] as File);
      // Muestro la imagen
      const reader = new FileReader();
      reader.onload = (loadEvent: any) => {
        this.imgBase64 = reader.result;
        this.imgUrl = loadEvent.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  done() {
    if (this.selectedFile != null){
      this.userForm.controls.image.setValue(this.imgBase64);
    }

    const obs = this.profileService.updateProfile(this.userForm.value);
    obs.subscribe(response => {
      if (response.ok) {
        this.errorMessage.next('');
        this.location.back();
      } else {
        this.errorMessage.next(response.message);
      }
    });
  }
}
