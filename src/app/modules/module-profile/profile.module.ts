import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { SharedAchievementsModule } from '../shared/shared-achievements.module';
import { SharedCoursesModule } from '../shared/shared-courses.module';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ProfileComponent } from './profile/profile.component';
import { UserProfileResolver } from './resolvers/user-profile.resolver';
import { ProfileService } from 'src/app/services/profile.service';
import { UserService } from 'src/app/services/user.service';
import { LevelsService } from 'src/app/services/levels.service';
import { SharedPraiseModule } from '../shared/shared-praise.module';
import { AchievementsService } from '../module-achievements/services/achievements.service';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    resolve: { pageData: UserProfileResolver},
  },
  {
    path: 'edit',
    component: EditProfileComponent,
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    SharedAchievementsModule,
    SharedCoursesModule,
    ReactiveFormsModule,
    FormsModule,
    SharedPraiseModule,
    SharedCoursesModule,
  ],
  declarations: [
    EditProfileComponent,
    ProfileComponent,
  ],
  providers: [
    UserProfileResolver,
    ProfileService,
    UserService,
    LevelsService,
    AchievementsService,
  ],
})
export class ProfileModule {}
