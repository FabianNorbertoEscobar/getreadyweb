import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserGetReady, UserProfile } from 'src/app/interfaces/profile.interface';
import { UsuarioFromDB } from 'src/app/interfaces/usuario-db.interface';
import { CoursesService } from 'src/app/services/courses.service';
import { HeaderService } from 'src/app/services/header/header.service';
import { ProfileService } from 'src/app/services/profile.service';
import { AchievementsService } from '../../module-achievements/services/achievements.service';
import { Achievement } from '../../module-achievements/utils/achievement.utils';
import { DatabaseCourse } from '../../../services/courses.service';
import { Course } from 'src/app/interfaces/course.interface';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, AfterViewInit {

  user: UsuarioFromDB;
  userProfile: UserProfile;
  userGetReady: UserGetReady;
  imgSource: string;

  achievements: Achievement[];
  achievementPoints: number;

  levelProgressPercentage: number = 0;
  mouseOverProfilePicture: boolean = false;

  courses: Course[] = [];
  
  fetchingAchievements: boolean = true;
  fetchingCourses: boolean = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private headerService: HeaderService,
    private profileService: ProfileService,
    private achievementsService: AchievementsService,
    private coursesService: CoursesService,
    private router: Router,
  ) {
    this.user = this.activatedRoute.snapshot.data.pageData.user;
    this.userProfile = this.activatedRoute.snapshot.data.pageData.userProfile;
    this.userGetReady = this.activatedRoute.snapshot.data.pageData.userGetReady;
  }

  ngOnInit(): void {
    // Busco imagen de perfil
    this.profileService.getProfileImageById(this.user.id).subscribe(data => {
      if (data != undefined && data.base64Image != null) {
        this.imgSource = data.base64Image;
      } else {
          this.imgSource = '../../../../assets/empty-avatar.png'
      }
    });

    // Busco logros del usuario
    this.achievementsService.getAchievementsByUserId(this.user.id).subscribe((achievements: Achievement[]) => {
      this.achievements = achievements;
      this.achievementPoints = 0;
      for (let achievement of achievements) {
        this.achievementPoints += achievement.points;
      }

      this.fetchingAchievements = false;
    });

    // Busco cursos del usuario
    this.coursesService.findByUser(this.user.id).subscribe((courses: DatabaseCourse[]) => {
      for (let course of courses) {
        if (course.progreso > 0) {
          this.courses.push( {
            id: course.id,
            title: course.nombre,
            progress: course.progreso,
            description: course.descripcion,
            idSector: course.idSector,
            rated: course.dioFeedback,
          });
        }
      }

      this.fetchingCourses = false;
    });
  }

  ngAfterViewInit(): void {
    this.headerService.setDescriptionForRoute(`Perfil: ${this.userProfile.displayname ? this.userProfile.displayname : this.user.username}`);
  }

  /**
   * Verifica si el usuario tiene un nivel
   */
  userHasLevel(): boolean {
    return this.userGetReady.nivel != undefined;
  }

  /**
   * Edita el perfil del usuario
   */
  editProfile() {
    this.router.navigate(['profile/edit'], { queryParams: { userId: this.user.id } });
  }

}
