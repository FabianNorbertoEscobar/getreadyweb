
/**
 * Esta interfaz representa los datos del perfil de un usuario
 */
export class ProfileData {
  id: number;
  username: string;
  displayname: string;
  description: string;
  address: string;
  email: string;
  phone: string;
  image: string;
}
