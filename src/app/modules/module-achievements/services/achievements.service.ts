import { DatePipe } from '@angular/common';
import { Injectable, Injector } from "@angular/core";
import { Observable, of } from 'rxjs';
import { Achievement, CustomAchievement } from 'src/app/modules/module-achievements/utils/achievement.utils';
import { HttpService } from 'src/app/services/http.service';
import { SessionService } from 'src/app/services/session.service';

export interface GrantedAchievement {
    achievementId: number;
    userId: number;
    date: string;
}

@Injectable()
export class AchievementsService {

    constructor(
        private httpService: HttpService,
        private sessionService: SessionService,
        private datePipe: DatePipe,
    ) {}

    /**
     * Obtiene el listado de logros
     */
    getAchievements(): Observable<Achievement[]> {
        return this.httpService.get('achievement');
    }

    /**
     * Obtiene el listado de logros del usuario
     * `TODO`: Reemplazar esto por el servicio `getAchievementsByUserId`
     */
    getUserAchievements(): Observable<Achievement[]> {
        return this.httpService.get(`achievement/user/${this.sessionService.getCurrentUser().id}`);
    }

    /**
     * Obtiene el listado de logros de un usuario
     * @param id el id del usuario
     */
    getAchievementsByUserId(id: number): Observable<Achievement[]> {
        return this.httpService.get(`achievement/user/${id}`);
    }

    /**
     * Otorga un logro al usuario
     * @param achievementId el id del logro
     */
    grantAchievementToUser(achievementId: number): Observable<any> {
        let grantedAchievement: GrantedAchievement = {
            achievementId: achievementId,
            userId: this.sessionService.getCurrentUser().id,
            date: this.datePipe.transform(new Date(), "yyyy-MM-dd"),
        };

        return this.httpService.post(`achievement/user/`, grantedAchievement);
    }

    /**
     * Crea un logro custom
     * @param customAchievement el logro custom
     */
    createCustomAchievement(customAchievement: CustomAchievement): Observable<Achievement> {
        return this.httpService.post(`achievement`, customAchievement);
    }

    /**
     * Actualiza un logro custom
     * @param achievementId el id del logro
     * @param achievement el logro custom
     */
    updateCustomAchievement(achievementId: number, achievement: Achievement) {
        return this.httpService.put(`achievement/${achievementId}`, achievement);
    }

    /**
     * Elimina un logro custom
     * @param achievementId el id del logro
     */
    deleteCustomAchievement(achievementId: number) {
        return this.httpService.delete(`achievement/${achievementId}`);
    }
}

// const ACHIEVEMENTS_MOCK: Achievement[] = [
//     {
//         id: 1,
//         title: 'Erudito',
//         description: 'Termina un curso',
//         rarity: 'uncommon',
//         points: 15,
//         category: 'courses',
//         custom: false,
//     },
//     {
//         id: 2,
//         title: 'Knock, knock...',
//         description: 'Pídele un chiste al chatbot',
//         rarity: 'rare',
//         points: 20,
//         category: 'chatbot',
//         custom: false,
//     },
//     {
//         id: 3,
//         title: 'Jugador profesional',
//         description: 'Juega todos los juegos de la plataforma',
//         rarity: 'epic',
//         points: 25,
//         category: 'games',
//         custom: false,
//     },
//     {
//         id: 4,
//         title: 'Dios',
//         description: 'Sube al nivel 10',
//         rarity: 'legendary',
//         points: 30,
//         category: 'general',
//         custom: false,
//     },
//     {
//         id: 5,
//         title: 'Noob',
//         description: 'Juega un juego de la plataforma',
//         rarity: 'uncommon',
//         points: 15,
//         category: 'games',
//         custom: false,
//     },
//     {
//         id: 6,
//         title: 'Haciendo amigos',
//         description: 'Envíale un mensaje al chatbot',
//         rarity: 'common',
//         points: 10,
//         category: 'chatbot',
//         custom: false,
//     },
//     {
//         id: 7,
//         title: '¿Quién apagó la luz?',
//         description: 'Prueba el modo oscuro',
//         rarity: 'common',
//         points: 10,
//         category: 'general',
//         custom: false,
//     }
// ];