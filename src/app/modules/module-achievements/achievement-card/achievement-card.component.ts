import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Achievement, AchievementRarity } from 'src/app/modules/module-achievements/utils/achievement.utils';

@Component({
  selector: 'app-achievement-card',
  templateUrl: './achievement-card.component.html',
  styleUrls: ['./achievement-card.component.scss']
})
export class AchievementCardComponent implements OnInit {

  @Input()
  achievement: Achievement;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Devuelve el color de la tarjeta
   */
  getColor(): string {
    return this.achievement.custom ? 'accent' : 'primary';
  }
}
