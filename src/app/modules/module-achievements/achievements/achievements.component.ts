import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { DialogConfirmationComponent } from 'src/app/components/dialog/dialog-confirmation/dialog-confirmation.component';
import { Achievement } from 'src/app/modules/module-achievements/utils/achievement.utils';
import { AchievementsSharedService } from 'src/app/services/achievements-shared.service';
import { OverlayService } from 'src/app/services/overlay.service';
import { SessionService } from 'src/app/services/session.service';
import { AchievementsService } from '../services/achievements.service';

@Component({
  selector: 'app-achievements',
  templateUrl: './achievements.component.html',
  styleUrls: ['./achievements.component.scss']
})
export class AchievementsComponent implements OnInit {

  categoryFilter: string = "all";
  achievements: Achievement[] = [];
  userAchievements: Achievement[] = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private achievementsService: AchievementsService,
    public achievementsSharedService: AchievementsSharedService,
    public sessionService: SessionService,
    private matDialog: MatDialog,
    private overlayService: OverlayService,
  ) {}

  ngOnInit(): void {
    this._setAllAchievements();

    this.achievementsSharedService.achievements.subscribe(achievements => {
      this._setAchievementsByFilter();
    });
    this.achievementsSharedService.userAchievements.subscribe(userAchievements => {
      this._setAchievementsByFilter();
    });
  }

  /**
   * Escucha los cambios en el filtro de categoría
   * @param selectedCategory la categoría seleccionada
   */
  onCategoryFilterChange(selectedCategory: string): void {
    this.categoryFilter = selectedCategory;
    this._setAchievementsByFilter();
  }

  /**
   * Retorna `true` o `false` según si el logro especificado ya fue obtenido por el usuario
   * @param achievement el logro
   */
  achievedByUser(achievement: Achievement): boolean {
    return this.userAchievements.findIndex(userAchievement => userAchievement.id == achievement.id) >= 0;
  }
  
  /**
   * Abre el formulario para crear un logro
   */
  addCustomAchievement(): void {
    this.router.navigate(['custom-achievement'], { relativeTo: this.activatedRoute });
  }

  /**
   * Abre el formulario para editar un logro
   * @param achievement el logro
   */
  editAchievement(achievement: Achievement): void {
    this.router.navigate(['custom-achievement', achievement.id], { relativeTo: this.activatedRoute });
  }

  /**
   * Elimina un logro
   * @param achievement el logro
   */
  deleteAchievement(achievement: Achievement): void {
    const dialogRef = this.matDialog.open(DialogConfirmationComponent, {
      width: "650px",
      data: {
        title: "Borrar logro",
        description: `Se borrará el logro ${achievement.title} ¿Desea continuar?`,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.overlayService.displayLoadingOverlay();
        this.achievementsService.deleteCustomAchievement(achievement.id).subscribe(result => {
          forkJoin([this.achievementsSharedService.fetchAchievements(), this.achievementsSharedService.fetchUserAchievements()]).subscribe(done => {
            this.overlayService.hideLoadingOverlay();
          })
        });
      }
    });
  }

  /**
   * Setea todos los logros en los array de logros
   */
  private _setAllAchievements(): void {
    this.achievements = this.achievementsSharedService.achievements.value;
    this.userAchievements = this.achievementsSharedService.userAchievements.value;
  }

  /**
   * Setea los logros por categoría
   * @param category la categoría
   */
  private _setAchievementsByFilter(): void {
    if (this.categoryFilter == "all") {
      this._setAllAchievements();
    } else {
      this.achievements = this.achievementsSharedService.achievements.value.filter(achievement => achievement.category == this.categoryFilter);
      this.userAchievements = this.achievementsSharedService.userAchievements.value.filter(achievement => achievement.category == this.categoryFilter);
    }
  }
}
