export type AchievementRarity = 'common' | 'uncommon' | 'rare' | 'epic' | 'legendary';

export type AchievementCategory = 'courses' | 'games' | 'appraisals' | 'general' | 'chatbot';

export interface Achievement {
    id: number;
    title: string;
    description: string;
    rarity: AchievementRarity;
    points: number;
    category: AchievementCategory;
    date?: Date; // La fecha es opcional porque puede no haberse obtenido el logro
    custom?: boolean; // Indica si es un logro custom, es decir, creado por un administrador
}

export interface CustomAchievement {
    title: string;
    description: string;
    rarity: AchievementRarity;
    points: number;
    category: AchievementCategory;
    custom: boolean;
}

/** Cuantos puntos da un logro en base a su rareza */
export const POINTS_BY_RARITY: {[key: string]: number} = {
    "common": 10,
    "uncommon": 15,
    "rare": 20,
    "epic": 25,
    "legendary": 30,
}

/** Devuelve cuantos puntos vale un logro de acuerdo a su rareza
 * @param rarity la rareza del logro
 */
export function getPointsByRarity(rarity: AchievementRarity): number {
    return POINTS_BY_RARITY[rarity];
}