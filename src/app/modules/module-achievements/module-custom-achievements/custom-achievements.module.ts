import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CoursesService } from 'src/app/services/courses.service';
import { GamesService } from 'src/app/services/games.service';
import { SharedModule } from '../../shared/shared.module';
import { CustomAchievementFormComponent } from './custom-achievement-form/custom-achievement-form.component';

const routes: Routes = [
    {
        path: '',
        component: CustomAchievementFormComponent,
    },
    {
        path: ':id',
        component: CustomAchievementFormComponent,
    }
]

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        ReactiveFormsModule,
    ],
    declarations: [CustomAchievementFormComponent],
    providers: [
        GamesService,
        CoursesService,
    ]
})
export class CustomAchievementsModule {}