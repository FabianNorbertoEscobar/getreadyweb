import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AchievementsSharedService } from 'src/app/services/achievements-shared.service';
import { CoursesService } from 'src/app/services/courses.service';
import { GamesService } from 'src/app/services/games.service';
import { OverlayService } from 'src/app/services/overlay.service';
import { SessionService } from 'src/app/services/session.service';
import { AchievementsService } from '../../services/achievements.service';
import { Achievement, AchievementCategory, AchievementRarity, CustomAchievement, getPointsByRarity } from '../../utils/achievement.utils';

export interface RarityOption {
  value: AchievementRarity;
  displayValue: string;
}

export interface CategoryOption {
  value: AchievementCategory;
  displayValue: string;
}

@Component({
  selector: 'app-custom-achievement-form',
  templateUrl: './custom-achievement-form.component.html',
  styleUrls: ['./custom-achievement-form.component.scss']
})
export class CustomAchievementFormComponent implements OnInit {

  rarityOptions: RarityOption[] = [
    {
      value: "common",
      displayValue: "Común",
    },
    {
      value: "uncommon",
      displayValue: "Poco común",
    },
    {
      value: "rare",
      displayValue: "Raro",
    },
    {
      value: "epic",
      displayValue: "Épico",
    },
    {
      value: "legendary",
      displayValue: "Legendario",
    },
  ];

  categoryOptions: CategoryOption[] = [
    {
      value: 'courses',
      displayValue: "Cursos",
    },
    {
      value: 'games',
      displayValue: "Juegos",
    },
  ]

  customAchievementGroup: FormGroup;
  titleOptions: string[] = [];
  achievementId: string;
  achievement: Achievement;

  constructor(
    private achievementsService: AchievementsService,
    private achievementsSharedService: AchievementsSharedService,
    private gamesService: GamesService,
    private coursesService: CoursesService,
    private sessionService: SessionService,
    private overlayService: OverlayService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    this.achievementId = this.activatedRoute.snapshot.paramMap.get('id');

    this.customAchievementGroup = new FormBuilder().group({
      rarity: ['', [ Validators.required ]],
      category: ['', [ Validators.required ]],
      title: ['', [ Validators.required ]],
      description: ['', [ Validators.required ]],
    });
  }

  ngOnInit(): void {
    if (this.achievementId) {
      this.achievement = this.achievementsSharedService.getAchievementById(Number.parseInt(this.achievementId));
      if (!this.achievement) {
        this.router.navigate(['achievements']);
      }
      this._formInit();
    }

    // Cuando se selecciona la categoría, se deja la descripción vacía y se obtiene la lista de títulos posibles
    this.customAchievementGroup.get('category').valueChanges.subscribe((newValue: string) => {
      this.customAchievementGroup.get('description').setValue('');
      this._setAchievementTitleOptions(newValue).subscribe(() => {this.overlayService.hideLoadingOverlay()});
    });

    // Cuando se selecciona el título, se crea una descripción por defecto (en el caso de que sea un logro nuevo)
    this.customAchievementGroup.get('title').valueChanges.subscribe((newValue: string) => {
      if (!this.achievementId) {
        this.customAchievementGroup.get('description').setValue(this._getDefaultDescription());
      }
    });
  }

  /**
   * Guarda un logro custom
   */
  saveAchievement(): void {
    if (this.achievementId) {
      let achievement: Achievement = {
        id: Number.parseInt(this.achievementId),
        title: this.customAchievementGroup.get('title').value,
        description: this.customAchievementGroup.get('description').value,
        rarity: this.customAchievementGroup.get('rarity').value,
        points: getPointsByRarity(this.customAchievementGroup.get('rarity').value),
        category: this.customAchievementGroup.get('category').value,
        custom: true,
      }

      this.overlayService.displayLoadingOverlay();
      this.achievementsService.updateCustomAchievement(achievement.id, achievement).subscribe(result => {
        this.overlayService.hideLoadingOverlay();
        if (result) {
          this.achievementsSharedService.fetchAchievements().subscribe(achievements => {
            this.router.navigate(['achievements']);
          });
        }
      });
    } else {
      let customAchievement: CustomAchievement = {
        title: this.customAchievementGroup.get('title').value,
        description: this.customAchievementGroup.get('description').value,
        rarity: this.customAchievementGroup.get('rarity').value,
        points: getPointsByRarity(this.customAchievementGroup.get('rarity').value),
        category: this.customAchievementGroup.get('category').value,
        custom: true,
      }
  
      this.overlayService.displayLoadingOverlay();
      this.achievementsService.createCustomAchievement(customAchievement).subscribe(result => {
        this.overlayService.hideLoadingOverlay();
        if (result) {
          this.achievementsSharedService.fetchAchievements().subscribe(achievements => {
            this.router.navigate(['achievements']);
          });
        }
      });
    }  
  }

  /**
   * Vuelve a logros, sin guardar los cambios
   */
  cancel(): void {
    this.router.navigate(['achievements']);
  }

  private _getDefaultDescription(): string {
    if (this.customAchievementGroup.get('category').value == 'courses') {
      return `Completa el curso ${this.customAchievementGroup.get('title').value}`;
    }

    return `Juega a ${this.customAchievementGroup.get('title').value}`;
  }

  /**
   * Inicializa el formulario con los valores del logro que se haya pasado por parámetro (edición de un logro ya existente)
   */
  private _formInit(): void {
    this.customAchievementGroup.get('rarity').setValue(this.achievement.rarity);
    this.customAchievementGroup.get('category').setValue(this.achievement.category);
    this.customAchievementGroup.get('description').setValue(this.achievement.description);

    this.overlayService.displayLoadingOverlay();
    this._setAchievementTitleOptions(this.achievement.category).subscribe(() => {
      this.customAchievementGroup.get('title').setValue(this.achievement.title);
      this.overlayService.hideLoadingOverlay()
    });
  }

  /**
   * Dispone la lista de títulos posibles para el logro, en base a la categoría seleccionada
   * @param category la categoría
   */
  private _setAchievementTitleOptions(category: string): Observable<any> {
    if (category == 'courses') {
      return this.coursesService.getCourses().pipe(
        tap((courses) => {
          this.titleOptions = courses.map(course => course.nombre)
        }),
      );
    } else {
      return this.gamesService.getAll(this.sessionService.getCurrentUser().id).pipe(
        tap((games) => {
          this.titleOptions = games.map(game => game.title);
        }),
      );
    }
  }
}
