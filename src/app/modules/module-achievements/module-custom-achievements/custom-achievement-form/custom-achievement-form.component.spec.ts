import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomAchievementFormComponent } from './custom-achievement-form.component';

describe('CustomAchievementFormComponent', () => {
  let component: CustomAchievementFormComponent;
  let fixture: ComponentFixture<CustomAchievementFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomAchievementFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomAchievementFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
