import { NgModule } from "@angular/core";
import { SharedModule } from '../shared/shared.module';
import { AchievementsComponent } from './achievements/achievements.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedAchievementsModule } from '../shared/shared-achievements.module';
import { AchievementsService } from './services/achievements.service';

const routes: Routes = [
    {
        path: '',
        component: AchievementsComponent,
    },
    {
        path: 'custom-achievement',
        loadChildren: () => (import('./module-custom-achievements/custom-achievements.module')).then(m => m.CustomAchievementsModule),
    },
]

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        SharedAchievementsModule,
    ],
    declarations: [
        AchievementsComponent,
    ],
    providers: [
        AchievementsService,
    ]
})
export class AchievementsModule {}