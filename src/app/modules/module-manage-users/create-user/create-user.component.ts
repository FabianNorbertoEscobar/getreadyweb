import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { BehaviorSubject } from 'rxjs';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { SessionService } from 'src/app/services/session.service';
import { SectorService } from 'src/app/services/sector.service';
import { ActivatedRoute, Router } from '@angular/router';

export interface SelectItem {
  value: string;
  description: string;
}

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  private readonly admin_key = "ADMIN";
  private readonly admin_sector_key = "ADMIN_SECTOR";
  private readonly user_key = "USUARIO";
  
  id: number;
  public form: FormGroup;
  public hidePass: boolean = true;
  public rolesList: SelectItem[] = new Array<SelectItem>();
  public sectorList: SelectItem[] = new Array<SelectItem>();
  public aclaracion: string = "";
  public labelButton: string;

  private errorMessage = new BehaviorSubject<string>("");
  public errorMessage$ = this.errorMessage.asObservable();
  
  
  constructor(
    private formBuilder: FormBuilder,
    private location: Location,
    private userService: UserService,
    private authService: AuthService,
    private sessionService: SessionService,
    private sectorService: SectorService,
    private route: ActivatedRoute,
    private router: Router
  ) { 
    this.route.queryParams.subscribe(params => {
      this.id = params['userId'];
    });
    this.initializeRolesList();
    this.initializeSectoresList();
  }

    private getRolByValue(rol: string): SelectItem {
      switch(rol) {
        case this.admin_key: {
          return {
            value: this.admin_key,
            description: "Administrador"
          }
        }
        case this.admin_sector_key: {
          return {
            value: this.admin_sector_key,
            description: "Administrador de sector"
          }
        }
        case this.user_key: {
          return {
            value: this.user_key,
            description: "Usuario"
          }
        }
    }
  }

  private initializeSectoresList(): void {
    let currentUser = this.sessionService.getCurrentUser();
    this.sectorService.findAllByUser(currentUser.id).subscribe(r => {
      for (let sector of r) {
        let selectSector = {
          value: sector.id,
          description: sector.nombre
        }
        this.sectorList.push(selectSector);
      }
    });
  }

  private initializeRolesList(): void {
    let currentUser = this.sessionService.getCurrentUser();
    switch(currentUser.roleType) {
      case this.admin_key: {
        this.rolesList.push(this.getRolByValue(this.admin_key));
        this.rolesList.push(this.getRolByValue(this.admin_sector_key));
        this.rolesList.push(this.getRolByValue(this.user_key));
        break;
      }
      case this.admin_sector_key: {
        this.rolesList.push(this.getRolByValue(this.admin_sector_key));
        this.rolesList.push(this.getRolByValue(this.user_key));
        break;
      }
      case this.user_key: {
        // esto no debería pasar porque un usuario NO puede crear otros usuarios
        break;
      }
    }
  }

  public selectedRol() {
    let enable = this.form.getRawValue().rol == this.user_key; 
    let formControl = this.form.get('puedeEditar');
    formControl[enable ? 'enable' : 'disable']();
    formControl.setValue(!enable);

    switch(this.form.value.rol) {
      case this.admin_key:
        this.aclaracion = "(acceso total al sistema)";
        break;
      case this.admin_sector_key:
        this.aclaracion = "(usuarios y contenido dentro de su sector)"
        break;
      case this.user_key:
        this.aclaracion = "(contenido dentro de su sector)"
        break;
    }

    if(this.form.getRawValue().rol == this.admin_key) {
      this.form.get('sector').setValue(null);
      this.form.get('sector').disable();
    } else {
      this.form.get('sector').enable();
    }
  }

  public ngOnInit(): void {
    this.form = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      apellido: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      email: ['', [
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]
      ],
      rol: ['', [Validators.required]],
      puedeEditar: [],
      sector:['', [Validators.required]]
    }); 

    if (this.id) {
      this.labelButton = "Guardar";
      this.userService.getUserById(this.id).subscribe(r => {
        this.form.get('nombre').setValue(r.nombre);
        this.form.get('nombre').disable();
        this.form.get('apellido').setValue(r.apellido);
        this.form.get('apellido').disable();
        this.form.get('username').setValue(r.username);
        this.form.get('username').disable();
        this.form.get('email').setValue(r.email);
        this.form.get('password').setValue("lo asigno para que no me quede como form invalid");
        this.form.get('rol').setValue(r.rol.tipo);
        this.form.get('rol').disable();
        this.form.get('sector').setValue(r.sector?.id);
        this.selectedRol();
        this.form.get('sector').disable();
        this.form.get('puedeEditar').setValue(r.puedeEditar);
      });
    } else {
      this.labelButton = "Agregar";
      this.selectedRol(); // para inicializar el check
    }
  }

  public togglePasswordVissibility(event: any): void {
    event.preventDefault();
    event.stopPropagation();
    this.hidePass = !this.hidePass;
  }

  public submit() {
    const value = this.form.getRawValue();

    if (!this.id) {
      value.idCurrentUser = this.authService.getCurrentUser().id;  
      this.userService.createUser(value).subscribe(response => {
        if (response.ok) {
          this.errorMessage.next("");
          this.location.back();
        } else {
          this.errorMessage.next(response.message);
        }
      });  
    } else {
      value.id = this.id;
      this.userService.updateUser(value).subscribe(response => {
        if (response.ok) {
          this.errorMessage.next("");
          this.location.back();
        } else {
          this.errorMessage.next(response.message);
        }
      });
    }
  }

  public back() {
    this.router.navigate(['manage-users']);
  }
}
