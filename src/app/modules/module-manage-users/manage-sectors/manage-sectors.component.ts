import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { SectorService } from 'src/app/services/sector.service';
import { SessionService } from 'src/app/services/session.service';
import { BehaviorSubject } from 'rxjs';
import { take, debounceTime, filter } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-manage-sectors',
    templateUrl: './manage-sectors.component.html',
    styleUrls: ['./manage-sectors.component.scss']
})
export class ManageSectorsComponent implements OnInit {
    private currentUserId: number;
    private currentSector: any;

    adding: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    sectors: any[] = new Array<any>();
    nameFormControl: FormControl = new FormControl('', [Validators.required]);

    constructor(private sectorService: SectorService,
        private activatedRoute: ActivatedRoute,
        private sessionService: SessionService) {
        
        this.currentUserId = this.sessionService.getCurrentUser().id;
        this.sectors = this.activatedRoute.snapshot.data.pageData;
        this.adding.pipe(debounceTime(100), filter(a => a)).subscribe(a => { 
            (<HTMLInputElement>document.getElementById("input")).focus();
        });
    }

    private quitAddingMode() {
        this.adding.next(false);
        this.nameFormControl.reset();
        this.currentSector = undefined;
    }

    private loadSectors() {
        this.sectorService.getAll().subscribe(r => {
            this.sectors = r;

            // lo cambio acá y no cuando mando a grabar por caso de delay en servidor
            this.quitAddingMode();
        });
    }

    ngOnInit(): void {
    }

    newSector(): void {
        this.adding.next(true);
    }

    edit(sector: any) {
        this.currentSector = sector;
        this.nameFormControl.setValue(sector.nombre);
        this.adding.next(true);
    }
    
    delete(sector: any) {
        this.sectorService.delete(sector.id).subscribe(r => {
            this.loadSectors();
        });
    }

    save() {
        if (this.nameFormControl.valid) {
            if (this.currentSector) {
                let sectorDto = {
                    id: this.currentSector.id,
                    nombre: this.nameFormControl.value,
                    idOrganizacion: this.currentSector.idOrganizacion
                }
                this.sectorService.edit(sectorDto).subscribe(r => {
                    this.currentSector = undefined;
                    this.loadSectors();
                });
            } else {
                this.sectorService.save(this.nameFormControl.value, this.currentUserId).subscribe(r => {
                    this.loadSectors();
                });
            }
        }
    }

    cancel() {
        this.quitAddingMode();
    }
}