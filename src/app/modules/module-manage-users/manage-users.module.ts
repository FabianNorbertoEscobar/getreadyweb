import { NgModule } from "@angular/core";
import { SharedModule } from '../shared/shared.module';
import { UtilsModule } from '../utils.module';
import { RouterModule, Routes } from '@angular/router';
import { CreateUserComponent } from 'src/app/modules/module-manage-users/create-user/create-user.component';
import { ListUserComponent } from './list-user/list-user.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ManageSectorsComponent } from './manage-sectors/manage-sectors.component';
import { SectorComponent } from './sector.component/sector.component';
import { SectorsResolver } from './resolvers/sectors.resolver';
import { SharedPraiseModule } from '../shared/shared-praise.module';
import { UsersBySectorResolver } from './resolvers/users-by-sector.resolver';

const routes: Routes = [
  {
      path: '',
      component: ListUserComponent,
  },
  {
      path: 'sectors',
      component: ManageSectorsComponent,
      resolve: { pageData: SectorsResolver }
  },
  {
      path: 'sector',
      component: SectorComponent,
      resolve: { pageData: UsersBySectorResolver }
  },
  {
    path: 'create',
    component: CreateUserComponent,
  },
  {
    path: 'edit',
    component: CreateUserComponent,
  }
]

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        UtilsModule,
        ReactiveFormsModule,
        FormsModule,
        SharedPraiseModule,
    ],
  declarations: [
    ListUserComponent,
    CreateUserComponent,
    ManageSectorsComponent,
    SectorComponent
  ],
  providers: [
    SectorsResolver,
    UsersBySectorResolver
  ]
})
export class ManageUsersModule { }
