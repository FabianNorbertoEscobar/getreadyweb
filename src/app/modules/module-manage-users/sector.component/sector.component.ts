import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';
import { PraiseService } from '../../../services/praise.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-sector',
    templateUrl: './sector.component.html',
    styleUrls: ['./sector.component.scss']
})
export class SectorComponent implements OnInit {

    sector: any;
    users: any[] = new Array<any>();
    allUsersBySector: Map<string, Array<any>>;
    sectores: any[];
    currentUser: any;
    praiseEnabled: boolean;

    constructor(
      private sessionService: SessionService,
      private activatedRoute: ActivatedRoute,
      private praiseService: PraiseService,
      private router: Router
    ) {
      this.currentUser = this.sessionService.getCurrentUser();
      this.allUsersBySector = this.activatedRoute.snapshot.data.pageData.allUsersBySector;
      this.sectores = this.activatedRoute.snapshot.data.pageData.sectors;
    }

    ngOnInit(): void {
      this.praiseService.getPraiseAvailability(this.currentUser.id).subscribe(x => this.praiseEnabled = x );
    }

    getRol(roleType: string) {
        if (roleType == "USUARIO") return "";
        if (roleType == "ADMIN_SECTOR") return "Administrador del sector";
        if (roleType == "ADMIN") return "Administrador general";
    }

    openUserProfile(user: any) {
        this.router.navigate([`profile`], { queryParams: {id: user.id} });
    }
}
