import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { SectorService } from 'src/app/services/sector.service';

@Injectable()
export class SectorsResolver implements Resolve<any> {
    
    constructor(private sectorService: SectorService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.sectorService.getAll();
    }
}