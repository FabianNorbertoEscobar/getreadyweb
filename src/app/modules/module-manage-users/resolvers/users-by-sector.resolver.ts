import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SectorService } from 'src/app/services/sector.service';
import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';

@Injectable()
export class UsersBySectorResolver implements Resolve<any> {
   
    constructor(private sessionService: SessionService,
        private sectorService: SectorService,
        private userService: UserService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let currentUser = this.sessionService.getCurrentUser();
        
        let requests: Observable<any>[] = [
            this.sectorService.getAll(),
            this.userService.getUsers()
        ];

        return forkJoin(requests).pipe(map(result => {
            let allUsersBySector = new Map<string, Array<any>>();
            let sectores = result[0];
            sectores.forEach(sector => {
                let usersInSector = result[1].filter(u => u.id != currentUser.id &&
                    (!u.sector || u.sector?.id == sector.id));
                usersInSector.sort((n1, n2) =>
                {
                    if (n1.rol.tipo > n2.rol.tipo) { return 1; }
                    if (n1.rol.tipo < n2.rol.tipo) { return -1; }
                    return 0;
                });

                allUsersBySector.set(sector.id, usersInSector);
            });

            return {
                sectors: sectores,
                allUsersBySector: allUsersBySector
            }
        }));
    }
}