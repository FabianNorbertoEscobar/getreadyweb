import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { User } from '../UserDatabase';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from 'src/app/services/auth.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { DialogConfirmationComponent } from 'src/app/components/dialog/dialog-confirmation/dialog-confirmation.component';
import { DialogInfoComponent } from 'src/app/components/dialog/dialog-info/dialog-info.component';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {
  private currentUserSector: number;
  private currentUserRoleType: string;
  public dataSource: MatTableDataSource<User> = new MatTableDataSource<User>();

  constructor(
    private router: Router, 
    private userService: UserService,
    private authService: AuthService,
    private dialog: MatDialog) { }


  private canEditOrDelete(user: User): boolean {
    return this.currentUserRoleType == "ADMIN" || this.currentUserSector == user.sector?.id;
  }

  private showCannotDoAction() {
    const dialogRef: MatDialogRef<DialogInfoComponent> = this.dialog.open(DialogInfoComponent, {
      data: {
        title: 'Atención',
        description: 'No puede realizar la operación porque no tiene permisos.'
      }
    });
  }

  public ngOnInit() {
    this.userService.getUsers().subscribe(data => {
      let currentUser = this.authService.getCurrentUser();
      this.currentUserRoleType = currentUser.roleType;
      if (currentUser.roleType == "ADMIN_SECTOR") {
        this.currentUserSector = data.find(u => u.id == this.authService.getCurrentUser().id).sector.id;
      }   
      data.sort((n1,n2) => {
        if (!n1.sector || !n2.sector) { return -1; }
        if (n1.sector.nombre > n2.sector.nombre) { return 1; }
        if (n1.sector.nombre < n2.sector.nombre) { return -1; }
        return 0;
      });
      this.dataSource.data = data.filter(u => u.id != this.authService.getCurrentUser().id);
    });
  }

  public deleteUser(user: User): void {
    if (!this.canEditOrDelete(user)) {
      this.showCannotDoAction();
    } else {
      const dialogRef: MatDialogRef<DialogConfirmationComponent> = this.dialog.open(DialogConfirmationComponent, {
        data: {
          title: 'Confirmar',
          description: 'El usuario será eliminado y se perderá su progreso ¿Desea continuar?'
        }
      });
      dialogRef.afterClosed().subscribe(data => {
        if (data) {
          this.userService.deleteUser(user.id).subscribe(response => {
            if (response.ok) {
              this.dataSource.data = this.dataSource.data.filter(u => u.id != user.id);
            }
          });
        }
      });
    }
  }

  public editUser(user: User): void {
    if(!this.canEditOrDelete(user)) {
      this.showCannotDoAction()
    } else {
      this.router.navigate(['manage-users/edit'], { queryParams: { userId: user.id }});
    }
  }

  public addUser(): void {
    this.router.navigate(['manage-users/create']);
  }
}
