import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { GetReadyComponent } from '../main/get-ready/get-ready.component';
import { ProfileResolver } from './resolvers/profile.resolver';

const routes: Routes = [
    {
      path: '',
      component: GetReadyComponent,
      resolve: { pageData: ProfileResolver },
      children: [
          {
              path: '',
              redirectTo: '/home',
              pathMatch: 'full',
          },
          {
              path: '',
              loadChildren: () => import('../main/get-ready.module').then(m => m.GetReadyModule),
          }
      ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GlobalRoutingModule {}
