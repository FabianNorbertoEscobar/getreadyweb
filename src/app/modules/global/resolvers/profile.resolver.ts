import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { forkJoin, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AchievementsSharedService } from 'src/app/services/achievements-shared.service';
import { AchievementsService } from '../../module-achievements/services/achievements.service';

@Injectable()
export class ProfileResolver implements Resolve<any> {

    constructor(
        private achievementsService: AchievementsService,
        private achievementsSharedService: AchievementsSharedService,
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        console.log('fetching user...');

        return forkJoin([this.achievementsService.getAchievements(), this.achievementsService.getUserAchievements()]).pipe(
            tap(
                (result) => {
                    this.achievementsSharedService.achievements.next(result[0]);
                    this.achievementsSharedService.userAchievements.next(result[1].sort((a, b) => { return (new Date(a.date)).getTime()- (new Date(b.date)).getTime() }));
                }
            ),
            catchError(error => {
                return throwError(error);
            }),
        );
    }
}