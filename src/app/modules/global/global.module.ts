import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { GlobalRoutingModule } from './global-routing.module';
import { ProfileResolver } from './resolvers/profile.resolver';
import { ToolbarComponent } from 'src/app/components/toolbar/toolbar.component';
import { NavbarComponent } from 'src/app/components/navbar/navbar.component';
import { OverlayTransparentComponent } from 'src/app/components/overlay-transparent/overlay-transparent.component';
import { GetReadyComponent } from '../main/get-ready/get-ready.component';
import { MaterialModule } from '../shared/material.module';
import { ChatbotModule } from '../module-chatbot/chatbot.module';
import { DialogConfirmationComponent } from 'src/app/components/dialog/dialog-confirmation/dialog-confirmation.component';
import { DialogInfoComponent } from 'src/app/components/dialog/dialog-info/dialog-info.component';
import { AchievementsService } from '../module-achievements/services/achievements.service';
import { OverlayConfettiComponent } from 'src/app/components/overlay-confetti/overlay-confetti.component';
import { OverlayAchievementComponent } from 'src/app/components/overlay-achievement/overlay-achievement.component';
import { NotificationComponent } from 'src/app/components/notifications/notification.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    NotificationComponent,
    ToolbarComponent,
    NavbarComponent,
    OverlayTransparentComponent,
    GetReadyComponent,
    DialogConfirmationComponent,
    DialogInfoComponent,
    OverlayConfettiComponent,
    OverlayAchievementComponent,
  ],
  imports: [
    GlobalRoutingModule,
    CommonModule,
    MaterialModule,
    ChatbotModule,
    ReactiveFormsModule,
  ],
  providers: [
    ProfileResolver,
    AchievementsService,
  ]
})
export class GlobalModule {}