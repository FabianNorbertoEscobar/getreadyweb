import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarCompetenciasComponent } from './listar-competencias.component';

describe('ListarCompetenciasComponent', () => {
  let component: ListarCompetenciasComponent;
  let fixture: ComponentFixture<ListarCompetenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarCompetenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarCompetenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
