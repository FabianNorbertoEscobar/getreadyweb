import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { Competencia } from '../competencia';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { DialogConfirmationComponent } from 'src/app/components/dialog/dialog-confirmation/dialog-confirmation.component';
import { CompetenciasService } from 'src/app/services/competencias.service';

@Component({
  selector: 'app-listar-competencias',
  templateUrl: './listar-competencias.component.html',
  styleUrls: ['./listar-competencias.component.scss']
})
export class ListarCompetenciasComponent implements OnInit {

  public dataSource: MatTableDataSource<Competencia> = new MatTableDataSource<Competencia>();

  constructor(
    private router: Router, 
    private userService: UserService,
    private competenciasService: CompetenciasService,
    private dialog: MatDialog) { }

  public ngOnInit() {
    this.competenciasService.getCompetencias().subscribe(data => {
      this.dataSource.data = data;
    });
  }

  public eliminar(competencia: Competencia): void {
    const dialogRef: MatDialogRef<DialogConfirmationComponent> = this.dialog.open(DialogConfirmationComponent, {
      data: {
        title: 'Confirmar',
        description: 'La competencia será eliminada ¿Desea continuar?'
      }
    });
    
    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.competenciasService.eliminar(competencia.id).subscribe(response => {
          if (response.ok) {
            this.dataSource.data = this.dataSource.data.filter(c => c.id != competencia.id);
          }
        });
      }
    });
  }

  public modificar(competencia: Competencia): void {
    this.router.navigate(['competencias/modificar'], { queryParams: { id: competencia.id }});
  }

  public agregar(): void {
    this.router.navigate(['competencias/agregar']);
  }
}
