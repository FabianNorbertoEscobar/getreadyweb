import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Competencia } from '../competencia';
import { CompetenciasService } from 'src/app/services/competencias.service';

@Component({
  selector: 'app-modificar-competencia',
  templateUrl: './modificar-competencia.component.html',
  styleUrls: ['./modificar-competencia.component.scss']
})
export class ModificarCompetenciaComponent implements OnInit {

  public form: FormGroup;
  private id: number = -1;
  private errorMessage = new BehaviorSubject<string>("");
  public errorMessage$ = this.errorMessage.asObservable();

  
  constructor(
    private formBuilder: FormBuilder,
    private location: Location,
    private competenciasService: CompetenciasService,
    private route: ActivatedRoute
  ) { 
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
    });
  }

  public ngOnInit(): void {
    this.form = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]]
    }); 
    this.competenciasService.getCompetenciasByOrganizacion(this.id).subscribe(competencia => {
      this.form.get('nombre').setValue(competencia.nombre);
      this.form.get('descripcion').setValue(competencia.descripcion);
    });
    
  }

  public submit() {
    const value = this.form.value;
    value.id = this.id;
    let obs = this.competenciasService.modificar(value);
    obs.subscribe(response => {
      if (response.ok) {
        this.errorMessage.next("");
        this.location.back();
      } else {
        this.errorMessage.next(response.message);
      }
    })
  }

  public back() {
    this.location.back();
  }
}
