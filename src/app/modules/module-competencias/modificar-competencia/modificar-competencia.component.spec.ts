import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificarCompetenciaComponent } from './modificar-competencia.component';

describe('ModificarCompetenciaComponent', () => {
  let component: ModificarCompetenciaComponent;
  let fixture: ComponentFixture<ModificarCompetenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificarCompetenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificarCompetenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
