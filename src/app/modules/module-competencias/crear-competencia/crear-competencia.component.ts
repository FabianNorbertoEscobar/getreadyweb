import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { Location } from '@angular/common';
import { CompetenciasService } from 'src/app/services/competencias.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-crear-competencia',
  templateUrl: './crear-competencia.component.html',
  styleUrls: ['./crear-competencia.component.scss']
})
export class CrearCompetenciaComponent implements OnInit {

  public form: FormGroup;
  public hidePass: boolean = true;

  private errorMessage = new BehaviorSubject<string>("");
  public errorMessage$ = this.errorMessage.asObservable();

  
  constructor(
    private formBuilder: FormBuilder,
    private location: Location,
    private competenciasService: CompetenciasService,
    private authService: AuthService
  ) { }

  public ngOnInit(): void {
    this.form = this.formBuilder.group({
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]]
    }); 
  }

  public submit() {
    const value = this.form.value;
    value.idUsuario = this.authService.getCurrentUser().id;
    let obs = this.competenciasService.agregar(value);
    obs.subscribe(response => {
      if (response.ok) {
        this.errorMessage.next("");
        this.location.back();
      } else {
        this.errorMessage.next(response.message);
      }
    })
  }

  public back() {
    this.location.back();
  }
}
