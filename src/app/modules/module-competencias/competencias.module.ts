import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrearCompetenciaComponent } from './crear-competencia/crear-competencia.component';
import { ModificarCompetenciaComponent } from './modificar-competencia/modificar-competencia.component';
import { ListarCompetenciasComponent } from './listar-competencias/listar-competencias.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { UtilsModule } from '../utils.module';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
      path: '',
      component: ListarCompetenciasComponent,
  },
  {
    path: 'agregar',
    component: CrearCompetenciaComponent,
  },
  {
    path: 'modificar',
    component: ModificarCompetenciaComponent,
  }
]

@NgModule({
  declarations: [CrearCompetenciaComponent, ModificarCompetenciaComponent, ListarCompetenciasComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    UtilsModule,
    ReactiveFormsModule
  ]
})
export class CompetenciasModule { }
