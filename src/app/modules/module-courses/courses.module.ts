import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CoursesComponent } from './courses/courses.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedCoursesModule } from '../shared/shared-courses.module';
import { CoursesResolver } from './resolvers/courses-resolver.service';
import { CourseCardComponent } from './course-card/course-card.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CourseGiveFeedback } from './course-give-feedback/course-give-feedback.component';
import { StarRatingComponent } from './course-give-feedback/course-rating/star-rating.component';
import { CourseGetFeedback } from './course-get-feedback/course-get-feedback.component';
import { CourseStatsComponent } from './course-stats/course-stats.component';
import { ChartsModule } from 'ng2-charts';
import { CoursesStatsResolver } from './resolvers/courses-stats.resolver';

const routes: Routes = [
    {
        path: '',
        component: CoursesComponent,
        resolve: { pageData: CoursesResolver },
    },
    {
        path: 'stats',
        component: CourseStatsComponent,
        resolve: { pageData: CoursesStatsResolver }
    },
    {
      path: 'new-course',
      loadChildren: () => (import('./module-new-course/new-course.module')).then(m => m.NewCourseModule),
    },
    {
      path: ':courseId/sections',
      loadChildren: () => (import('./module-sections/sections.module')).then(m => m.SectionsModule),
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        SharedCoursesModule,
        ReactiveFormsModule,
        ChartsModule
    ],
    declarations: [
        CoursesComponent,
        CourseCardComponent,
        CourseGiveFeedback,
        CourseGetFeedback,
        StarRatingComponent,
        CourseStatsComponent
    ],
    providers: [
        CoursesResolver,
        CoursesStatsResolver
    ]
})
export class CoursesModule {}
