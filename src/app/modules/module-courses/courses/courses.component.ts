import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Course } from 'src/app/interfaces/course.interface';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject, fromEvent } from 'rxjs';
import { startWith, map, debounceTime } from 'rxjs/operators';
import { PermissionsService } from 'src/app/services/permissions.service';
import { SessionService } from 'src/app/services/session.service';

export interface CourseFilter {
  description: string;
  filter(course: Course): boolean;
}

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {

  allCourses: Course[] = [];
  filteredCourses: Course[] = [];

  removable = true;

  separatorKeysCodes: number[] = [ENTER, COMMA];
  filterControl = new FormControl();
  filteredFilters: Observable<CourseFilter[]>;
  filters: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  allFilters: CourseFilter[] = [];

  searchControl = new FormControl();

  @ViewChild('filterInput')
  filterInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto')
  matAutocomplete: MatAutocomplete;

  @ViewChild('searchInput', { static: true })
  searchInput: ElementRef<HTMLInputElement>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private permissionService: PermissionsService,
    private sessionService: SessionService
  ) {
    this.allCourses = this.activatedRoute.snapshot.data.pageData;

    // filtro por sector
    this.allCourses = this.allCourses.filter(c => {
      let id_sector = this.sessionService.getCurrentUser().id_sector;
      if (id_sector) {
        return c.idSector == id_sector;
      }
      return true; 
    });

    this.filteredCourses = [...this.allCourses];

    this._setUpFilters();

    this.filteredFilters = this.filterControl.valueChanges.pipe(
      startWith(null),
      map((filter: CourseFilter | null) => filter ? (filter.description ? filter.description : filter ) : null),
      map((filter: string | null) => filter ? this._filter(filter) : this.allFilters.slice()),
    );
  }

  canSeeStats() {
    return this.sessionService.getCurrentUser()?.roleType != 'USUARIO';
  }

  ngOnInit(): void {
    this.filters.subscribe(filters => {
      this.applyFilters();
    });

    fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      debounceTime(1500),
    ).subscribe((event: KeyboardEvent) => {
      this.applyFilters();
    });
  }

  canCreateCourse(): boolean {
    return this.permissionService.canCreateOrEditContent();
  }

  newCourse() {
    this.router.navigate(['new-course'], { relativeTo: this.activatedRoute });
  }

  goToCourseStats() {
    this.router.navigate(['stats'], { relativeTo: this.activatedRoute });
  }

  /**
   * Quita el filtro de la lista de filtros aplicados
   * @param {string} filter el filtro
   */
  remove(filter: string): void {
    const index = this.filters.value.indexOf(filter);

    if (index >= 0) {
      this.filters.value.splice(index, 1);
      this.filters.next(this.filters.value);
    }
  }

  /**
   * Maneja el evento de selección de un filtro
   * @param {MatAutocompleteSelectedEvent} event el evento
   */
  selected(event: MatAutocompleteSelectedEvent): void {
    let isIncluded: boolean = false;

    for (let i = 0 ; i < this.filters.value.length ; i++) {
      if (this.filters.value[i] == event.option.viewValue) {
        isIncluded = true;
        break;
      }
    }

    if (!isIncluded) {
      this.filters.value.push(event.option.viewValue);
      this.filters.next(this.filters.value);
    }

    this.filterInput.nativeElement.value = '';
    this.filterControl.setValue(null);
  }

  /**
   * Aplica los filtros y el valor de búsqueda al listado de cursos
   */
  applyFilters(): void {
    let currentFilters: CourseFilter[] = this.allFilters.filter(filter => this.filters.value.includes(filter.description));

    // Aplica filtros
    if (this.filters.value.length) {
      this.filteredCourses = [];

      for (let course of this.allCourses) {
        for (let filter of currentFilters) {
          if (filter.filter(course) && this._searched(course)) {
            this.filteredCourses.push(course);
          }
        }
      }
    } else {
      // Aplica búsqueda
      if (this.searchControl.value) {
        this.filteredCourses = [];

        for (let course of this.allCourses) {
          if (this.searchControl.value && this._searched(course)) {
            this.filteredCourses.push(course);
          }
        }
      } else {
        this.filteredCourses = [...this.allCourses];
      }
    }
  }


  /**
   * Filtra el listado de filtros de acuerdo al valor introducido
   * @param {string}  value  el valor
   */
  private _filter(value: string): CourseFilter[] {
    const filterValue = value.toLowerCase();
    return this.allFilters.filter(filter => filter.description.toLowerCase().indexOf(filterValue) === 0);
  }

  /**
   * Indica si hay coincidencia entre el valor introducido en el campo de búsqueda y el curso
   * @param {Course}  course  el curso
   */
  private _searched(course: Course): boolean {
    if (this.searchControl.value) {
      return course.title.trim().toLowerCase().includes(this.searchControl.value.trim().toLowerCase());
    }

    return true;
  }

  /**
   * Inicializa los filtros a usar
   */
  private _setUpFilters() {
    this.allFilters.push(
      {
        description: 'Sin empezar',
        filter: (course: Course) => {
          if (course.progress == 0) {
            return true;
          }

          return false;
        }
      },
      {
        description: 'En curso',
        filter: (course: Course) => {
          if (course.progress > 0 && course.progress < 100) {
            return true;
          }

          return false;
        }
      },
      {
        description: 'Completado',
        filter: (course: Course) => {
          if (course.progress == 100) {
            return true;
          }

          return false;
        }
      }
    );
  }
}
