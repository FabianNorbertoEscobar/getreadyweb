import { Component } from '@angular/core';
import { Label } from 'ng2-charts';
import { SessionService } from 'src/app/services/session.service';
import { User } from 'src/app/services/user';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'course-stats',
    templateUrl: './course-stats.component.html',
    styleUrls: ['./course-stats.component.scss']
  })
  export class CourseStatsComponent {
    private currentUser: User;
    private usuarios: any[];
    chartOptions = {
        responsive: true,
        maintainAspectRatio: false
    }
    coursesStateLabel: Label[] = ['Sin empezar', 'En curso', 'Completado'];
    data: any[];
    
    constructor(
        private sessionService: SessionService,
        private activatedRoute: ActivatedRoute,
        private location: Location
    ) {
        this.currentUser = this.sessionService.getCurrentUser();
        let serverData = this.activatedRoute.snapshot.data.pageData;
        if (this.currentUser.id_sector) {
            this.usuarios = serverData.usuarios.filter(c => c.id_sector == this.currentUser.id_sector);;
            serverData = serverData.cursos.filter(c => c.idSector == this.currentUser.id_sector);
        } else {
            this.usuarios = serverData.usuarios;
            serverData = serverData.cursos;
        }
        for(let c of serverData) {
            // necesito que esta información sea estática para que muestre los tooltip del canvas
            c.canvasData = [ [c.sinEmpezar, c.enCurso, c.completado] ]
        }
        this.data = serverData;
    }

    getCoursesLabel() {
        if (this.currentUser.id_sector) {
            return "Cantidad de cursos en el sector: " + this.data?.length;
        }
        return "Cantidad total de cursos: " + this.data?.length;
    }

    getUsersLabel() {
        if (this.currentUser.id_sector) {
            return "Cantidad de usuarios en el sector: " + this.usuarios?.length;
        }
        return "Cantidad total de usuarios: " + this.usuarios?.length;
    }

    getLowestScore() {
        if (this.data) {
            let sorted = this.data.filter(d => d.promedio > 0).sort((n1,n2) => n1.promedio - n2.promedio);
            return sorted[0]?.nombre ?? "faltan calificaciones";
        }
        return "faltan cursos";
    }

    getHighestScore() {
        if (this.data) {
            let sorted = this.data.filter(d => d.promedio > 0).sort((n1,n2) => n2.promedio - n1.promedio);
            return sorted[0]?.nombre ?? "faltan calificaciones";
        }
        return "faltan cursos";
    }
    
    getPuntajePromedio(course: any): string {
        return course.promedio > 0 ? "Puntaje promedio: "+course.promedio : "Sin calificar"
    }

    back() {
        this.location.back();
    }
}
