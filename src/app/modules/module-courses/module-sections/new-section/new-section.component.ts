import { Component, OnInit } from '@angular/core';
import { SectionsService } from 'src/app/services/sections.service';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormArray, FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { OverlayService } from 'src/app/services/overlay.service';
import { DialogConfirmationComponent } from 'src/app/components/dialog/dialog-confirmation/dialog-confirmation.component';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { CoursesService } from 'src/app/services/courses.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-new-section',
  templateUrl: 'new-section.component.html',
  styleUrls: ['new-section.component.scss']
})

export class NewSectionComponent implements OnInit {
  private id: number;
  form: FormGroup;
  courseId: number;

  private errorMessage = new BehaviorSubject<string>('');
  public errorMessage$ = this.errorMessage.asObservable();

  constructor(
    private fb: FormBuilder,
    private sectionsService: SectionsService,
    private dialog: MatDialog,
    private overlayService: OverlayService,
    private route: ActivatedRoute,
    private router: Router,
    private coursesService: CoursesService,
    private activatedRoute: ActivatedRoute
  ) {

    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params['_id'];
    });

    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
      description: ['', [Validators.required, Validators.maxLength(255)]],
      text: [''],
      urls: new FormArray([])
    });
  }

  get name(): AbstractControl {
    return this.form.get('name');
  }

  get description(): AbstractControl {
    return this.form.get('description');
  }

  get text(): AbstractControl {
    return this.form.get('text');
  }

  get urls(): FormArray {
    return this.form.get('urls') as FormArray;
  }

  ngOnInit() {
    this.courseId = Number.parseInt(this.route.snapshot.paramMap.get('courseId'), 10);
    this.coursesService.getCourse(this.courseId).subscribe(response => {
      if (response.ok === false) {
        this.router.navigate(['courses']);
      }
    }).unsubscribe();

    if (this.id) {
      this.sectionsService.getSection(this.id).pipe(take(1)).subscribe(r => {
        this.name.setValue(r.nombre);
        this.description.setValue(r.descripcion);
        this.text.setValue(r.texto);
        r.videoUrls.array.forEach(e => {
          this.addUrlInput();
        });
        this.urls.setValue(r.videoUrls);
      })
    }
  }

  addUrlInput() {
    const lastUrlIndex = this.urls.value.length - 1;
    if (lastUrlIndex >= 0 && !this.urls.value[lastUrlIndex]) {
      return;
    }
    this.urls.push(new FormControl());
  }

  deleteUrlInput(index: number) {
    this.urls.removeAt(index);
  }

  submit() {
    const dialogRef: MatDialogRef<DialogConfirmationComponent> = this.dialog.open(DialogConfirmationComponent, {
      data: {
        title: `Confirmar ${this.id ? "modificación" : "creación"} de sección`,
        description: `Se ${this.id ? "modificará" : "creará"} la sección "${this.name.value}" ¿Desea continuar?`
      }
    });

    // TODO: Manejar el orden
    const section = {
      id: this.id,
      nombre: this.name.value,
      descripcion: this.description.value,
      texto: this.text.value,
      orden: 1,
      idCurso: this.courseId,
      videoUrls: this.urls.value
    };

    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.overlayService.displayLoadingOverlay();
        setTimeout(() => {
          this.overlayService.hideLoadingOverlay();
          if (this.id) {
            this.sectionsService.updateSection(section).subscribe(response => {
              // Si viene un 200, no llega response.ok, directamente viene el id
              if (response.ok !== false) {
                this.errorMessage.next('');
                this.router.navigate([`courses/${this.courseId}/sections`]);
              } else {
                this.errorMessage.next(response.message);
              }
            })
          } else {
            this.sectionsService.addSectionToCourse(section).subscribe(response => {
              // Si viene un 200, no llega response.ok, directamente viene el id
              if (response.ok !== false) {
                this.errorMessage.next('');
                this.router.navigate([`courses/${this.courseId}/sections`]);
              } else {
                this.errorMessage.next(response.message);
              }
            });
          }
        }, 1500);
      }
    });

  }

  back() {
    this.router.navigate([`courses/${this.courseId}/sections`]);
  }
}
