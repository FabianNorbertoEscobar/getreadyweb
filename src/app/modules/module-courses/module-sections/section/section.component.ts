import { Component, OnInit } from '@angular/core';
import { HeaderService } from 'src/app/services/header/header.service';
import { SectionsService } from 'src/app/services/sections.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import { Section } from 'src/app/interfaces/sections.interface';
import { CoursesService } from 'src/app/services/courses.service';
import { SessionService } from 'src/app/services/session.service';
import { AchievementsSharedService } from 'src/app/services/achievements-shared.service';
import { OverlayService } from 'src/app/services/overlay.service';

@Component({
  selector: 'app-section',
  templateUrl: 'section.component.html',
  styleUrls: ['section.component.scss']
})

export class SectionComponent implements OnInit {
  private courseId: number;
  private course: any;
  section$: Observable<Section>;
  isThereNext$: Observable<boolean>;
  isTherePrev$: Observable<boolean>;
  currentVideoIndex = 0;

  constructor(
    private sectionsService: SectionsService,
    private route: ActivatedRoute,
    private router: Router,
    private sanitizer: DomSanitizer,
    private courseService: CoursesService,
    private sessionService: SessionService,
    private achievementsSharedService: AchievementsSharedService,
    private overlayService: OverlayService
  ) { }

  ngOnInit() {
    this.courseId = Number.parseInt(this.route.snapshot.paramMap.get('courseId'), 10);
    this.courseService.getCourse(this.courseId).pipe(take(1)).subscribe(r => this.course = r);

    this.section$ = this.sectionsService.currentSection$.pipe(
      map(currentSection => {
        if (currentSection) {
          return currentSection;
        }
        // Si no está cargada previamente la sección (recargando la página), vuelvo a la vista de secciones
        this.router.navigate(['courses', this.courseId, 'sections']);
      })
    );
    this.isThereNext$ = this.sectionsService.isThereNext$;
    this.isTherePrev$ = this.sectionsService.isTherePrev$;
  }

  changeSection(offset: number) {
    this.sectionsService.changeSection(offset);
  }

  changeVideo(offset: number) {
    this.currentVideoIndex = this.currentVideoIndex + offset;
  }

  sanitizedURL(url: string) {
    const videoId = this.getVideoId(url);
    const embedUrl = `https://www.youtube.com/embed/${videoId}`;
    return this.sanitizer.bypassSecurityTrustResourceUrl(embedUrl);
  }

  getVideoId(url: string) {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
    const match = url.match(regExp);

    return (match && match[2].length === 11)
      ? match[2]
      : null;
  }

  saveAndExit() {
    this.overlayService.displayLoadingOverlay();
    let count = this.sectionsService.sections.value.length;
    let currentSection = this.sectionsService.currentSectionIndex.value + 1;
    
    let saveProgressDto = {
      total: count,
      current: currentSection,
      idCourse: this.courseId,
      idUser: this.sessionService.getCurrentUser().id
    }

    if (saveProgressDto.total == saveProgressDto.current) {
      this.achievementsSharedService.grantAchievementToUser('Erudito');
      this.achievementsSharedService.grantAchievementToUser(this.course.nombre);
    }

    this.courseService.saveProgress(saveProgressDto).subscribe(r => {
      this.overlayService.hideLoadingOverlay();
      this.router.navigate(['courses']);
    },
    () => this.overlayService.hideLoadingOverlay());
  }
}
