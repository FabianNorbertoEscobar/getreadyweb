import { Component, OnInit } from '@angular/core';
import { HeaderService } from 'src/app/services/header/header.service';
import { SectionsService } from 'src/app/services/sections.service';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { concatMap, tap } from 'rxjs/operators';
import { CoursesService } from 'src/app/services/courses.service';
import { Section } from 'src/app/interfaces/sections.interface';
import { PermissionsService } from 'src/app/services/permissions.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogConfirmationComponent } from 'src/app/components/dialog/dialog-confirmation/dialog-confirmation.component';
import { OverlayService } from 'src/app/services/overlay.service';

@Component({
  selector: 'app-sections',
  templateUrl: 'sections.component.html',
  styleUrls: ['sections.component.scss']
})

export class SectionsComponent implements OnInit {
  course$: Observable<any>;
  sections$: Observable<Array<Section>>;

  constructor(
    private headerService: HeaderService,
    private sectionsService: SectionsService,
    private coursesService: CoursesService,
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private permissionService: PermissionsService,
    private overlayService: OverlayService
  ) { }

  ngOnInit() {
    this.overlayService.displayLoadingOverlay();
    const courseId = Number.parseInt(this.route.snapshot.paramMap.get('courseId'), 10);
    this.course$ = this.coursesService.getCourse(courseId).pipe(
      // Si en la url hay un id de curso inexistente, retorno a la vista de cursos
      tap(response => {
        if (response.ok === false) {
          this.router.navigate(['courses']);
        }
      })
    );
    setTimeout(() => {
      this.course$.subscribe(course => {
        this.headerService.setDescriptionForRoute(course.nombre);
      });
    });
    this.sections$ = this.sectionsService.getSectionsByCourse(courseId).pipe(
      tap(() => this.overlayService.hideLoadingOverlay())
    );
  }

  goToSection(section: Section, sectionIndex: number) {
    this.sectionsService.selectSection(section, sectionIndex);
    this.router.navigate([section.id], { relativeTo: this.route });
  }

  canCreateSection(): boolean {
    return this.permissionService.canCreateOrEditContent();
  }

  newSection() {
    this.router.navigate(['new-section'], { relativeTo: this.route });
  }

  edit(section: Section, event: any): void {
    this.router.navigate(['new-section'], { queryParams: {_id: section.id}, relativeTo: this.route });
  }

  onClickMenu(event: any) {
    event.stopPropagation();
  }

  delete(section: Section, event: any): void {
    event.stopPropagation();
    const dialogRef: MatDialogRef<DialogConfirmationComponent> = this.dialog.open(DialogConfirmationComponent, {
      data: {
        title: 'Confirmar eliminación',
        description: `Se eliminará la sección "${section.nombre}". La información no podrá ser recuperada. ¿Desea continuar?`
      }
    });

    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.sectionsService.deleteSection(section.id).subscribe(() => { location.reload(); });
      }
    });
  }
}
