import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SectionsComponent } from './sections/sections.component';
import { SharedModule } from '../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NewSectionComponent } from './new-section/new-section.component';
import { SectionComponent } from './section/section.component';

const routes: Routes = [
  {
    path: '',
    component: SectionsComponent,
  },
  {
    path: 'new-section',
    component: NewSectionComponent
  },
  {
    path: ':sectionId',
    component: SectionComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ReactiveFormsModule,
  ],
  exports: [],
  declarations: [
    SectionsComponent,
    NewSectionComponent,
    SectionComponent
  ],
  providers: [],
})
export class SectionsModule { }
