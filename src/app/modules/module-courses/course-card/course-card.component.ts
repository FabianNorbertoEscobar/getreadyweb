import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Course } from 'src/app/interfaces/course.interface';
import { CoursesService } from 'src/app/services/courses.service';
import { PermissionsService } from 'src/app/services/permissions.service';
import { SessionService } from 'src/app/services/session.service';
import { CourseGetFeedback } from '../course-get-feedback/course-get-feedback.component';
import { CourseGiveFeedback } from '../course-give-feedback/course-give-feedback.component';

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.scss']
})
export class CourseCardComponent implements OnInit {
  private isUser: boolean;

  @Input()
  course: Course;
  buttonId = "feedback";

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private permissionService: PermissionsService,
    private sessionService: SessionService,
    private dialog: MatDialog,
    private courseService: CoursesService
  ) { 
    this.isUser = this.sessionService.getCurrentUser().roleType == "USUARIO";
  }

  private parentIsButton(target: any) {
    return target.id == this.buttonId // click en boton
      || target.parentElement.id == this.buttonId // click en span
      || target.parentElement.parentElement.id == this.buttonId; // click en icono
  }

  openGiveFeedbackModal(data: any): void {
    const feedbackModal: MatDialogRef<CourseGiveFeedback> = this.dialog.open(CourseGiveFeedback, { data: data});
    feedbackModal.afterClosed().subscribe(data => {
      if (data) {
        data.idCourse = this.course.id;
        data.idUser = this.sessionService.getCurrentUser().id;
        this.courseService.giveFeedback(data).subscribe(r => {
          // suscribo pa que haga algo
        });
      }
    });
  }

  private giveFeedback(): void {
    if (this.course.rated) {
      this.courseService.getFeedbackUser(this.course.id, this.sessionService.getCurrentUser().id).subscribe(r => {
        this.openGiveFeedbackModal(r);
      });
    } else {
      this.openGiveFeedbackModal(undefined);
    }
    
  }

  private getFeedback(): void {
    this.dialog.open(CourseGetFeedback, { data: { id: this.course.id }});
  }

  canEdit(): boolean {
    return this.permissionService.canCreateOrEditContent();
  }

  showFeedbackButton(): boolean {
    if (this.isUser) {
      return this.course.progress == 100;
    } 
    return true; 
  }

  getLabelFeedbackButton(): string {
    if (this.isUser) {
      return "Dar feedback";
    }
    return "Ver feedback";
  }

  feedbackClick() {
    if (this.isUser) {
      this.giveFeedback();
    } else {
      this.getFeedback();
    }
    
  }

  ngOnInit(): void {

  }

  takeCourse(event: any) {
    if (!this.parentIsButton(event.target)) {
      this.router.navigate([this.course.id, 'sections'], { relativeTo: this.activatedRoute });
    }
  }

  edit() {
    this.router.navigate([`new-course`], { queryParams: {_id: this.course.id}, relativeTo: this.activatedRoute });
  }
}
