import { Component, OnInit, Input } from '@angular/core';
import { Course } from 'src/app/interfaces/course.interface';

@Component({
  selector: 'app-course-progress-card',
  templateUrl: './course-progress-card.component.html',
  styleUrls: ['./course-progress-card.component.scss']
})
export class CourseProgressCardComponent implements OnInit {

  @Input()
  course: Course;

  constructor() { }

  ngOnInit(): void {
  }

}
