import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseProgressCardComponent } from './course-progress-card.component';

describe('CourseCardComponent', () => {
  let component: CourseProgressCardComponent;
  let fixture: ComponentFixture<CourseProgressCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseProgressCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseProgressCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
