import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CoursesService } from 'src/app/services/courses.service';

@Component({
    selector: 'app-course-get-feedback',
    templateUrl: './course-get-feedback.component.html',
    styleUrls: ['./course-get-feedback.component.scss']
})
export class CourseGetFeedback implements OnInit {
    opinions: any[] = [];
    promedio: number = -1;
    loading = true;

    constructor(private dialogRef: MatDialogRef<CourseGetFeedback>,
        private coursesService: CoursesService,
        @Inject(MAT_DIALOG_DATA) public data: any) {
        this.coursesService.getFeedback(data.id).subscribe(r => {
            this.loading = false;
            this.opinions = r;

            if (r.length > 0) {
                let suma = 0;
                for(let opinion of this.opinions) {
                    suma += opinion.puntaje;
                }
                this.promedio = suma / this.opinions.length;
            }
            
        });
    }

    ngOnInit(): void {
    
    }
    
    close() {
        this.dialogRef.close();
    }
}