import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { of, throwError } from 'rxjs';
import { delay, catchError, map } from 'rxjs/operators';
import { CoursesService } from 'src/app/services/courses.service';
import { SessionService } from 'src/app/services/session.service';

@Injectable()
export class CoursesResolver implements Resolve<any> {

  // private courses$ = of([
  //   {
  //     title: 'Introducción a la empresa',
  //     progress: 86,
  //   },
  //   {
  //     title: '¿Qué realizamos día a día?',
  //     progress: 100,
  //   },
  //   {
  //     title: 'Nuestros clientes',
  //     progress: 0,
  //   },
  // ]);

    constructor(private coursesService: CoursesService,
      private sessionService: SessionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.coursesService.findByUser(this.sessionService.getCurrentUser().id).pipe(
            delay(1000),
            map(courses => courses.map(({ dioFeedback, progreso, nombre, ...course }) => ({
              ...course,
              title: nombre,
              progress: progreso,
              rated: dioFeedback
            }))),
            catchError(error => {
                return throwError(error);
            }),
        );
    }
}
