import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { CoursesService } from 'src/app/services/courses.service';

@Injectable()
export class CoursesStatsResolver implements Resolve<any> {
    
    constructor(private coursesService: CoursesService) {}
    
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.coursesService.getStats();
    }

}