import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StarRatingComponent } from './course-rating/star-rating.component';

@Component({
    selector: 'app-course-give-feedback',
    templateUrl: './course-give-feedback.component.html',
    styleUrls: ['./course-give-feedback.component.scss']
})
export class CourseGiveFeedback implements OnInit {
    @ViewChild("starRating", { static: false }) starRating: StarRatingComponent;
    form: FormGroup;
    rating: number;
    constructor(private dialogRef: MatDialogRef<CourseGiveFeedback>,
        @Inject(MAT_DIALOG_DATA) public data: any) { 
            this.form = new FormBuilder().group({
            opinion: ['', [Validators.required]]
        });
    }
     
    get opinion(): AbstractControl {
        return this.form.get('opinion');
    }
    
    ngOnInit(): void {
        this.opinion.setValue(this.data?.opinion);
        this.rating = this.data?.puntaje ?? 5;
    }

    close(): void {
        this.dialogRef.close();
    }

    save(): void {
        let dialogResult = {
            rating: this.starRating.rating,
            opinion: this.opinion.value
        }
        this.dialogRef.close(dialogResult);
    }
}
