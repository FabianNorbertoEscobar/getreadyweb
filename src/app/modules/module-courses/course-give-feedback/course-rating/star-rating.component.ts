import {Component, Input} from '@angular/core';

@Component({
  selector: 'star-rating',
  template: `
    <div class="star-rating">
      <p *ngIf="title"> {{ title }}</p>
      <i *ngIf="showOperators" class="fas fa-angle-left operator" (click)="minus()" title="-0.5"></i>
      <i *ngFor="let fullStar of fullStars" class="fas fa-star"></i>
      <i *ngIf="hasAnHalfStar" class="fas fa-star-half-alt"></i>
      <i *ngFor="let emptyStar of emptyStars" class="far fa-star"></i>
      <i *ngIf="showOperators" class="fas fa-angle-right operator" (click)="plus()" title="+0.5"></i>
    </div>
  `,
  styles: [`
    .fa-star, .fa-star-half-alt {
      color: orange;
      font-size: x-large;
      margin-left: 0.25rem;
      margin-right: 0.25rem;
    }
    .operator {
      margin-left: 1rem;
      margin-right: 1rem;
      cursor: pointer;
    } 
    .star-rating {
      min-width: 100px;
    }
  `]
})
export class StarRatingComponent {
  private readonly MAX_NUMBER_OF_STARS = 5;

  @Input()
  rating = 0;

  @Input()
  showOperators = false;

  @Input()
  title = "";

  private get numberOfFullStars(): number {
    return Math.floor(this.rating);
  }

  private get numberOfEmptyStars(): number {
    return this.MAX_NUMBER_OF_STARS - Math.ceil(this.rating);
  }

  get fullStars(): any[] {
    return Array(this.numberOfFullStars);
  }

  get emptyStars(): any[] {
    return Array(this.numberOfEmptyStars);
  }

  get hasAnHalfStar(): boolean {
    return this.rating % 1 !== 0;
  }

  minus() {
    if (this.rating > 0) {
      this.rating -= 0.5;
    }
  }

  plus() {
    if (this.rating < 5) {
      this.rating += 0.5;
    }
  }
}