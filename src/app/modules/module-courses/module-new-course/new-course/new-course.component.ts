import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { CoursesService } from 'src/app/services/courses.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { DialogConfirmationComponent } from 'src/app/components/dialog/dialog-confirmation/dialog-confirmation.component';
import { OverlayService } from 'src/app/services/overlay.service';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { SectorService } from 'src/app/services/sector.service';
import { LevelsService } from 'src/app/services/levels.service';

export interface SelectItem {
  value: string;
  description: string;
}

@Component({
  selector: 'app-new-course',
  templateUrl: 'new-course.component.html',
  styleUrls: ['new-course.component.scss']
})
export class NewCourseComponent implements OnInit {
  private id: number = undefined;
  private currentUser;
  public form: FormGroup;

  private errorMessage = new BehaviorSubject<string>('');
  public errorMessage$ = this.errorMessage.asObservable();
  public sectorList: SelectItem[] = new Array<SelectItem>();

  constructor(
    private fb: FormBuilder,
    private coursesService: CoursesService,
    private dialog: MatDialog,
    private overlayService: OverlayService,
    private router: Router,
    private sessionService: SessionService,
    private sectorService: SectorService,
    private activatedRoute: ActivatedRoute,
    private levelsService: LevelsService
  ) {

    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params['_id'];
    });

    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
      sector: [{ value: '', disabled: this.sessionService.getCurrentUser().roleType != "ADMIN" }, [Validators.required]],
      description: ['', [Validators.required]],
    });

    this.initializeSectoresList();
  }

  private initializeSectoresList(): void {
    this.currentUser = this.sessionService.getCurrentUser();
    this.sectorService.findAllByUser(this.currentUser.id).subscribe(r => {
      for (let sector of r) {
        let selectSector = {
          value: sector.id,
          description: sector.nombre
        }
        this.sectorList.push(selectSector);
      }
    });

    if (this.currentUser.id_sector) {
      this.sector.setValue(this.currentUser.id_sector);
    }
  }

  get name(): AbstractControl {
    return this.form.get('name');
  }

  get sector(): AbstractControl {
    return this.form.get('sector');
  }

  get description(): AbstractControl {
    return this.form.get('description');
  }

  ngOnInit() { 
    if (this.id) {
      this.coursesService.getCourse(this.id).subscribe(r => {
        this.name.setValue(r.nombre);
        this.sector.setValue(r.idSector);
        this.description.setValue(r.descripcion);
      })
    } 
  }

  submit() {
    const dialogRef: MatDialogRef<DialogConfirmationComponent> = this.dialog.open(DialogConfirmationComponent, {
      data: {
        title: `Confirmar ${this.id ? "modificación": "creación"} de curso`,
        description: `Se ${this.id ? "modificará" : "creará"} el curso "${this.name.value}" ¿Desea continuar?`
      }
    });

    if (this.id) {
      const course = {
        id: this.id,
        nombre: this.name.value,
        descripcion: this.description.value,
        idSector: this.sector.value,
        progreso: undefined,
        dioFeedback: undefined
      };

      dialogRef.afterClosed().subscribe(data => {
        if (data) {
          this.overlayService.displayLoadingOverlay();
          setTimeout(() => {
            this.overlayService.hideLoadingOverlay();
            this.coursesService.updateCourse(course).subscribe(response => {
              if (response.ok !== false) {
                this.errorMessage.next('');
                this.back();
              } else {
                this.errorMessage.next(response.message);
              }
            });
          }, 1500);
        }
      });
    } else {
      const newCourse = {
        nombre: this.name.value,
        descripcion: this.description.value,
        idSector: this.sector.value,
        progreso: 0,
        dioFeedback: false 
      };

      dialogRef.afterClosed().subscribe(data => {
        if (data) {
          this.overlayService.displayLoadingOverlay();
          setTimeout(() => {
            this.overlayService.hideLoadingOverlay();
            this.coursesService.addCourse(newCourse).subscribe(response => {
              // Si viene un 200, no llega response.ok
              if (response.ok !== false) {
                this.errorMessage.next('');

                this.levelsService.levelUp(this.currentUser.id, 200).subscribe(r => {
                  // solo quiero que le sume experiencia
                  // desde el svr no tengo el id del que lo creo
                });

                this.router.navigate([`courses/${response}/sections`]);
              } else {
                this.errorMessage.next(response.message);
              }
            });
          }, 1500);
        }
      });
    }
  }

  back() {
    this.router.navigate(['courses']);
  }
}
