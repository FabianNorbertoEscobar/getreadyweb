import { NgModule } from '@angular/core';
import { NewCourseComponent } from './new-course/new-course.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { LevelsService } from 'src/app/services/levels.service';

const routes: Routes = [
  {
      path: '',
      component: NewCourseComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ReactiveFormsModule,
  ],
  exports: [],
  declarations: [
    NewCourseComponent,
  ],
  providers: [LevelsService],
})
export class NewCourseModule { }
