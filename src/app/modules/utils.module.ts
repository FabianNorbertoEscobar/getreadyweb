import { NgModule } from '@angular/core';
import { CounterComponent } from '../components/counter/counter.component';
import { SharedModule } from './shared/shared.module';

const components = [
    CounterComponent
]

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: components,
    exports: components,
})
export class UtilsModule {}