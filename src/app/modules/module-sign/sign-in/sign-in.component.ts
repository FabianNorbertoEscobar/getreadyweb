import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { ColorThemeService, ColorTheme } from 'src/app/services/theme.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';
import { OverlayService } from 'src/app/services/overlay.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  host: {
    'class': 'full-height'
  }
})
export class SignInComponent implements OnInit {
  loginForm: FormGroup;
  hidePass = true;

  private invalidLogin: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);
  public invalidLogin$: Observable<Boolean> = this.invalidLogin.asObservable(); 

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private themeService: ColorThemeService,
    private overlayService: OverlayService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });

    setTimeout(() => {
      this.themeService.setColorTheme(ColorTheme.APP_LIGHT);
    });
  }

  togglePasswordVissibility(event) {
    event.preventDefault();
    event.stopPropagation();
    this.hidePass = !this.hidePass;
  }

  public signUp(): void {
    let url = this.router.createUrlTree(['/sign/up']).toString();
    window.open(url, '_blank');
  }

  public forgotPassword(): void {
    this.router.navigate(['sign/change']);
  }

  public submit(): void {
    this.overlayService.displayLoadingOverlay();

    this.invalidLogin.next(false);
    const { username, password } = this.loginForm.value;
    let obs = this.authService.login(username, password);
    obs.pipe(take(1)).subscribe(
      response => {
      this.overlayService.hideLoadingOverlay();
      this.invalidLogin.next(!response.validCredentials);  
      }, 
      error => {
        // por algun error no atrapado
        this.overlayService.hideLoadingOverlay();
      });
    obs.connect();
  }
}
