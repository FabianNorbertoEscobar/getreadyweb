import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { DialogInfoComponent } from 'src/app/components/dialog/dialog-info/dialog-info.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in-change-password',
  templateUrl: './sign-in-change-password.component.html',
  styleUrls: ['./sign-in-change-password.component.scss']
})
export class SignInChangePasswordComponent implements OnInit {
  private errorMessage = new BehaviorSubject<string>("");
  public errorMessage$ = this.errorMessage.asObservable();
  public value: string = "";

  constructor(private authService: AuthService,
    private dialog: MatDialog,
    private router: Router) { }

  private onSuccessful() {
    const dialogRef: MatDialogRef<DialogInfoComponent> = this.dialog.open(DialogInfoComponent, {
      data: {
        title: 'Reestablecer contraseña',
        description: 'La nueva contraseña fue enviada al correo electrónico.'
      }
    });
    
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.router.navigate(["sign/in"]); 
      }
    });
  }

  public ngOnInit(): void {
  }

  public sendMail(): void {
    let regExp = new RegExp("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$");
    if (regExp.test(this.value)) {
      this.errorMessage.next("");
      this.authService.resetPassword(this.value).subscribe(r => {
        if (r.ok) {
          this.onSuccessful();
        } else {
          this.errorMessage.next(r.message);
        }
      });
    } else {
      this.errorMessage.next("Ingrese un correo electrónico válido.");
    }
  }
}
