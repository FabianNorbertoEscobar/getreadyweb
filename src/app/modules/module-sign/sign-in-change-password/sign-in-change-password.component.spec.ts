import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignInChangePasswordComponent } from './sign-in-change-password.component';

describe('SignInChangePasswordComponent', () => {
  let component: SignInChangePasswordComponent;
  let fixture: ComponentFixture<SignInChangePasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignInChangePasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInChangePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
