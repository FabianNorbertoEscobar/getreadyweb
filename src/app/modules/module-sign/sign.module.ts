import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInChangePasswordComponent } from './sign-in-change-password/sign-in-change-password.component';

const routes: Routes = [
    {
        path: 'in',
        component: SignInComponent
    },
    {
        path: 'up',
        component: SignUpComponent,
    },
    {
        path: 'change',
        component: SignInChangePasswordComponent
    },
    {
        path: '',
        redirectTo: 'in',
        pathMatch: 'full',
    }
]

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        ReactiveFormsModule,
    ],
    declarations: [
    SignInComponent,
    SignUpComponent,
    SignInChangePasswordComponent],
    providers: [],
})
export class SignModule {}