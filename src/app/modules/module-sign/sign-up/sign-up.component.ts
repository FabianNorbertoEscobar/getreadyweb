import { Component, OnInit } from '@angular/core';
import { ColorThemeService, ColorTheme } from 'src/app/services/theme.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { BehaviorSubject } from 'rxjs';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { DialogInfoComponent } from 'src/app/components/dialog/dialog-info/dialog-info.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  public signupForm: FormGroup;
  public hidePass: boolean = true;
  public limitDate = new Date();

  private errorMessage = new BehaviorSubject<string>("");
  public errorMessage$ = this.errorMessage.asObservable();

  constructor(
    private formBuilder: FormBuilder,
    private themeService: ColorThemeService,
    private authService: AuthService,
    private dialog: MatDialog,
    private router: Router
  ) 
  { }

  private onSuccessfulSubmit() {
    const dialogRef: MatDialogRef<DialogInfoComponent> = this.dialog.open(DialogInfoComponent, {
      data: {
        title: 'Registro',
        description: 'El registro finalizó con éxito. Ya puede ingresar al sistema.'
      }
    });
    
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.router.navigate(['sign/in']); 
      }
    });
  }

  public ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      email: ['', [
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]
      ],
      organizationName: ['', [Validators.required]],
      organizationQuantity: ['', [Validators.required]]
    });

    setTimeout(() => {
      this.themeService.setColorTheme(ColorTheme.APP_LIGHT);
    });

    this.limitDate = new Date();
  }

  public togglePasswordVissibility(event: any): void {
    event.preventDefault();
    event.stopPropagation();
    this.hidePass = !this.hidePass;
  }

  public submit() {
    const value = this.signupForm.value;
    let obs = this.authService.signUp(value);
    obs.subscribe(response => {
      if (response.ok) {
        this.errorMessage.next("");
        this.onSuccessfulSubmit();
      } else {
        this.errorMessage.next(response.message);
      }
    })
  }
}
