import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewOrganizationComponent } from './view-organization/view-organization.component';
import { EditOrganizationComponent } from './edit-organization/edit-organization.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { UtilsModule } from '../utils.module';
import { ReactiveFormsModule } from '@angular/forms';
import { OrganizationResolver } from './resolvers/organization.resolver';

const routes: Routes = [
  {
      path: '',
      component: ViewOrganizationComponent,
      resolve: { pageData: OrganizationResolver }
  },
  {
      path: 'edit',
      component: EditOrganizationComponent,
  }
]

@NgModule({
  declarations: [ViewOrganizationComponent, EditOrganizationComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    UtilsModule,
    ReactiveFormsModule
  ],
  providers: [OrganizationResolver]
})
export class OrganizationModule { }
