import { Component, OnInit } from '@angular/core';
import { OrganizationService } from 'src/app/services/organization.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { User } from 'src/app/services/user';
import { UserService } from 'src/app/services/user.service';
import { Organization } from 'src/app/interfaces/organization.interface';

@Component({
  selector: 'app-view-organization',
  templateUrl: './view-organization.component.html',
  styleUrls: ['./view-organization.component.scss']
})
export class ViewOrganizationComponent implements OnInit {
  currentUser: User = null;
  organization: Organization = null;

  constructor(
    private router: Router,
    private sessionService: SessionService,
    private activatedRoute: ActivatedRoute
  ) {
      this.organization = this.activatedRoute.snapshot.data.pageData.organizacion;
      this.currentUser = this.sessionService.getCurrentUser();
   }

  ngOnInit(): void {
    
  }
  
  edit() {
    this.router.navigate(['organization/edit'], { queryParams: { id: this.organization.id } });
  }

}
