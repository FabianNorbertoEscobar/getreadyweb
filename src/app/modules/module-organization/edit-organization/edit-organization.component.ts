import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Location } from '@angular/common';
import { OrganizationService } from 'src/app/services/organization.service';

@Component({
  selector: 'app-edit-organization',
  templateUrl: './edit-organization.component.html',
  styleUrls: ['./edit-organization.component.scss']
})
export class EditOrganizationComponent implements OnInit {

  public form: FormGroup;
  private id: number = -1;
  private errorMessage = new BehaviorSubject<string>("");
  public errorMessage$ = this.errorMessage.asObservable();

  constructor(
    private formBuilder: FormBuilder,
    private location: Location,
    private organizationService: OrganizationService,
    private route: ActivatedRoute
  ) { 
    this.route.queryParams.subscribe(params => {
    this.id = params['id'];
    });
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      nombre: [''],
      descripcion: [''],
      tipo: [''],
      especialidad: [''],
      financiacion: [''],
      cantidadAproxEmpleados: [''],
      fechaRegistro: [''],
      fundacion: [''],
      ubicacion: [''],
      website: [''],
      email: [''],
      linkedin: [''],
      facebook: [''],
      twitter: [''],
      instagram: [''],
      telefono: ['']
    }); 
    this.organizationService.get(this.id)
    .subscribe(organization => {
      this.form.get('nombre').setValue(organization.nombre);
      this.form.get('descripcion').setValue(organization.descripcion);
      this.form.get('tipo').setValue(organization.tipo);
      this.form.get('especialidad').setValue(organization.especialidad);
      this.form.get('financiacion').setValue(organization.financiacion);
      this.form.get('cantidadAproxEmpleados').setValue(organization.cantidadAproxEmpleados);
      this.form.get('fundacion').setValue(organization.fundacion);
      this.form.get('ubicacion').setValue(organization.ubicacion);
      this.form.get('website').setValue(organization.website);
      this.form.get('email').setValue(organization.email);
      this.form.get('linkedin').setValue(organization.linkedin);
      this.form.get('facebook').setValue(organization.facebook);
      this.form.get('twitter').setValue(organization.twitter);
      this.form.get('instagram').setValue(organization.instagram);
      this.form.get('telefono').setValue(organization.telefono);
    });
    
  }

  public submit() {
    const value = this.form.value;
    value.id = this.id;
    let obs = this.organizationService.edit(value);
    obs.subscribe(response => {
      if (response.ok) {
        this.errorMessage.next("");
        this.location.back();
      } else {
        this.errorMessage.next(response.message);
      }
    })
  }

  public back() {
    this.location.back();
  }
}
