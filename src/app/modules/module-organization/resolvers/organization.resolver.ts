import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';

@Injectable()
export class OrganizationResolver implements Resolve<any> {
    
    constructor(private sessionService: SessionService,
        private userService: UserService) {}
    
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.userService.getUserById(this.sessionService.getCurrentUser().id);
    }
    
}