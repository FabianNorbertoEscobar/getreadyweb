import { Component, OnInit, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { Question, Answer } from '../module-new-game/trivia/trivia.component';
import { CounterComponent } from 'src/app/components/counter/counter.component';
import { AnswerCardComponent } from './answer-card/answer-card.component';
import { ActivatedRoute } from '@angular/router';
import { TriviasService } from 'src/app/services/trivias.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { SessionService } from 'src/app/services/session.service';
import { AchievementsSharedService } from 'src/app/services/achievements-shared.service';
import { Achievement } from 'src/app/modules/module-achievements/utils/achievement.utils';
import { AchievementsService } from '../../module-achievements/services/achievements.service';
import { OverlayService } from 'src/app/services/overlay.service';

export interface Trivia {
  title: string;
  description: string;
  questions: Question[];
}

export const TRIVIA_TIMER = 10;

@Component({
  selector: 'app-play-trivia',
  templateUrl: './play-trivia.component.html',
  styleUrls: ['./play-trivia.component.scss'],
  host: {
    'class': 'full-height'
  }
})
export class PlayTriviaComponent implements OnInit {
  private id: number;
  private idUser: number;
  private gameState: BehaviorSubject<string> = new BehaviorSubject<string>("menu");
  
  public trivia: Trivia;
  public gameState$: Observable<string> = this.gameState.asObservable(); // menu, started, finished
  
  public timer = TRIVIA_TIMER;

  public currentQuestionIndex: number = 0;

  @ViewChild('counter', { static: false, read: CounterComponent })
  public counter: CounterComponent;
  @ViewChildren('answercard', { read: AnswerCardComponent })
  public answerCardsQueryList: QueryList<AnswerCardComponent>;

  public finalScore: number = 0;

  constructor(
    private route: ActivatedRoute,
    private sessionService: SessionService,
    private triviaService: TriviasService,
    private achievementsService: AchievementsService,
    private achievementsSharedService: AchievementsSharedService,
    private overlayService: OverlayService,
  ) {
    
      this.route.queryParams.subscribe(params => {
        this.id = params['_id'];
      });

      this.idUser = this.sessionService.getCurrentUser().id;
  }

  ngOnInit(): void {
    this.triviaService.get(this.id).subscribe(r => {
      if (r.ok === undefined || r.ok) {
        this.trivia = r;
      }
    });

    this.gameState.subscribe(state => {
      if (state == "finished") {
        let value = {
          idUsuario: this.idUser,
          idTrivia: this.id,
          puntaje: this.finalScore
        }
        this.triviaService.finished(value).subscribe(r => {
           //pa que me haga el connect
        });
      }
    })
  }

  startGame() {
    this.gameState.next('started');
    this.counter.start();
  }

  nextQuestion() {
    let answerCards: AnswerCardComponent[] = this.answerCardsQueryList['_results'];

    for (let i = 0 ; i < answerCards.length ; i++) {
      if (answerCards[i].answer.correct) {
        answerCards[i].highlight = true;
      }
      answerCards[i].clickable = false;
    }

    setTimeout(() => {
      if ((this.currentQuestionIndex + 1) < this.trivia.questions.length) {
        this.currentQuestionIndex++;
        this.counter.reset();
        this.counter.start();
      } else {
        this.gameState.next('finished');
        this.achievementsSharedService.grantAchievementToUser("Noob");
        this.achievementsSharedService.grantAchievementToUser(this.trivia.title);
      }
    }, 2000);
  }

  answerQuestion(answer: Answer) {
    this.counter.stop();

    if (answer.correct) {
      this.finalScore += 10 * this.counter.currentCount;
    }

    this.nextQuestion();
  }

  playAgain() {
    this.currentQuestionIndex = 0;
    this.finalScore = 0;
    this.counter.reset();
    this.startGame();
  }

  getGameState() {
    return this.gameState.value;
  }
}
