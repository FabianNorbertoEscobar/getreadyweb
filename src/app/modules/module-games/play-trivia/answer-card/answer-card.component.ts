import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Answer } from '../../module-new-game/trivia/trivia.component';

@Component({
  selector: 'app-answer-card',
  templateUrl: './answer-card.component.html',
  styleUrls: ['./answer-card.component.scss']
})
export class AnswerCardComponent implements OnInit {

  @Input()
  answer: Answer;
  highlight: boolean = false;
  clickable: boolean = true;

  @Output()
  clicked: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  onClick() {
    this.highlight = true;
    this.clicked.emit(true);
  }
}
