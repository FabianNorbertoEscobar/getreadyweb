import { Component, OnInit } from '@angular/core';
import { TriviaStats } from 'src/app/interfaces/game-stats-db.interface';
import { SessionService } from 'src/app/services/session.service';
import { User } from 'src/app/services/user';
import { orderBy } from 'lodash';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-game-statistics',
  templateUrl: 'game-statistics.component.html',
  styleUrls: ['game-statistics.component.scss']
})

export class GameStatisticsComponent implements OnInit {
  triviasCount: number;
  acertijosCount: number;
  mostPlayedTrivia: { nombre: string, vecesJugada: number };
  mostPlayedAcertijo: { nombre: string, vecesJugado: number };
  triviaRankings: Array<TriviaStats>;
  currentUser: User;
  colorsArray = ['primary', 'accent', 'warn', 'basic'];

  constructor(
    private sessionService: SessionService,
    private activatedRoute: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.currentUser = this.sessionService.getCurrentUser();
    let stats = this.activatedRoute.snapshot.data.pageData;
    this.triviasCount = stats.trivias.length;
    this.acertijosCount = stats.acertijos.length;
    this.mostPlayedTrivia = stats.triviaMasJugada;
    this.mostPlayedAcertijo = stats.acertijoMasJugado;
    this.triviaRankings = stats.trivias.map((trivia: TriviaStats) =>
      ({ ...trivia, userScores: orderBy(trivia.userScores, ['maxScore', 'nombre'], ['desc', 'asc'])}));
  }

  back() {
    this.location.back();
  }
}
