import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListHistoricGameComponent } from './list-historic-game.component';

describe('ListHistoricGameComponent', () => {
  let component: ListHistoricGameComponent;
  let fixture: ComponentFixture<ListHistoricGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListHistoricGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListHistoricGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
