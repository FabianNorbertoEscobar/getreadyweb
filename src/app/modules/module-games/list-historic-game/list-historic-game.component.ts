import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Location } from '@angular/common'; 
import { ActivatedRoute } from '@angular/router';


export interface GameHistoric {
  username: string;
  typeAction: string;
  game: string;
  formattedDate: string;
}

@Component({
  selector: 'app-list-historic-game',
  templateUrl: './list-historic-game.component.html',
  styleUrls: ['./list-historic-game.component.scss']
})
export class ListHistoricGameComponent implements OnInit {
  public dataSource: MatTableDataSource<GameHistoric> = new MatTableDataSource<GameHistoric>();

  constructor(private activatedRoute: ActivatedRoute,
    private _location: Location) { 
    this.dataSource.data = this.activatedRoute.snapshot.data.pageData;
  }

  public ngOnInit() { }

  public back() {
    this._location.back(); 
  }

}
