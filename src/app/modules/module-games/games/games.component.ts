import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Game } from '../game-card/game-card.component';
import { Router, ActivatedRoute } from '@angular/router';
import { GamesService } from 'src/app/services/games.service';
import { SessionService } from 'src/app/services/session.service';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { PermissionsService } from 'src/app/services/permissions.service';
import { User } from 'src/app/services/user';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

export interface GameFilter {
  description: string;
  filter(games: Game): boolean;
}

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})
export class GamesComponent implements OnInit {
  currentUser: User;

  games: Game[] = [];
  removable = true;

  filteredGames: Game[] = [];

  filterControl = new FormControl();
  filteredFilters: Observable<GameFilter[]>;
  filters: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
  allFilters: GameFilter[] = [];
  
  @ViewChild('filterInput')
  filterInput: ElementRef<HTMLInputElement>;
  separatorKeysCodes: number[] = [ENTER, COMMA];

  isMobile = false;
  
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private gamesService: GamesService,
    private sessionService: SessionService,
    private permissionService: PermissionsService,
    private breakpointObserver: BreakpointObserver,
  ) {
    this.currentUser = this.sessionService.getCurrentUser();
    this.games = this.activatedRoute.snapshot.data.pageData;
  }

  ngOnInit(): void {  
      this.games = this.games.filter(g => {
        const id_sector = this.currentUser.id_sector;
        if (id_sector) {
          return g.idSector == id_sector;
        }
        return true;
      });

      this.games.filter(game => game.type.code == "trivia").forEach(game => {
        game.type.route = "trivia";
        game.type.iconSrc = "../../../../assets/interview.png";
      });

      this.games.filter(game => game.type.code == "acertijo").forEach(game => {
        game.type.route = "acertijo";
        game.type.iconSrc = "../../../../assets/riddle.png";
      });

      this.filteredGames = [...this.games];
      this._setUpFilters();
      this.filteredFilters = this.filterControl.valueChanges.pipe(
        startWith(null),
        map((filter: GameFilter | null) => filter ? (filter.description ? filter.description : filter ) : null),
        map((filter: string | null) => filter ? this._filter(filter) : this.allFilters.slice()),
      );

      this.filters.subscribe(filters => {
        this.applyFilters();
      });

      this.breakpointObserver.observe(['(max-width: 599px)']).subscribe((state: BreakpointState) => {
        this.isMobile = state.matches;
      });
  }

  canCreateGame(): boolean {
    return this.permissionService.canCreateOrEditContent();
  }

  newGame() {
    this.router.navigate(['new-game'], { relativeTo: this.activatedRoute });
  }

  /**
   * Filtra el listado de filtros de acuerdo al valor introducido
   * @param {string}  value  el valor
   */
  private _filter(value: string): GameFilter[] {
    const filterValue = value.toLowerCase();
    return this.allFilters.filter(filter => filter.description.toLowerCase().indexOf(filterValue) === 0);
  }

  /**
   * Aplica los filtros y el valor de búsqueda al listado de juegos
   */
  applyFilters(): void {
    let currentFilters: GameFilter[] = this.allFilters.filter(filter => this.filters.value.includes(filter.description));

    // Aplica filtros
    if (this.filters.value.length) {
      this.filteredGames = [];

      for (let game of this.games) {
        for (let filter of currentFilters) {
          if (filter.filter(game)) {
            this.filteredGames.push(game);
          }
        }
      }
    } else {
        this.filteredGames = [...this.games];
    }
  }

  /**
   * Inicializa los filtros a usar
   */
  private _setUpFilters() {
    this.allFilters.push(
      {
        description: 'Jugados',
        filter: (game: Game) => { return game.played; }
      },
      {
        description: 'Sin jugar',
        filter: (game: Game) => { return !game.played; }
      }
    );
  }

  /**
   * Maneja el evento de selección de un filtro
   * @param {MatAutocompleteSelectedEvent} event el evento
   */
  selected(event: MatAutocompleteSelectedEvent): void {
    let isIncluded: boolean = false;

    for (let i = 0 ; i < this.filters.value.length ; i++) {
      if (this.filters.value[i] == event.option.viewValue) {
        isIncluded = true;
        break;
      }
    }

    if (!isIncluded) {
      this.filters.value.push(event.option.viewValue);
      this.filters.next(this.filters.value);
    }

    this.filterInput.nativeElement.value = '';
    this.filterControl.setValue(null);
  }

  /**
   * Quita el filtro de la lista de filtros aplicados
   * @param {string} filter el filtro
   */
  remove(filter: string): void {
    const index = this.filters.value.indexOf(filter);

    if (index >= 0) {
      this.filters.value.splice(index, 1);
      this.filters.next(this.filters.value);
    }
  }

  seeHistoric() {
    this.router.navigate(['/games/history']);
  }

  goToStatsPage() {
    this.router.navigate(['/games/stats']);
  }
}
