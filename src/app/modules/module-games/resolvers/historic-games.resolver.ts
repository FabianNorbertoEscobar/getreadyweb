import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { GamesService } from 'src/app/services/games.service';

@Injectable()
export class HistoricGamesResolver implements Resolve<any> {
    
    constructor(private gamesService: GamesService) { }
    
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.gamesService.getHistoric();
    }

}