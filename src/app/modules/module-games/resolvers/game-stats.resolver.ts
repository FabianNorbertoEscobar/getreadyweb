import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { GamesService } from 'src/app/services/games.service';
import { SessionService } from 'src/app/services/session.service';

@Injectable()
export class GameStatsResolver implements Resolve<any> {
    
    constructor(private gamesService: GamesService,
        private sessionService: SessionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.gamesService.getStats(this.sessionService.getCurrentUser().id_sector || 0);
    }

}