import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { PermissionsService } from 'src/app/services/permissions.service';

export interface GameType {
  // se asigna en back-end
  code: string;

  // se asignan/usan desde front-end
  iconSrc: string;
  route: string;
}

export interface Game {
  _id: string;
  title: string;
  description: string;
  type: GameType;
  played: boolean;
  idSector: number;
  maxScore: number;
}

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.scss']
})
export class GameCardComponent implements OnInit {
  @Input()
  game: Game;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private permissionService: PermissionsService
  ) { }

  ngOnInit(): void {
  }

  play() {
    this.router.navigate([`play/${this.game.type.route}`], { queryParams: {_id: this.game._id}, relativeTo: this.activatedRoute });
  }

  canEditGame(): boolean {
    return this.permissionService.canCreateOrEditContent();
  }
  
  edit() {
    this.router.navigate([`new-game/${this.game.type.route}`], { queryParams: {_id: this.game._id}, relativeTo: this.activatedRoute });
  }
}
