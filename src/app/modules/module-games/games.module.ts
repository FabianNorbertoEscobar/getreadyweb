import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { GamesComponent } from './games/games.component';
import { Routes, RouterModule } from '@angular/router';
import { GameCardComponent } from './game-card/game-card.component';
import { PlayTriviaComponent } from './play-trivia/play-trivia.component';
import { UtilsModule } from '../utils.module';
import { AnswerCardComponent } from './play-trivia/answer-card/answer-card.component';
import { GamesService } from 'src/app/services/games.service';
import { TriviasService } from 'src/app/services/trivias.service';
import { ReactiveFormsModule } from '@angular/forms';
import { ListHistoricGameComponent } from './list-historic-game/list-historic-game.component';
import { AchievementsService } from '../module-achievements/services/achievements.service';
import { PlayAcertijoComponent } from './play-acertijo/play-acertijo.component';
import { AcertijoService } from 'src/app/services/acertijo.service';
import { GamesResolver } from './resolvers/games.resolver';
import { HistoricGamesResolver } from './resolvers/historic-games.resolver';
import { GameStatisticsComponent } from './game-statistics/game-statistics.component';
import { GameStatsResolver } from './resolvers/game-stats.resolver';

const routes: Routes = [
    {
        path: '',
        component: GamesComponent,
        resolve: { pageData: GamesResolver }
    },
    {
        path: 'new-game',
        loadChildren: () => (import('./module-new-game/new-game.module')).then(m => m.NewGameModule),
    },
    {
        path: 'play/trivia',
        component: PlayTriviaComponent,
    },
    {
        path: 'play/acertijo',
        component: PlayAcertijoComponent
    },
    {
        path: 'history',
        component: ListHistoricGameComponent,
        resolve: { pageData: HistoricGamesResolver }
    },
    {
        path: 'stats',
        component: GameStatisticsComponent,
        resolve: { pageData: GameStatsResolver }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        UtilsModule,
        ReactiveFormsModule
    ],
    declarations: [
        GamesComponent,
        GameCardComponent,
        PlayTriviaComponent,
        PlayAcertijoComponent,
        AnswerCardComponent,
        ListHistoricGameComponent,
        GameStatisticsComponent
    ],
    providers: [
        GamesService,
        GamesResolver,
        HistoricGamesResolver,
        TriviasService,
        AchievementsService,
        AcertijoService,
        GameStatsResolver
    ]
})
export class GamesModule {}
