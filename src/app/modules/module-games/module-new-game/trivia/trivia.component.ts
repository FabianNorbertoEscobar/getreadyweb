import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogQuestionComponent, DialogQuestionData } from './dialog-question/dialog-question.component';
import { Router, ActivatedRoute } from '@angular/router';
import { OverlayService } from 'src/app/services/overlay.service';
import { DialogConfirmationComponent } from 'src/app/components/dialog/dialog-confirmation/dialog-confirmation.component';
import { TriviasService } from 'src/app/services/trivias.service';
import { SessionService } from 'src/app/services/session.service';
import { SectorService } from 'src/app/services/sector.service';

export interface Question {
  question: string;
  answers: Answer[];
}

export interface Answer {
  answer: string;
  correct: boolean;
}

@Component({
  selector: 'app-trivia',
  templateUrl: './trivia.component.html',
  styleUrls: ['./trivia.component.scss']
})
export class TriviaComponent implements OnInit {
  // si está undefined es nueva trivia
  private id: number;

  public triviaForm: FormGroup;
  public questions: Question[] = [];
  public sectorList: any[] = new Array<any>(); // value, description

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private overlayService: OverlayService,
    private triviaService: TriviasService,
    private sessionService: SessionService,
    private sectorService: SectorService
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params['_id'];
    });

    this.triviaForm = new FormBuilder().group({
      title: ['', [ Validators.required ]],
      sector: [{ value: '', disabled: this.sessionService.getCurrentUser().roleType != "ADMIN" }, [Validators.required]],
      description: ['', [ Validators.required ]],
    });
    this.initializeSectoresList();
  }

  private initializeSectoresList(): void {
    let currentUser = this.sessionService.getCurrentUser();
    this.sectorService.findAllByUser(currentUser.id).subscribe(r => {
      for (let sector of r) {
        let selectSector = {
          value: sector.id,
          description: sector.nombre
        }
        this.sectorList.push(selectSector);
      }
    });

    if (currentUser.id_sector) {
      this.triviaForm.get('sector').setValue(currentUser.id_sector);
    }
  }

  ngOnInit(): void {
    if (this.id == undefined) {
      // está creando, cargo las defecto
      this.questions.push(
        {
          question: "¿Pregunta 1?",
          answers: [
            {
              answer: "Respuesta 1",
              correct: false,
            },
            {
              answer: "Respuesta 2",
              correct: true,
            },
            {
              answer: "Respuesta 3",
              correct: false,
            },
            {
              answer: "Respuesta 4",
              correct: false,
            },
          ]
        },
      );
    } else {
      // está editando
      this.triviaService.get(this.id).subscribe(r => {
        if (r.ok === undefined || r.ok) {
          this.triviaForm.get('title').setValue(r.title);
          this.triviaForm.get('description').setValue(r.description);
          this.triviaForm.get('sector').setValue(r.sector);
          this.questions = r.questions;
        }
      })
    }
  }

  addQuestion() {
    const dialogRef: MatDialogRef<DialogQuestionComponent> = this.dialog.open(DialogQuestionComponent, {
      data: {
        title: 'Nueva pregunta',
        question: {
          question: '',
          answers: [
            {
              answer: "",
              correct: false,
            },
            {
              answer: "",
              correct: false,
            },
            {
              answer: "",
              correct: false,
            },
            {
              answer: "",
              correct: false,
            },
          ]
        },
      },
      width: "600px",
      height: "550px",
    });

    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.questions.push(data.question);
      }
    });
  }

  editQuestion(question: Question) {
    let questionIndex: number = this.questions.indexOf(question);

    this.dialog.open(DialogQuestionComponent, {
      data: {
        title: 'Editar pregunta',
        question: question,
      },
      width: "600px",
      height: "550px",
    }).afterClosed().subscribe((result: DialogQuestionData) => {
      if (result) {
        this.questions[questionIndex] = result.question
      }
    });
  }

  deleteQuestion(question: Question) {
    this.questions.splice(this.questions.indexOf(question), 1);
  }

  done() {
    let descriptionDialog = this.id != undefined ? 'Se actualizará la trivia' : 'Se creará una nueva trivia';
    descriptionDialog += ' ¿Desea continuar?'
    const dialogRef: MatDialogRef<DialogConfirmationComponent> = this.dialog.open(DialogConfirmationComponent, {
      data: {
        title: 'Confirmar trivia',
        description: descriptionDialog
      }
    });

    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.overlayService.displayLoadingOverlay();
        let trivia = this.triviaForm.getRawValue();
        trivia.idUser = this.sessionService.getCurrentUser().id;
        trivia.questions = this.questions;
        if (this.id == undefined) {
          this.triviaService.add(trivia).subscribe(r => {
            if (r.ok) {
              this.router.navigate(["games"]);
            }
          });
        } else {
          this.triviaService.update(this.id, trivia).subscribe(r => {
            if (r.ok) {
              this.router.navigate(["games"]);
            }
          });
        }
        
      }
    });
  }
}
