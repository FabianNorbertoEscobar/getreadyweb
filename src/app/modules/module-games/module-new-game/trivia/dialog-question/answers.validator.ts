import { FormGroup, ValidatorFn } from "@angular/forms";

export const AnswersValidator: ValidatorFn = 

    (control: FormGroup): {[key: string]: boolean} | null => {

        for (let controlKey of Object.keys(control.controls)) {
            if (control.get(controlKey).get('correct').value) {
                return null;
            }
        }

        return {'noTrueAnswer': true};
    };