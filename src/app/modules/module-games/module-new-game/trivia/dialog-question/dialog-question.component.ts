import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Question } from '../trivia.component';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AnswersValidator } from './answers.validator';
import { Observable, merge } from 'rxjs';
import { map } from 'rxjs/operators';

export interface DialogQuestionData {
  title: string;
  question: Question;
}

@Component({
  selector: 'app-dialog-question',
  templateUrl: './dialog-question.component.html',
  styleUrls: ['./dialog-question.component.scss']
})
export class DialogQuestionComponent implements OnInit {

  public questionForm: FormGroup;
  public questionFormAnswers: FormGroup;
  public objectRef = Object;

  constructor(
    public dialogRef: MatDialogRef<DialogQuestionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogQuestionData,
  ) {
    this.questionForm = new FormBuilder().group({
      question: [this.data.question.question, [Validators.required]],
      answers: new FormBuilder().group({

        answer1: new FormBuilder().group({
          answer: [this.data.question.answers[0].answer, [Validators.required]],
          correct: [this.data.question.answers[0].correct, [Validators.required]],
        }),
        answer2: new FormBuilder().group({
          answer: [this.data.question.answers[1].answer, [Validators.required]],
          correct: [this.data.question.answers[1].correct, [Validators.required]],
        }),
        answer3: new FormBuilder().group({
          answer: [this.data.question.answers[2].answer, [Validators.required]],
          correct: [this.data.question.answers[2].correct, [Validators.required]],
        }),
        answer4: new FormBuilder().group({
          answer: [this.data.question.answers[3].answer, [Validators.required]],
          correct: [this.data.question.answers[3].correct, [Validators.required]],
        }),

      }),
    });

    this.questionForm.get('answers').setValidators(AnswersValidator);

    this.questionFormAnswers = <FormGroup>this.questionForm.get('answers');
  }

  ngOnInit() {
    // Cuando se selecciona un checkbox, se tienen que deseleccionar todos los demas
    let correctAnswers: Observable<any>;

    correctAnswers = merge(
      this.questionFormAnswers.get('answer1').get('correct').valueChanges.pipe(map(value => value = {correct: value, control: 'answer1'})),
      this.questionFormAnswers.get('answer2').get('correct').valueChanges.pipe(map(value => value = {correct: value, control: 'answer2'})),
      this.questionFormAnswers.get('answer3').get('correct').valueChanges.pipe(map(value => value = {correct: value, control: 'answer3'})),
      this.questionFormAnswers.get('answer4').get('correct').valueChanges.pipe(map(value => value = {correct: value, control: 'answer4'})),
    );

    correctAnswers.subscribe(value => {
      if (value.correct) {
        for (let controlKey of Object.keys(this.questionFormAnswers.controls)) {
          if (controlKey != value.control) {
            this.questionFormAnswers.get(controlKey).get('correct').setValue(false);
          }
        }
      }
    });
  }

  save() {
    this.data.question.question = this.questionForm.get('question').value;
    for (let i = 0 ; i < 4 ; i++) {
      this.data.question.answers[i] = this.questionFormAnswers.get(`answer${i+1}`).value;
    }

    this.dialogRef.close(this.data);
  }

  close() {
    this.dialogRef.close();
  }
}
