import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

export interface NewGame {
  title: string;
  description: string;
  iconSrc: string;
  route: string;
}

@Component({
  selector: 'app-new-game-card',
  templateUrl: './new-game-card.component.html',
  styleUrls: ['./new-game-card.component.scss']
})
export class NewGameCardComponent implements OnInit {

  @Input()
  newGame: NewGame;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
  }

  goToRoute() {
    this.router.navigate([`${this.newGame.route}`], { relativeTo: this.activatedRoute });
  }
}
