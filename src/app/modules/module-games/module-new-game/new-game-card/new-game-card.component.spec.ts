import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewGameCardComponent } from './new-game-card.component';

describe('NewGameCardComponent', () => {
  let component: NewGameCardComponent;
  let fixture: ComponentFixture<NewGameCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewGameCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewGameCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
