import { Component, OnInit } from '@angular/core';
import { NewGame } from '../new-game-card/new-game-card.component';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.scss']
})
export class NewGameComponent implements OnInit {

  newGames: NewGame[] = []

  constructor() {
    this.newGames.push(
      {
        title: 'Crear Trivia',
        description: 'Contesta una serie de preguntas con cuatro opciones a elegir, de las cuales sólo una es la correcta ¡Mientras más opciones correctas marque el jugador, más alto será el puntaje final!',
        iconSrc: '../../../../assets/interview.png',
        route: 'trivia',
      },
    ); 

    this.newGames.push(
      {
        title: 'Crear Acertijo',
        description: 'Adivina adivinador... ¿te consideras lo suficientemente creativo como para hacer acertijos? Animate!!',
        iconSrc: '../../../../assets/riddle.png',
        route: 'acertijo',
      }
    );
  }

  ngOnInit(): void {
  }

}
