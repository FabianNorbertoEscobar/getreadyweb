import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { DialogConfirmationComponent } from 'src/app/components/dialog/dialog-confirmation/dialog-confirmation.component';
import { OverlayService } from 'src/app/services/overlay.service';
import { SessionService } from 'src/app/services/session.service';
import { AcertijoService } from 'src/app/services/acertijo.service';
import { SectorService } from 'src/app/services/sector.service';

@Component({
    selector: 'app-acertijo',
    templateUrl: './acertijo.component.html',
    styleUrls: ['./acertijo.component.scss']
  })
  export class AcertijoComponent implements OnInit {
    private id: number;
    public form: FormGroup;
    public sectorList: any[] = new Array<any>(); // value, description

    constructor(private dialog: MatDialog,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private overlayService: OverlayService,
        private sessionService: SessionService,
        private acertijoService: AcertijoService,
        private sectorService: SectorService
    ) {
        this.activatedRoute.queryParams.subscribe(params => {
            this.id = params['_id'];
          });
      
        
        this.form = new FormBuilder().group({
            title: ['', [ Validators.required ]],
            description: ['', [ Validators.required ]],
            acertijo: ['', [ Validators.required ]],
            response: ['', [ Validators.required ]],
            sector: [{ value: '', disabled: this.sessionService.getCurrentUser().roleType != "ADMIN" }, [Validators.required]]
        });
        this.initializeSectoresList();
    }

    private initializeSectoresList(): void {
        let currentUser = this.sessionService.getCurrentUser();
        this.sectorService.findAllByUser(currentUser.id).subscribe(r => {
            for (let sector of r) {
            let selectSector = {
                value: sector.id,
                description: sector.nombre
            }
            this.sectorList.push(selectSector);
            }
        });

        if (currentUser.id_sector) {
            this.form.get('sector').setValue(currentUser.id_sector);
        }
    }    

    ngOnInit(): void {
        if (!!this.id) {
            this.acertijoService.get(this.id).subscribe(r => {
                if (r.ok === undefined || r.ok) {
                    this.form.get('title').setValue(r.title);
                    this.form.get('description').setValue(r.description);
                    this.form.get('acertijo').setValue(r.acertijo);
                    this.form.get('response').setValue(r.response);
                    this.form.get('sector').setValue(r.sector);
                }
            });
        }
    }

    done() {
        let descriptionDialog = this.id != undefined ? 'Se actualizará el acertijo' : 'Se creará un nuevo acertijo';
        descriptionDialog += ' ¿Desea continuar?'
        const dialogRef: MatDialogRef<DialogConfirmationComponent> = this.dialog.open(DialogConfirmationComponent, {
        data: {
            title: 'Confirmar acertijo',
            description: descriptionDialog
        }
        });

        dialogRef.afterClosed().subscribe(data => {
            if (data) {
                this.overlayService.displayLoadingOverlay();
                let acertijo = this.form.getRawValue();
                acertijo.idUser = this.sessionService.getCurrentUser().id;
                if (this.id == undefined) {
                    this.acertijoService.add(acertijo).subscribe(r => {
                        if (r.ok) {
                            this.router.navigate(["games"]);
                        }
                    });
                } else {
                    this.acertijoService.update(this.id, acertijo).subscribe(r => {
                        if (r.ok) {
                            this.router.navigate(["games"]);
                        }
                    });
                }
            }
        });
    }
  }