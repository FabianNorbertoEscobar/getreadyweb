import { NgModule } from "@angular/core";
import { SharedModule } from '../../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { NewGameComponent } from './new-game/new-game.component';
import { NewGameCardComponent } from './new-game-card/new-game-card.component';
import { TriviaComponent } from './trivia/trivia.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DialogQuestionComponent } from './trivia/dialog-question/dialog-question.component';
import { TriviasService } from 'src/app/services/trivias.service';
import { AcertijoComponent } from './acertijo/acertijo.component';
import { AcertijoService } from 'src/app/services/acertijo.service';

const routes: Routes = [
    {
        path: '',
        component: NewGameComponent,
    },
    {
        path: 'trivia',
        component: TriviaComponent,
    },
    {
        path: 'acertijo',
        component: AcertijoComponent
    }
]

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        ReactiveFormsModule,
    ],
    declarations: [
        NewGameComponent,
        NewGameCardComponent,
        TriviaComponent,
        AcertijoComponent,
        DialogQuestionComponent,
    ],
    providers: [
        TriviasService,
        AcertijoService
    ]
})
export class NewGameModule {}