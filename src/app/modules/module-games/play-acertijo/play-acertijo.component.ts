import { OnInit, Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AcertijoService } from 'src/app/services/acertijo.service';
import { ActivatedRoute } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { take } from 'rxjs/operators';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { DialogConfirmationComponent } from 'src/app/components/dialog/dialog-confirmation/dialog-confirmation.component';
import { OverlayService } from 'src/app/services/overlay.service';
import { Location } from '@angular/common'; 
import { DialogInfoComponent } from 'src/app/components/dialog/dialog-info/dialog-info.component';

export interface Acertijo {
    title: string;
    description: string;
    acertijo: string;
    response: string;
}

@Component({
    selector: 'app-play-acertijo',
    templateUrl: './play-acertijo.component.html',
    styleUrls: ['./play-acertijo.component.scss'],
    host: {
        'class': 'full-height'
    }
})
export class PlayAcertijoComponent implements OnInit {
    private id: number;
    private idUser: number;
    
    status: string;
    form: FormGroup;
    acertijo: Acertijo;

    constructor(
        private sessionService: SessionService,
        private acertijoService: AcertijoService,
        private route: ActivatedRoute,
        private overlayService: OverlayService,
        private dialog: MatDialog,
        private _location: Location
    ) {
        this.route.queryParams.subscribe(params => {
            this.id = params['_id'];
        });
        this.idUser = this.sessionService.getCurrentUser().id;
        this.form = new FormBuilder().group({
            response: ['', [ Validators.required ]]
        });
        this.status = "playing";
    }

    ngOnInit(): void {    
        this.acertijoService.get(this.id).pipe(take(1)).subscribe(r => {
            this.acertijo = r;
        });
    }

    done() {
        this.status = "finished";
    }

    quit() {
        let descriptionDialog = '¿Estás seguro que deseas rendirte?';
        const dialogRef: MatDialogRef<DialogConfirmationComponent> = this.dialog.open(DialogConfirmationComponent, {
            data: {
                title: 'Rendición',
                description: descriptionDialog
            }
        });
        dialogRef.afterClosed().pipe(take(1)).subscribe(data => {
            if (data) {
                this.overlayService.displayLoadingOverlay();
                this.acertijoService.quit(this.id, this.idUser).pipe(take(1)).subscribe(r => {
                    this._location.back();
                });                
            }
        });
    }

    acertijoNotOk() {
        const dialogRef: MatDialogRef<DialogInfoComponent> = this.dialog.open(DialogInfoComponent, {
            data: {
                title: 'Uhh :(',
                description: 'Seguro tendrás revancha en otro acertijo ¡Animos!'
            }
        });
        dialogRef.afterClosed().pipe(take(1)).subscribe(data => {
            if (data) {
                this.overlayService.displayLoadingOverlay();
                this.acertijoService.finished(this.id, this.idUser, false, 0).pipe(take(1)).subscribe(r => {
                    this._location.back();
                });                
            }
        });
    }

    acertijoOk() {
        let score = 60;
        const dialogRef: MatDialogRef<DialogInfoComponent> = this.dialog.open(DialogInfoComponent, {
            data: {
                title: 'Iujuu',
                description: `¡Felicitaciones! Sumaste ${score} de experiencia.`
            }
        });
        dialogRef.afterClosed().pipe(take(1)).subscribe(data => {
            if (data) {
                this.overlayService.displayLoadingOverlay();
                this.acertijoService.finished(this.id, this.idUser, true, score).pipe(take(1)).subscribe(r => {
                    this._location.back();
                });                
            }
        });
    }

}
