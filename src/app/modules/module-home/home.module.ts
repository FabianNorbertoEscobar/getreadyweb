import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '../shared/shared.module';
import { SharedAchievementsModule } from '../shared/shared-achievements.module';
import { SharedCoursesModule } from '../shared/shared-courses.module';
import { ReactiveFormsModule } from '@angular/forms';
import { UserChangePasswordComponent } from './user-change-password/user-change-password.component';
import { LevelsService } from 'src/app/services/levels.service';
import { HomeResolver } from './resolvers/home.resolver';
import { SharedPraiseModule } from '../shared/shared-praise.module';
import { NotificationsListComponent } from 'src/app/components/notifications/notifications-list/notifications-list.component';


const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        resolve: { pageData: HomeResolver }
    },
    {
        path: 'change-password',
        component: UserChangePasswordComponent
    },
    {
        path: 'notifications',
        component: NotificationsListComponent
    }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    SharedAchievementsModule,
    SharedCoursesModule,
    ReactiveFormsModule,
    SharedPraiseModule,
  ],
    declarations: [
        HomeComponent,
        UserChangePasswordComponent,
        NotificationsListComponent
    ],
    providers: [
        LevelsService,
        HomeResolver
    ],
})
export class HomeModule {}
