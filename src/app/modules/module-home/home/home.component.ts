import { Component, OnInit } from '@angular/core';
import { Profile, UserGetReady } from 'src/app/interfaces/profile.interface';
import { MathService } from 'src/app/services/math.service';
import { Achievement } from 'src/app/modules/module-achievements/utils/achievement.utils';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';
import { LevelsService } from 'src/app/services/levels.service';
import { User } from 'src/app/services/user';
import { OverlayService } from 'src/app/services/overlay.service';
import { Course } from 'src/app/interfaces/course.interface';
import { CoursesService } from 'src/app/services/courses.service';
import { map, take } from 'rxjs/operators';
import { AchievementsSharedService } from 'src/app/services/achievements-shared.service';
import { ProfileService } from "../../../services/profile.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  user: Profile;
  userGetReady: UserGetReady;
  imgSource: string;

  levelProgressPercentage: number = 0;
  mouseOverProfilePicture: boolean = false;

  courses: Course[];

  currentUser: User = null;

  achievementPoints: number = 0;
  lastAchievements: Achievement[] = [];

  constructor(
    private mathService: MathService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sessionService: SessionService,
    private coursesService: CoursesService,
    private levelsService: LevelsService,
    private overlayService: OverlayService,
    private achievementsSharedService: AchievementsSharedService,
    private profileService: ProfileService
  ) {
    this.userGetReady = this.activatedRoute.snapshot.data.pageData.userGetReady;
    
    //más adelante sacar todo esto
    this.currentUser = this.sessionService.getCurrentUser();

    this.user = {
      id: this.currentUser.id,
      username: '',
      displayname: ''
    };

    profileService.getProfileImageById(this.currentUser.id).subscribe(data => {
      if (data != undefined && data.base64Image != null) {
        this.imgSource = data.base64Image;
      } else {
        this.imgSource = '../../../../assets/empty-avatar.png'
      }
      this.user.username = data?.username;
      this.user.displayname = data?.displayname;
    });

    this.initializeCourses();
  }

  private initializeCourses() {
    this.coursesService.findByUser(this.user.id).pipe(
      map(courses => courses.map(({ progreso, nombre, ...course }) => ({
        ...course,
        title: nombre,
        progress: progreso
      }))), take(1))
      .subscribe(courses => {
        // filtro los cursos en progreso
        this.courses = courses
          .filter(c => c.progress < 100)
          .sort((n1,n2) => {
            if (n1.progress < n2.progress) { return 1; }
            if (n1.progress > n2.progress) { return -1; }
            return 0;
          }).slice(0,3);
      });
  }

  ngOnInit(): void {
    if (!this.userHasLevel()) {
      this.overlayService.displayLoadingOverlay();
      this.levelsService.createUserLevel(this.currentUser.id).subscribe((newUserGetReady: UserGetReady) => {
        console.log(newUserGetReady);
        this.userGetReady = newUserGetReady;
        this.overlayService.hideLoadingOverlay();
        this.levelProgressPercentage = this.mathService.getPercentage(this.userGetReady.nivel.experiencia, this.userGetReady.experiencia);
      });
    } else {
      this.levelProgressPercentage = this.mathService.getPercentage(this.userGetReady.nivel.experiencia, this.userGetReady.experiencia);
    }

    this.achievementsSharedService.userAchievements.subscribe(
      newAchievements => {
        this._updateAchievements();
        console.log(this.achievementPoints);
      }
    );
  }

  userHasLevel(): boolean {
    return this.userGetReady.nivel != undefined;
  }

  editProfile() {
    this.router.navigate(['profile/edit'], { queryParams: { userId: this.user.id } });
  }

  private _updateAchievements(): void {
    let acum: number = 0;

    this.lastAchievements = this.achievementsSharedService.userAchievements.value.slice(0, 3);

    for (let achievement of this.achievementsSharedService.userAchievements.value) {
      acum += achievement.points;

      this.achievementPoints = acum;
    }
  }

  goToCoursesStats() {
    this.router.navigate(['courses/stats']);
  }

  goToGamesStats() {
    this.router.navigate(['games/stats']);
  }

  goToAppraisalStats() {
    this.router.navigate(['appraisals/statistics']);
  }
  
  goToManageSectors() {
    this.router.navigate(['manage-users/sectors']);
  }

}
