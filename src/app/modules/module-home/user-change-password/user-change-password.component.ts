import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-change-password',
  templateUrl: './user-change-password.component.html',
  styleUrls: ['./user-change-password.component.scss']
})
export class UserChangePasswordComponent implements OnInit {
  private id: number;
  private errorMessage = new BehaviorSubject<string>("");
  public errorMessage$ = this.errorMessage.asObservable();
  public hidePass1: boolean = true;
  public hidePass2: boolean = true;
  public form: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService) { 
      this.route.queryParams.subscribe(params => {
        this.id = params['userId'];
    });
    }

  public togglePasswordVissibility1(event) {
    event.preventDefault();
    event.stopPropagation();
    this.hidePass1 = !this.hidePass1;
  }

  public togglePasswordVissibility2(event) {
    event.preventDefault();
    event.stopPropagation();
    this.hidePass2 = !this.hidePass2;
  }

  public ngOnInit(): void {
    this.form = this.formBuilder.group({
      currentPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required]]
    });
  }

  public submit(): void {
    let data = this.form.value;
    data.idUser = this.id;
    this.userService.changePassword(data).subscribe(r => {
      if (r.ok) {
        this.errorMessage.next("");
        this.router.navigate(['']); 
      } else {
        this.errorMessage.next(r.message);
      }
    })
  }
}
