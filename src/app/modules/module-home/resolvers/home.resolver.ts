import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { of, throwError, forkJoin, Observable } from 'rxjs';
import { delay, catchError, map } from 'rxjs/operators';
import { LevelsService } from 'src/app/services/levels.service';
import { SessionService } from 'src/app/services/session.service';
import { UserGetReady } from 'src/app/interfaces/profile.interface';

@Injectable()
export class HomeResolver implements Resolve<any> {

    constructor(
        private levelsService: LevelsService,
        private sessionService: SessionService,
    ) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let userGetReady: Observable<UserGetReady> = this.levelsService.getUserLevel(this.sessionService.getCurrentUser().id);

        return forkJoin([userGetReady]).pipe(
            map((results) => ({
                userGetReady: results[0],
            })),
            catchError(error => {
                return throwError(error);
            }),
        );
    }
}