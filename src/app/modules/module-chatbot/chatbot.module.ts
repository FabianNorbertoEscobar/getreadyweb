import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ChatbotComponent } from './chatbot/chatbot.component';
import { DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BadWordsPipe } from 'src/app/modules/module-chatbot/pipes/bad-words.pipe';
import { ChatbotInteractionService } from './services/chatbot-interaction.service';
import { ChatbotService } from './services/chatbot.service';

@NgModule({
    imports: [
        SharedModule,
        FormsModule,
    ],
    exports: [
        ChatbotComponent,
    ],
    declarations: [
        ChatbotComponent,
        BadWordsPipe,
    ],
    providers: [
        DatePipe,
        BadWordsPipe,
        ChatbotInteractionService,
        ChatbotService,
    ]
})
export class ChatbotModule {}