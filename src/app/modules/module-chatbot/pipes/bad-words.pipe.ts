import { Pipe, PipeTransform } from "@angular/core";

/**
 * Array con lo que consideramos malas palabras
 */
export const BAD_WORDS: string[] = [
    'puto', 'pelotudo', 'puta', 'pelotuda', 'nazi',
]

@Pipe({
    name: 'badwords',
    pure: true,
})
export class BadWordsPipe implements PipeTransform {

    /** Malas palabras */
    badWords: string[] = BAD_WORDS;

    /**
     * Censura las malas palabras de un mensaje dado.
     * @param message el mensaje
     * @param args array de parámetros
     */
    transform(message: string, ...args: any[]) {
        let censoredMessage: string = '';
        let messageWords: string[] = message.split(' ');

        messageWords.forEach(word => {
            let newWord: string;

            if (this.badWords.includes(word.toLowerCase())) {
                newWord = word.replace(word.substring(1, word.length), '****');
            } else {
                newWord = word;
            }

            censoredMessage += ` ${newWord}`;
        });

        censoredMessage.trimLeft();

        return censoredMessage;
    }
}