import { Component, OnInit, ElementRef, ViewChild, HostListener } from '@angular/core';
import { DatePipe } from '@angular/common';
import { BadWordsPipe } from 'src/app/modules/module-chatbot/pipes/bad-words.pipe';
import { ChatbotInteractionService } from '../services/chatbot-interaction.service';
import { ChatbotService } from '../services/chatbot.service';
import { environment } from 'src/environments/environment';
import { AchievementsSharedService } from 'src/app/services/achievements-shared.service';

/** Interfaz dedicada a los mensajes del template del chatbot*/
export interface ChatMessage {
	userMessage: boolean;
	message: string;
  timestamp?: string;
  options?: MessageOption[]
}

/** Opciones para un mensaje con opciones */
export interface MessageOption {
  option: string;
  url: string;
}

/**
 * Este componente es el famoso chatbot
 */
@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.scss'],
})
export class ChatbotComponent implements OnInit {

  /** Formato de hora del timestamp de los mensajes */
	messageTimeFormat: string = "HH:mm"
  /** Flag que indica si el chatbot está actualmente visible */
  chatVisible: boolean = false;
  /** Array de mensajes */
  messages: ChatMessage[] = [];
  /** El mensaje actual que se va a enviar */
  message: string;
  /** Referencia al container de mensajes */
  @ViewChild('chatbox', {static: false})
  chatBox: ElementRef;
  /** Flag que indica si el chatbot está enviando una respuesta */
  writing: boolean = false;
  
  environment = environment;

  constructor(
    private datePipe: DatePipe,
    private badWords: BadWordsPipe,
    private chatbotInteractionService: ChatbotInteractionService,
    private chatbotService: ChatbotService,
    private achievementsSharedService: AchievementsSharedService,
  ) {
    this.messages.push({
      userMessage: false,
      message: 'Hola! En que puedo ayudarte? :)',
      timestamp: this.datePipe.transform(new Date(), this.messageTimeFormat),
    });
  }

  ngOnInit(): void {
	  this.chatbotInteractionService.message.subscribe(message => {
		  if (message) {
        if (!this.chatVisible) {
          this.toggleChat();
        }

        this.addMessage(message, false, new Date());
		  }
	  });
  }

  /**
   * Función para mostrar u ocultar el chatbot
   */
  toggleChat() {
    this.chatVisible = !this.chatVisible;
    
    if (this.chatVisible) {
      this.updateScrollBar();
    }
  }
  
  /**
   * Envía el mensaje del usuario al chatbot, cargando el mensaje del usuario y la respuesta del agente en el chat
   */
  sendMessage() {
    this.addMessage(this.message, true, new Date());

    this.writing = true;
    this.chatbotService.sendMessage(this.message).subscribe(response => {
      this.addMessage(response.mensaje, false, response.timestamp, response.intent);
      this.writing = false;

      this.achievementsSharedService.grantAchievementToUser("Haciendo amigos");
      
    });

    this.message = null;
  }

  /**
   * Carga un mensaje en el chatbot
   * @param _message el mensaje
   * @param isUserMessage flag que se indica como `true` si el mensaje es del usuario, `false` en caso contrario
   */
  addMessage(_message: string, isUserMessage: boolean, timestamp: any, intent?: string) {
    if(_message) {
      let chatMessage: ChatMessage = {
        userMessage: isUserMessage,
        message: _message,
        timestamp: this.datePipe.transform(timestamp, this.messageTimeFormat),
        options: [],
      }

      if (intent && intent == "Default" && !isUserMessage) {
        chatMessage.options.push({ option: "Mis cursos", url: `${environment.appUrl}courses`});
        chatMessage.options.push({ option: "Juegos", url: `${environment.appUrl}games`});
        chatMessage.options.push({ option: "Logros", url: `${environment.appUrl}achievements`});
      }

      this.messages.push(chatMessage);

      this.updateScrollBar();
    }
  }

  /**
   * Actualiza la posición de la scrollbar en el chat
   */
  updateScrollBar() {
    setTimeout(() => {
      this.chatBox.nativeElement.scrollTop = this.chatBox.nativeElement.scrollHeight;
    }, 10);
  }

  openUrl(url: string): void {
    window.open(url);
  }

  /**
   * Parsea el mensaje a imprimir para detectar la presencia de hyperlinks
   * @param message el mensaje
   */
  parseMessage(message: string) {
    let httpStartIndex: number = message.indexOf('https://');
    let url: string = "";
    let envUrl: string = "";

    if (httpStartIndex < 0) {
      httpStartIndex = message.indexOf('http://');
    }

    if (httpStartIndex >= 0) {
      for (let i = httpStartIndex ; i < message.length ; i++ ) {
        // Si llegó al final de la URL, obtengo la URL
        if (message.charAt(i) == " " || message.charAt(i) == '\n' || i == message.length - 1) {
          if (i == message.length - 1) {
            url = message.substring(httpStartIndex, i + 1);
          } else {
            url = message.substring(httpStartIndex, i);
          }
          break;
        }
      }

      if (!environment.production) {
        envUrl = url.replace("https://get-ready-web.herokuapp.com/", environment.appUrl);
      }

      message = message.replace(url, this.getUrl(envUrl ? envUrl : url));
    }

    return message;
  }

  /**
   * Previene la introducción de caracteres no permitidos en el mensaje, con el fin de evitar la inyección de código.
   * @param $event el evento del teclado
   */
  removeSpecialCharacters($event: KeyboardEvent) {
    if ($event.charCode == 91) {
      return false;
    }

    return true;
  }

  /**
   * Previene el pegado de texto.
   * @param e el evento del teclado
   */
  @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent) {
    e.preventDefault();
  }

  /**
   * Devuelve un hyperlink a la ruta especificada.
   * @param urlString la url
   */
  getUrl(urlString: string): string {
    return `<a href="${urlString}">${urlString}</a>`;
  }
}
