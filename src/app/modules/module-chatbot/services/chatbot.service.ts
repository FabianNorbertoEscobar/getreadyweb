import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';
import { SessionService } from 'src/app/services/session.service';
import { User } from 'src/app/services/user';

/** El mensaje enviado por el usuario al chatbot */
export interface UserMessage {
    mensaje: string;
    idUsuario: number;
    tipoRol: string;
    idSector: number;
}

/** El mensaje (respuesta) enviado por el chatbot al usuario */
export interface BotMessage {
    mensaje: string;
    intent: string;
    timestamp: string;
}

@Injectable()
export class ChatbotService {

    constructor(
        private http: HttpService,
        private httpClient: HttpClient,
        private sessionService: SessionService,
    ) {}

    /**
     * Envía un mensaje al agente de DialogFlow
     * @param userMessage el mensaje del usuario
     * @returns un `Observable` con la respuesta del agente
     */
    sendMessage(userMessage: string): Observable<BotMessage> {
        let currentUser: User = this.sessionService.getCurrentUser();
        let messagePayload: UserMessage = {
            mensaje: userMessage,
            idUsuario: currentUser.id,
            tipoRol: currentUser.roleType,
            idSector: currentUser.id_sector,
        }

        return this.http.post('chatbot', messagePayload);
    }
}