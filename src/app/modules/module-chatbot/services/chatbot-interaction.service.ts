import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class ChatbotInteractionService {

  private messageSource: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  message: Observable<string> = this.messageSource.asObservable();

  constructor() {}

  /**
   * Abre el chatbot. Opcionalmente, puede enviarse un mensaje para que lo muestre al abrirse
   * @param message el mensaje
   */
  toggleChatbot(message?: string) {
    this.messageSource.next(message);
  }
}
