import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AppraisalsComponent } from './appraisals/appraisals.component';
import { Routes, RouterModule } from '@angular/router';
import { AppraisalDetailComponent } from './appraisal-detail/appraisal-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppraisalStatisticsComponent } from './appraisal-statistics/appraisal-statistics.component';
import { ChartsModule } from 'ng2-charts';
import { TitleCasePipe } from '@angular/common';
import { AppraisalsResolver } from './resolvers/appraisals.resolver';
import { AppraisalStatsResolver } from './resolvers/appraisal-stats.resolver';

const routes: Routes = [
    {
        path: '',
        component: AppraisalsComponent,
        resolve: { pageData: AppraisalsResolver }
    },
    {
        path: 'new-appraisal',
        loadChildren: () => (import('./module-new-appraisal/new-appraisal.module')).then(m => m.NewAppraisalModule),
    },
    {
        path: 'statistics',
        component: AppraisalStatisticsComponent,
        resolve: { pageData: AppraisalStatsResolver }
    },
    {
        path: ':appraisalId',
        component: AppraisalDetailComponent
    },
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        FormsModule,
        ChartsModule
    ],
    declarations: [
        AppraisalsComponent,
        AppraisalDetailComponent,
        AppraisalStatisticsComponent
    ],
    providers: [
        TitleCasePipe, 
        AppraisalsResolver,
        AppraisalStatsResolver
    ]
})
export class AppraisalsModule {}
