import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { DialogConfirmationComponent } from 'src/app/components/dialog/dialog-confirmation/dialog-confirmation.component';
import { AppraisalFromDB, AppraisalLine } from 'src/app/interfaces/appraisal-db.interface';
import { AppraisalsService } from 'src/app/services/appraisals.service';
import { AuthService } from 'src/app/services/auth.service';
import { HeaderService } from 'src/app/services/header/header.service';
import { OverlayService } from 'src/app/services/overlay.service';
import { User } from 'src/app/services/user';
import sortBy from 'lodash/sortBy';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'app-appraisal-detail',
  templateUrl: 'appraisal-detail.component.html',
  styleUrls: ['appraisal-detail.component.scss']
})

export class AppraisalDetailComponent implements OnInit {
  appraisal: AppraisalFromDB;
  allowedScore: number[];
  form: FormGroup;
  dataSource = new BehaviorSubject<AbstractControl[]>([]);
  user: User;

  private errorMessage = new BehaviorSubject<string>('');
  errorMessage$ = this.errorMessage.asObservable();

  CREATED_APPRAISAL_STATUS_ID: number;
  STARTED_APPRAISAL_STATUS_ID: number;
  FINISHED_APPRAISAL_STATUS_ID: number;

  constructor(
    private appraisalsService: AppraisalsService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private headerService: HeaderService,
    private dialog: MatDialog,
    private overlayService: OverlayService,
    private titleCasePipe: TitleCasePipe,
  ) { }

  ngOnInit() {
    setTimeout(() => this.headerService.setDescriptionForRoute('Detalle de evaluación'));
    this.overlayService.displayLoadingOverlay();

    this.CREATED_APPRAISAL_STATUS_ID = this.appraisalsService.CREATED_APPRAISAL_STATUS_ID;
    this.STARTED_APPRAISAL_STATUS_ID = this.appraisalsService.STARTED_APPRAISAL_STATUS_ID;
    this.FINISHED_APPRAISAL_STATUS_ID = this.appraisalsService.FINISHED_APPRAISAL_STATUS_ID;

    this.user = this.authService.getCurrentUser();

    this.form = this.fb.group({
      appraisalLines: new FormArray([])
    });

    const appraisalId = Number.parseInt(this.route.snapshot.paramMap.get('appraisalId'), 10);
    this.appraisalsService.getAppraisal(appraisalId).pipe(
      map((res: any) => {
        if (res.ok !== false) {
          return res;
        }
        this.router.navigate(['appraisals']);
      })
    ).subscribe((appraisal: AppraisalFromDB) => {
      this.overlayService.hideLoadingOverlay();
      this.appraisal = appraisal;
      const linesControl = this.form.get('appraisalLines') as FormArray;
      // ordeno porque cuando mando put de evaluacion habiendo editado una linea, me llega en desorden
      const sortedLines = sortBy(appraisal.lineasEvaluacion, 'id');
      sortedLines.forEach(linea => {
        linesControl.push(new FormControl(linea));
      });
      this.updateView();
    });

    this.allowedScore = this.appraisalsService.allowedScore;
  }

  get appraisalLines(): FormArray {
    return this.form.get('appraisalLines') as FormArray;
  }

  back() {
    this.router.navigate(['appraisals']);
  }

  saveAppraisal() {
    const lineasEvaluacion = this.appraisalLines.value;

    const updatedAppraisal = {
      ...this.appraisal,
      lineasEvaluacion
    };

    this.overlayService.displayLoadingOverlay();
    this.appraisalsService.updateAppraisal(updatedAppraisal).subscribe(response => {
      // Si viene un 200, no llega response.ok
      this.overlayService.hideLoadingOverlay();
      if (response.ok !== false) {
        this.errorMessage.next('');
        this.snackBar.open('Se actualizaron los datos correctamente.', null, { duration: 2000 });
        this.router.navigate(['appraisals']);
      } else {
        this.errorMessage.next(response.message);
        this.snackBar.open('Hubo un error. Por favor, reintente.', null, { duration: 2000 });
      }
    });
  }

  updateView() {
    this.dataSource.next(this.appraisalLines.controls);
  }

  setObservations(event, control: FormControl, field: string) {
    control.setValue({ ...control.value, [field]: event.target.value});
  }

  startAppraisal() {
    const fullName = this.titleCasePipe.transform(`${this.appraisal.evaluado.nombre} ${this.appraisal.evaluado.apellido}`);
    const dialogRef: MatDialogRef<DialogConfirmationComponent> = this.dialog.open(DialogConfirmationComponent, {
      data: {
        title: 'Confirmar inicio del intercambio',
        description: `Se iniciará la comparación e intercambio con el empleado "${fullName}" ¿Desea continuar?`
      }
    });

    const lineasEvaluacion = this.appraisalLines.value;

    const updatedAppraisal = {
      ...this.appraisal,
      fechaEnCurso: new Date(),
      idEstado: this.appraisalsService.STARTED_APPRAISAL_STATUS_ID,
      lineasEvaluacion
    };

    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.overlayService.displayLoadingOverlay();
        setTimeout(() => {
          this.appraisalsService.updateAppraisal(updatedAppraisal).subscribe(response => {
            // Si viene un 200, no llega response.ok
            this.overlayService.hideLoadingOverlay();
            if (response.ok !== false) {
              this.errorMessage.next('');
              this.snackBar.open('La evaluación ha sido iniciada.', 'Entendido', { duration: 2000 });
              // Fuerzo una recarga de la ruta para que actualicen las definiciones de los rows
              // del mat-table, sino queda con el ng-template inicial
              this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
                this.router.navigate([`appraisals/${this.appraisal.id}`]);
              });
            } else {
              this.errorMessage.next(response.message);
              this.snackBar.open('Hubo un error. Por favor, reintente.', 'Ok', { duration: 2000 });
            }
          });
        }, 1500);
      }
    });
  }

  finishAppraisal() {
    const fullName = this.titleCasePipe.transform(`${this.appraisal.evaluado.nombre} ${this.appraisal.evaluado.apellido}`);
    const areAllSkillsScored = this.appraisalLines.value.every((line: AppraisalLine) => line.puntajeFinal);
    if (!areAllSkillsScored) {
      this.snackBar.open('Para finalizar debe puntuar todas las competencias.', 'Entendido');
      return;
    }

    const dialogRef: MatDialogRef<DialogConfirmationComponent> = this.dialog.open(DialogConfirmationComponent, {
      data: {
        title: 'Confirmar cierre del intercambio',
        description: `Se dará por finalizada la evaluación para el empleado "${fullName}" ¿Desea continuar?`
      }
    });

    const lineasEvaluacion = this.appraisalLines.value;

    const updatedAppraisal = {
      ...this.appraisal,
      fechaFinalizacion: new Date(),
      idEstado: this.appraisalsService.FINISHED_APPRAISAL_STATUS_ID,
      lineasEvaluacion
    };

    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.overlayService.displayLoadingOverlay();
        setTimeout(() => {
          this.appraisalsService.updateAppraisal(updatedAppraisal).subscribe(response => {
            // Si viene un 200, no llega response.ok
            this.overlayService.hideLoadingOverlay();
            if (response.ok !== false) {
              this.errorMessage.next('');
              this.snackBar.open('La evaluación ha sido finalizada.', 'Entendido', { duration: 2000 });
              this.router.navigate(['appraisals']);
            } else {
              this.errorMessage.next(response.message);
              this.snackBar.open('Hubo un error. Por favor, reintente.', 'Ok', { duration: 2000 });
            }
          });
        }, 1500);
      }
    });
  }
}
