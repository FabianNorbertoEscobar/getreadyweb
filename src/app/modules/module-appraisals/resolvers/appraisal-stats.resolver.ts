import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppraisalsService } from 'src/app/services/appraisals.service';
import { SessionService } from 'src/app/services/session.service';

@Injectable()
export class AppraisalStatsResolver implements Resolve<any> {
    
    constructor(private sessionService: SessionService,
        private appraisalsService: AppraisalsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let currentUser = this.sessionService.getCurrentUser();
        let requests: Observable<any>[] = [];

        if (currentUser.roleType === 'ADMIN_SECTOR') {
            requests.push(this.appraisalsService.getAverageEmployeeScoresByEvaluator(currentUser.id));
            requests.push(this.appraisalsService.getAverageSkillScoresByEvaluator(currentUser.id));
        } else if (currentUser.roleType === 'ADMIN') {
            requests.push(this.appraisalsService.getAverageEmployeeScoresByOrganization(currentUser.idOrganizacion));
            requests.push(this.appraisalsService.getAverageSkillScoresByOrganization(currentUser.idOrganizacion));
        }

        return forkJoin(requests).pipe(map(result => {
            return {
                employeeInfo: result[0],
                skillInfo: result[1]
            }
        }));
    }

}