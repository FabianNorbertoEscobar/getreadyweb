import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { sortBy } from 'lodash';
import { map } from 'rxjs/operators';
import { AppraisalsService } from 'src/app/services/appraisals.service';
import { AuthService } from 'src/app/services/auth.service';

@Injectable()
export class AppraisalsResolver implements Resolve<any> 
{
    constructor(private appraisalsService: AppraisalsService,
        private authService: AuthService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let user = this.authService.getCurrentUser();
        if (user.roleType === 'USUARIO') {
            return this.appraisalsService.getAppraisalsEmployee(user.id).pipe(
                map((res: any) => {
                if (res.ok !== false) {
                  return sortBy(res, 'id');
                }
              })
            );
        } else if (user.roleType === 'ADMIN_SECTOR'){
            return this.appraisalsService.getAppraisalsEvaluator(user.id).pipe(
                map((res: any) => {
                if (res.ok !== false) {
                    return sortBy(res, 'id');
                }
                })
        );
        } else {
            return this.appraisalsService.getAppraisalsByOrganization(user.idOrganizacion).pipe(
                map((res: any) => {
                if (res.ok !== false) {
                    return sortBy(res, 'id');
                }
                }));
        }
    }

}