import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppraisalFromDB } from 'src/app/interfaces/appraisal-db.interface';
import { AppraisalsService } from 'src/app/services/appraisals.service';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/services/user';

@Component({
  selector: 'app-appraisals',
  templateUrl: './appraisals.component.html',
  styleUrls: ['./appraisals.component.scss']
})
export class AppraisalsComponent implements OnInit {
  isMobile: boolean = false;
  appraisals: Array<AppraisalFromDB>;
  user: User;

  CREATED_APPRAISAL_STATUD_ID: number;
  STARTED_APPRAISAL_STATUS_ID: number;
  FINISHED_APPRAISAL_STATUS_ID: number;

  constructor(
    private appraisalsService: AppraisalsService,
    private authService: AuthService,
    private router: Router,
    private breakpointObserver: BreakpointObserver,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.breakpointObserver.observe(['(max-width: 599px)']).subscribe((state: BreakpointState) => {
      this.isMobile = state.matches;
    });
    this.user = this.authService.getCurrentUser();

    this.CREATED_APPRAISAL_STATUD_ID = this.appraisalsService.CREATED_APPRAISAL_STATUS_ID;
    this.STARTED_APPRAISAL_STATUS_ID = this.appraisalsService.STARTED_APPRAISAL_STATUS_ID;
    this.FINISHED_APPRAISAL_STATUS_ID = this.appraisalsService.FINISHED_APPRAISAL_STATUS_ID;

    this.appraisals = this.route.snapshot.data.pageData;
  }

  canCreateAppraisals(): boolean {
    return this.user.roleType === 'ADMIN_SECTOR';
  }

  canViewAppraisalsHeader(): boolean {
    return this.user.roleType === 'ADMIN_SECTOR' || this.user.roleType === 'ADMIN';
  }

  createAppraisal() {
    this.router.navigate(['new-appraisal'], { relativeTo: this.route});
  }

  goToAppraisalDetail(appraisalId: number) {
    this.router.navigate([appraisalId], { relativeTo: this.route });
  }

  goToAppraisalStatistics() {
    this.router.navigate(['statistics'], { relativeTo: this.route });
  }
}
