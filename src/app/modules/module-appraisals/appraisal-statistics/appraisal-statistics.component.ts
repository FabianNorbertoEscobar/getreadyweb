import { TitleCasePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Colors, Label } from 'ng2-charts';
import { AppraisalAverageEmployeeScores, AppraisalAverageSkillScores } from 'src/app/interfaces/appraisal-average-scores.interface';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/services/user';
import { Location } from '@angular/common';

@Component({
  selector: 'app-appraisal-statistics',
  templateUrl: 'appraisal-statistics.component.html',
  styleUrls: ['appraisal-statistics.component.scss']
})

export class AppraisalStatisticsComponent implements OnInit {
  public currentUser: User;

  private colorSequence = [
    '#1976d2',
    '#dc004e',
    '#f44336',
    '#ff9800',
    '#2196f3',
    '#2196f3'
  ];

  // Configs compartidas
  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [{}],
      yAxes: [{
        ticks: {
          max: 5,
          min: 0
        }
      }]
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartPlugins = [pluginDataLabels];

  // Configs del chart de empleados
  public employeeBarChartLabels: Label[] = [];
  public employeeBarChartData: ChartDataSets[] = [];
  public employeeBarColors: Colors[] = [
    {
      backgroundColor: [],
      pointHoverBackgroundColor: [],
    },
  ];

  // Configs del chart de competencias
  public skillBarChartLabels: Label[] = [];
  public skillBarChartData: ChartDataSets[] = [];
  public skillBarColors: Colors[] = [
    {
      backgroundColor: [],
      pointHoverBackgroundColor: [],
    },
  ];

  constructor(
    private authService: AuthService,
    private location: Location,
    private titleCasePipe: TitleCasePipe,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.currentUser = this.authService.getCurrentUser();
    this.setEmployeeBarChartInfo(this.activatedRoute.snapshot.data.pageData.employeeInfo);
    this.setSkillBarChartInfo(this.activatedRoute.snapshot.data.pageData.skillInfo);
  }

  setEmployeeBarChartInfo(employeeAverageScores: Array<AppraisalAverageEmployeeScores>) {
    this.employeeBarChartData = [
      {
        data: employeeAverageScores.map(emp => emp.promedio),
        label: 'Promedio de puntajes finales por empleado'
      }
    ];

    this.employeeBarChartLabels = employeeAverageScores.map(emp => {
      const fullName = `${emp.evaluado.nombre} ${emp.evaluado.apellido}`;
      return this.titleCasePipe.transform(fullName);
    });

    this.employeeBarColors[0].backgroundColor = employeeAverageScores.map((emp, index) =>
      this.colorSequence[index % this.colorSequence.length]);
  }

  setSkillBarChartInfo(skillAverageScores: Array<AppraisalAverageSkillScores>) {
    this.skillBarChartData = [
      {
        data: skillAverageScores.map(skill => skill.promedio),
        label: 'Promedio de puntajes finales por competencia'
      }
    ];

    this.skillBarChartLabels = skillAverageScores.map(skill => this.titleCasePipe.transform(skill.competencia.nombre));

    this.skillBarColors[0].backgroundColor = skillAverageScores.map((skill, index) =>
      this.colorSequence[index % this.colorSequence.length]);
  }

  back() {
    this.location.back();
  }
}
