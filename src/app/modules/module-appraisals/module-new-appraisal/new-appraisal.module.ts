import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { DialogSkillComponent } from './new-appraisal/dialog-skill/dialog-skill.component';
import { NewAppraisalComponent } from './new-appraisal/new-appraisal.component';

const routes: Routes = [
  {
      path: '',
      component: NewAppraisalComponent,
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [],
  declarations: [
    NewAppraisalComponent,
    DialogSkillComponent
  ],
  providers: [],
})
export class NewAppraisalModule { }
