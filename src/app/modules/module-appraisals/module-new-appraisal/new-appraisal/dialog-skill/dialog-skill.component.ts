import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { CompetenciaFromDB } from 'src/app/interfaces/competencia-db.interface';

export interface DialogData {
  availableSkills: Array<CompetenciaFromDB>;
}

@Component({
  selector: 'app-dialog-skill',
  templateUrl: 'dialog-skill.component.html',
  styleUrls: ['dialog-skill.component.scss']
})

export class DialogSkillComponent implements OnInit {
  selectedSkill: CompetenciaFromDB;

  constructor(
    public dialogRef: MatDialogRef<DialogSkillComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  ngOnInit() { }

  selectSkill(event: MatSelectChange) {
    this.selectedSkill = event.value;
  }

  cancel() {
    this.dialogRef.close();
  }

  addSkill() {
    this.dialogRef.close(this.selectedSkill);
  }
}
