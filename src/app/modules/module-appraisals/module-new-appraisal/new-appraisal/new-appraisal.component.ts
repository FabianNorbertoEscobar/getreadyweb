import { TitleCasePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { DialogConfirmationComponent } from 'src/app/components/dialog/dialog-confirmation/dialog-confirmation.component';
import { CompetenciaFromDB } from 'src/app/interfaces/competencia-db.interface';
import { UsuarioFromDB } from 'src/app/interfaces/usuario-db.interface';
import { AppraisalsService } from 'src/app/services/appraisals.service';
import { AuthService } from 'src/app/services/auth.service';
import { CompetenciasService } from 'src/app/services/competencias.service';
import { OverlayService } from 'src/app/services/overlay.service';
import { User } from 'src/app/services/user';
import { UserService } from 'src/app/services/user.service';
import { DialogSkillComponent } from './dialog-skill/dialog-skill.component';

@Component({
  selector: 'app-new-appraisal',
  templateUrl: 'new-appraisal.component.html',
  styleUrls: ['new-appraisal.component.scss']
})

export class NewAppraisalComponent implements OnInit {
  employees: Array<UsuarioFromDB> = [];
  skillsFromOrganization: Array<CompetenciaFromDB> = [];
  evaluator: User;
  listOfMonths = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

  form: FormGroup;

  private errorMessage = new BehaviorSubject<string>('');
  errorMessage$ = this.errorMessage.asObservable();

  constructor(
    private dialog: MatDialog,
    private userService: UserService,
    private authService: AuthService,
    private competenciasService: CompetenciasService,
    private formBuilder: FormBuilder,
    private router: Router,
    private appraisalsService: AppraisalsService,
    private overlayService: OverlayService,
    private titleCasePipe: TitleCasePipe,
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      employee: [null, [Validators.required]],
      skills: new FormArray([], Validators.required),
      months: ['', [Validators.required]]
    });

    this.evaluator = this.authService.getCurrentUser();

    // TODO: agregar nombre y apellido en la tabla usuario para mostrar en el selector eso y no el username
    this.userService.getUsers().subscribe(data => {
      if (data.ok !== false) {
        this.employees = data.filter(emp => emp.rol.tipo === 'USUARIO' && emp.sector.id === this.evaluator.id_sector);
      }
    });

    this.competenciasService.getCompetencias().subscribe(response => {
      if (response.ok !== false) {
        this.skillsFromOrganization = response;
      }
    });
  }

  get employee(): AbstractControl {
    return this.form.get('employee');
  }

  get skills(): FormArray {
    return this.form.get('skills') as FormArray;
  }

  get months(): AbstractControl {
    return this.form.get('months');
  }

  selectEmployee(event: MatSelectChange) {
    this.employee.setValue(event.value);
  }

  selectMonths(event: MatSelectChange) {
    this.months.setValue(event.value);
  }

  addSkill() {
    if (this.skillsFromOrganization.length) {
      const dialogRef: MatDialogRef<DialogSkillComponent> = this.dialog.open(DialogSkillComponent,
        { data: { availableSkills: this.skillsFromOrganization } }
      );
      dialogRef.afterClosed().subscribe(skill => {
        if (skill) {
          this.skills.push(new FormControl(skill));
          // saco de las competencias disponibles para agregar
          this.skillsFromOrganization = this.skillsFromOrganization.filter(s => s.id !== skill.id);
        }
      });
    }
  }

  removeSkill(skill: CompetenciaFromDB) {
    const indexToRemove = this.skills.value.findIndex(s => s.id === skill.id);
    this.skills.removeAt(indexToRemove);
    // agrego a las competencias disponibles para agregar
    this.skillsFromOrganization = [...this.skillsFromOrganization, skill];
  }

  submit() {
    const fullName = this.titleCasePipe.transform(`${this.employee.value.nombre} ${this.employee.value.apellido}`);
    const dialogRef: MatDialogRef<DialogConfirmationComponent> = this.dialog.open(DialogConfirmationComponent, {
      data: {
        title: 'Confirmar creación de evaluación',
        description: `Se creará una evaluación para el usuario "${fullName}" ¿Desea continuar?`
      }
    });

    const currentDate = new Date();
    const fechaTentativa = new Date(currentDate.setMonth(currentDate.getMonth() + this.months.value));

    const lineasEvaluacion = this.skills.value.map(
      (skill: CompetenciaFromDB) => ({ idCompetencia: skill.id })
    );

    const newAppraisal = {
      idEvaluado: this.employee.value.id,
      idEvaluador: this.evaluator.id,
      idEstado: 1,
      fechaTentativa,
      lineasEvaluacion
    };

    dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.overlayService.displayLoadingOverlay();
        setTimeout(() => {
          this.appraisalsService.addAppraisal(newAppraisal).subscribe(response => {
            this.overlayService.hideLoadingOverlay();
            // Si viene un 200, no llega response.ok
            if (response.ok !== false) {
              this.errorMessage.next('');
              this.router.navigate(['appraisals']);
            } else {
              this.errorMessage.next(response.message);
            }
          });
        }, 1500);
      }
    });
  }

  back() {
    this.router.navigate(['appraisals']);
  }
}
