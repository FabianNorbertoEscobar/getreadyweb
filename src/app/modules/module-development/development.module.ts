import { NgModule } from "@angular/core";
import { SharedModule } from '../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { DevelopmentComponent } from './development/development.component';
import { FileUploadTesterComponent } from './file-upload-tester/file-upload-tester.component';
import { HttpClientModule } from '@angular/common/http';
import { S3Service } from 'src/app/services/s3.service';
import { ReactiveFormsModule } from '@angular/forms';
import {SharedPraiseModule} from '../shared/shared-praise.module';

const routes: Routes = [
    {
        path: '',
        component: DevelopmentComponent,
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        HttpClientModule,
        ReactiveFormsModule,
        SharedPraiseModule,
    ],
    declarations: [
        DevelopmentComponent,
        FileUploadTesterComponent
    ],
    providers: [
        S3Service,
    ]
})
export class DevelopmentModule {}
