import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { S3Service, FileS3 } from 'src/app/services/s3.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-file-upload-tester',
  templateUrl: './file-upload-tester.component.html',
  styleUrls: ['./file-upload-tester.component.scss']
})
export class FileUploadTesterComponent implements OnInit {

  @ViewChild('file', {static: false})
  public fileInput: any;

  folderPathControl: FormControl = new FormControl('');
  filePathControl: FormControl = new FormControl('');

  myFile: File;
  response: string = "";

  constructor(
    private http: HttpClient,
    private s3Service: S3Service,
  ) { }

  ngOnInit(): void {}

  onFilesAdded(): void {
    this.myFile = this.fileInput.nativeElement.files[0];
    this.response = "";
  }

  uploadFile(): void {
    this.s3Service.upload(this.myFile, this.folderPathControl.value).subscribe((response: FileS3) => {
      this.response = response.fileUrl;
    });
  }

  deleteFile(): void {
    this.response = "";
    this.myFile = null;
    this.s3Service.delete(this.filePathControl.value).subscribe(response => {
      console.log(response);
    });
  }
}
