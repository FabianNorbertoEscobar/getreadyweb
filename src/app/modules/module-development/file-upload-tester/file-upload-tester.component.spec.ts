import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileUploadTesterComponent } from './file-upload-tester.component';

describe('FileUploadTesterComponent', () => {
  let component: FileUploadTesterComponent;
  let fixture: ComponentFixture<FileUploadTesterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileUploadTesterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadTesterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
