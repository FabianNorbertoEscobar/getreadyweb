import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../../services/session.service';
import { User } from '../../../services/user';
import { PraiseService} from '../../../services/praise.service';


@Component({
  selector: 'app-development',
  templateUrl: './development.component.html',
  styleUrls: ['./development.component.scss']
})
export class DevelopmentComponent implements OnInit {
  user: User;
  counters: boolean;
  praiseEnabled: boolean;
  totalExcellent: number;
  totalCollaborative: number;
  totalThanks: number;

  constructor(
    private sessionService: SessionService,
    private praiseService: PraiseService
  ) {
    this.user = this.sessionService.getCurrentUser();
    this.counters = true;
    this.praiseService.getPraiseAvailability(this.user.id).subscribe(x => this.praiseEnabled = x );
    this.praiseService.getTotalPraise(2).subscribe(x => {
      this.totalExcellent = x.totalExcellent;
      this.totalCollaborative = x.totalCollaborative;
      this.totalThanks = x.totalThanks;
    });
  }


  ngOnInit(): void {

  }
}
