import { NgModule } from '@angular/core';
import { SharedModule } from './shared.module';
import { AchievementCardComponent } from '../module-achievements/achievement-card/achievement-card.component';

const components = [
    AchievementCardComponent,
]

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        components
    ],
    exports: [
        components,
    ]
})
export class SharedAchievementsModule{}