import { NgModule } from '@angular/core';
import { SharedModule } from './shared.module';
import { PraiseButtonsComponent } from '../../components/praise-buttons/praise-buttons.component';

const components = [
  PraiseButtonsComponent,
];

@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [
    components
  ],
  exports: [
    components,
  ]
})
export class SharedPraiseModule{}
