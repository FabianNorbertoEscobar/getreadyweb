import { NgModule } from '@angular/core';
import { SharedModule } from './shared.module';
import { CourseProgressCardComponent } from '../module-courses/course-progress-card/course-progress-card.component';

const components = [
    CourseProgressCardComponent,
]

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        components
    ],
    exports: [
        components,
    ]
})
export class SharedCoursesModule {}