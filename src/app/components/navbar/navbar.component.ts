import { Component, OnInit } from '@angular/core';
import { NavbarItem } from '../../interfaces/navbar-item.interface';
import { Router } from '@angular/router';
import { NavbarService } from 'src/app/services/navbar.service';
import { OverlayService } from 'src/app/services/overlay.service';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { HeaderService } from 'src/app/services/header/header.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  navbarItems: NavbarItem[];
  navbarIsOpen: boolean = false;

  isMobile: boolean = false;
  title: string = "";

  constructor(
    private router: Router,
    private navbarItemsService: NavbarService,
    private overlayTransparentService: OverlayService,
    private breakpointObserver: BreakpointObserver,
    private headerService: HeaderService,
  ) {
    this.navbarItems = this.navbarItemsService.getNavbarItems();
  }

  ngOnInit(): void {
    this.navbarItemsService.navigationEvent.subscribe(navbarItem => {
      if (navbarItem) {
        this.highlightItem(navbarItem);
      }
    });

    this.breakpointObserver.observe(['(max-width: 599px)']).subscribe((state: BreakpointState) => {
      this.isMobile = state.matches;
    });

    this.headerService.currentTitle.subscribe((currentTitle: string) => {
      this.title = currentTitle;
    });
  }

  /**
   * Abre o cierra la barra de navegación
   */
  toggleNavbar() {
    this.navbarIsOpen = !this.navbarIsOpen;
    if (this.navbarIsOpen) {
      this.overlayTransparentService.displayTransparentOverlay(this, 'toggleNavbar');
    } else {
      this.overlayTransparentService.hideTransparentOverlay();
    }
  }

  /**
   * Navega a la ruta contenida en el item seleccionado
   * @param item el item
   */
  navigate(item: NavbarItem) {
    this.highlightItem(item);
    
    if (this.navbarIsOpen) {
      this.toggleNavbar();
    }

    this.router.navigate([item.route]);
  }

  /**
   * Marca el item correspondiente como seleccionado
   * @param item el item
   */
  highlightItem(item: NavbarItem) {
    this.navbarItems.forEach(navbarItem => {
      navbarItem.checked = false;
    });

    item.checked = true;
  }
}