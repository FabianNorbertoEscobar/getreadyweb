export type PraiseType = 'THANKS' | 'COLLABORATIVE' | 'EXCELLENT';


export interface PraiseDTO {
  idOrigin: number;
  idDestination: number;
  type: PraiseType;
}

export interface TotalPraiseDTO {
  totalExcellent: number;
  totalCollaborative: number;
  totalThanks: number;
}
