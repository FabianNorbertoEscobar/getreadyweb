import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {PraiseDTO, PraiseType} from './praise.utils';
import {PraiseService} from '../../services/praise.service';
import { LevelsService } from 'src/app/services/levels.service';

@Component({
  selector: 'app-praise-buttons',
  templateUrl: './praise-buttons.component.html',
  styleUrls: ['./praise-buttons.component.scss'],
})
export class PraiseButtonsComponent implements OnInit {

  @Input()
  public currentUserId: number;
  @Input()
  public targetUserId: number;
  @Input()
  public enabled: boolean;
  @Input()
  public counters: boolean;
  @Input()
  public thanks: number = 0;
  @Input()
  public collaborative: number = 0;
  @Input()
  public excellent: number = 0;
  @Input()
  public display: boolean = false;

  @Output()
  praiseStatusChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private praiseService: PraiseService
  ) {
  }

  ngOnInit(): void {
    if (this.display){
      this.enabled = true;
      if ( this.counters && this.thanks === 0 && this.excellent === 0 && this.collaborative === 0){
        this.praiseService.getTotalPraise(this.currentUserId).subscribe(r => {
          this.thanks = r.totalThanks;
          this.collaborative = r.totalCollaborative;
          this.excellent = r.totalExcellent;
        });
      }
    }
  }

  praise(praiseType: PraiseType, event: any) {
    event.stopPropagation();
    if (!this.display) {
      // Disable buttons
      this.enabled = false;
      this.praiseStatusChange.emit(this.enabled);
      // update count
      switch (praiseType) {
        case 'COLLABORATIVE':
          this.collaborative += 1;
          break;
        case 'EXCELLENT':
          this.excellent += 1;
          break;
        case 'THANKS':
          this.thanks += 1;
          break;
      }
      // Send to server
      const praise: PraiseDTO = {
        idOrigin: this.currentUserId,
        idDestination: this.targetUserId,
        type: praiseType
      };
      this.praiseService.grantPraise(praise).subscribe(r => {
        if (!r.ok) {
          switch (praiseType) {
            case 'COLLABORATIVE':
              this.collaborative -= 1;
              break;
            case 'EXCELLENT':
              this.excellent -= 1;
              break;
            case 'THANKS':
              this.thanks -= 1;
              break;
          }
          if (r.message != 'Not available') {
            this.enabled = true;
            this.praiseStatusChange.emit(this.enabled);
          }
        }
      });
    }
  }
}
