import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SessionService } from 'src/app/services/session.service';
import { WebSocketAPI } from './WebSocketAPI';
import { BehaviorSubject, Subscription } from 'rxjs';
import { filter, take } from 'rxjs/operators';
import { NotificationsService } from 'src/app/services/notifications.service';
import { Router } from '@angular/router';
import { MatMenu, MatMenuTrigger } from '@angular/material/menu';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit, OnDestroy {
    @ViewChild("menuTrigger", { static: false }) menu: MatMenuTrigger;
    
    private webSocketAPI: WebSocketAPI;
    private subscription: Subscription;
    private notificationsSnackbar: any[] = [];
    private isShowingNotification: boolean = false;
    isMobile = false;
    isLoadingNotifications: boolean = false;
    notifications: any[];
    matBadgeColor: BehaviorSubject<string> = new BehaviorSubject<string>("");
    matBadgeCount: BehaviorSubject<number> = new BehaviorSubject<number>(0);

    constructor(private sessionService: SessionService,
        private router: Router,
        private notificationsService: NotificationsService,
        private breakpointObserver: BreakpointObserver,
        private snackBar: MatSnackBar) {}

    private showNext(): void {
        if (this.notificationsSnackbar.length == 0) {
            return;
        }

        let notif = this.notificationsSnackbar.shift();

        let snackBarRef = this.snackBar.open(notif, "VER", {
            duration: 3000,
            horizontalPosition: "right",
            verticalPosition: "top",
            panelClass: ["notification-snackbar"]
        });
        
        snackBarRef.afterDismissed().pipe(take(1)).subscribe(() => {
            this.isShowingNotification = false;
            this.showNext();
        });

        snackBarRef.onAction().pipe(take(1)).subscribe(() => {
            this.notificationsSnackbar.splice(0, this.notificationsSnackbar.length);
            this.menu.openMenu();
            this.onClick(undefined);
        })
    }

    private onGetNotification(value: string) {
        this.notificationsSnackbar.push(value);
        this.matBadgeCount.next(this.matBadgeCount.value + 1);
        
        if (!this.isMobile // si es mobile el snackbar excede la pantalla
            && !this.isShowingNotification) { 
            this.isShowingNotification = true;
            this.showNext(); 
        }
    }

    onClick(event:any) {
        if (this.isMobile) {
            // si es mobile directamente redirijo al componente, la lista se ve rara
            this.goToNotifications();
        } else {
            this.isLoadingNotifications = true;
            this.notificationsService.getTop(this.sessionService.getCurrentUser().id).subscribe(r => {
                this.notifications = r;
                this.isLoadingNotifications = false;
            })
        } 
    }

    ngOnInit(): void {
        this.breakpointObserver.observe(['(max-width: 599px)']).subscribe((state: BreakpointState) => {
            this.isMobile = state.matches;
        });
        
        this.subscription = this.matBadgeCount.subscribe(value => {
            this.matBadgeColor.next(value > 0 ? "warn" : "");
        })
        
        this.notificationsService.get(this.sessionService.getCurrentUser().id)
            .pipe(take(1)).subscribe(r => {
                if (r) {
                    this.matBadgeCount.next(r.filter(v => !v.leida).length);
                }
            });
        
        this.notificationsService.allRead$.pipe(filter(v => v)).subscribe(() => {
            this.matBadgeCount.next(0);
        });

        this.webSocketAPI = new WebSocketAPI(this.sessionService.getCurrentUser().id,
            (value) => this.onGetNotification(value)); 
    }

    ngOnDestroy(): void {
        this.webSocketAPI._disconnect();
        this.subscription.unsubscribe();
    }

    markAsRead(event: any, item: any) {
        let btnTarget = event.currentTarget;
        this.notificationsService.setAsRead(item.id).subscribe(r => {
            if (r.ok) {
                btnTarget.disabled = true;
                this.matBadgeCount.next(this.matBadgeCount.value - 1);
            }
        })
        // paro la propagación así no se cierra el menú
        event.stopPropagation();
    }

    goToNotifications() {
        this.router.navigate(['home/notifications']);
    }
}
  