import * as StompJs from '@stomp/stompjs';
import { StompHeaders } from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import { environment } from 'src/environments/environment';

export class WebSocketAPI {
    // endpoint del socket
    private webSocketEndPoint: string = environment.serverUrl + 'notifications';

    private topic: string = "/user/notification/item";
    private onReceivedCallback: (value: string) => void;
    client: StompJs.Client;
    
    constructor(private currentIdUser: number,
        onReceivedCallback: (value: string) => void) {
        this.onReceivedCallback = onReceivedCallback;
        console.log("Initialize WebSocket Connection");
        
        this.client = new StompJs.Client({
            webSocketFactory: () => new SockJS(this.webSocketEndPoint),
            debug: (msg: string) => console.log(msg)
        });

        this.client.onConnect = () => {
            this.client.subscribe(this.topic, (response) => {
              const text: string = JSON.parse(response.body).text;
              console.log('Got ' + text);
              this.onReceivedCallback(text);
            });
            if (this.client && this.client.connected) {
                let headers: StompHeaders = new StompHeaders();
                headers['id'] = `${this.currentIdUser}`;
                this.client.publish({destination: '/swns/start', headers, body: `${this.currentIdUser}`});
            }
            console.info('websocket connected ' + (<any>this.client.webSocket)._transport.url);
          };
    
          this.client.onStompError = (frame) => {
            console.error(frame.headers['message']);
            console.error('Details:', frame.body);
          };
    
          this.client.activate();
        
    }

    _disconnect() {
        if (this.client && this.client.connected) {
            this.client.publish({destination: '/swns/stop'});
            this.client.deactivate();
            this.client = null;
            console.log("socket disconnected");
        }
    }
}