import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NotificationsService } from 'src/app/services/notifications.service';
import { SessionService } from 'src/app/services/session.service';

@Component({
    selector: 'app-notifications-list',
    templateUrl: './notifications-list.component.html',
    styleUrls: ['./notifications-list.component.scss']
})
export class NotificationsListComponent implements OnInit {
    private idUser: number;
    showLabel: boolean = false;
    notifications: any[] = [];
    hideReadControl: FormControl = new FormControl("");

    constructor(private sessionService: SessionService,
        private notificationsService: NotificationsService) {
        this.idUser = this.sessionService.getCurrentUser().id;
    }

    ngOnInit() {
        this.notificationsService.get(this.idUser).subscribe(r => this.notifications = r);
    }

    getStyleText(item:any) {
        return !item.leida ? "font-weight:bold;" : "";
    }

    setAllRead() {
        console.log(this.notifications);
        this.notificationsService.setAllAsRead(this.idUser).subscribe(r => {
            if (r.ok) {
                this.showLabel = true;
                this.notifications.forEach(i => i.leida = true);
            }
        });
    }
}