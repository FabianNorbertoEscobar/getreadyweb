import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { interval, Subscription } from 'rxjs';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CounterComponent implements OnInit {

  @Input()
  public initialCount: number = 100;
  @Input()
  public diameter: number = 72;

  @Output()
  public timeout: EventEmitter<boolean> = new EventEmitter<boolean>();

  public currentCount: number;
  public intervalSubscription: Subscription;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this.currentCount = this.initialCount;
  }

  start() {
    this.intervalSubscription = interval(1000)
    .subscribe(second => {
      this.currentCount = this.initialCount - (second + 1);
      this.changeDetectorRef.detectChanges();

      if (this.currentCount == 0) {
        this.stop();
        this.timeout.emit(true);
      }
    });
  }

  reset() {
    this.stop();
    this.currentCount = this.initialCount;
    this.changeDetectorRef.detectChanges();
  }

  stop() {
    this.intervalSubscription.unsubscribe();
  }
}
