import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayTransparentComponent } from './overlay-transparent.component';

describe('OverlayTransparentComponent', () => {
  let component: OverlayTransparentComponent;
  let fixture: ComponentFixture<OverlayTransparentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlayTransparentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayTransparentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
