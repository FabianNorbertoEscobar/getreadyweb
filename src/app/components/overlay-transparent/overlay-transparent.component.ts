import { Component, OnInit } from '@angular/core';
import { OverlayService } from 'src/app/services/overlay.service';
import { OverlayData } from 'src/app/interfaces/overlay.interface';

@Component({
  selector: 'app-overlay-transparent',
  templateUrl: './overlay-transparent.component.html',
  styleUrls: ['./overlay-transparent.component.scss']
})
export class OverlayTransparentComponent implements OnInit {

  display: boolean = false;
  currentData: OverlayData;

  constructor(
    private overlayService: OverlayService,
  ) {}

  ngOnInit(): void {
    this.overlayService.transparent.subscribe((data: OverlayData) => {
      if (data) {
        this.currentData = data;
        this.display = true;
      } else {
        this.currentData = null;
        this.display = false;
      }
    });
  }

  hide() {
    this.display = false;
    this.currentData.componentRef[this.currentData.functionName]();
  }
}
