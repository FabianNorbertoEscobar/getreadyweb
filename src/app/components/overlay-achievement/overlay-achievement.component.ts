import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Achievement } from 'src/app/modules/module-achievements/utils/achievement.utils';
import { OverlayService } from 'src/app/services/overlay.service';

@Component({
  selector: 'app-overlay-achievement',
  templateUrl: './overlay-achievement.component.html',
  styleUrls: ['./overlay-achievement.component.scss']
})
export class OverlayAchievementComponent implements OnInit, OnDestroy {

  subscription: Subscription = null;
  display: boolean = false;
  achievement: Achievement;

  displayTimeoutHandler = null;
  hideTimeoutHandler = null;

  constructor(
    private overlayService: OverlayService,
  ) { }

  ngOnInit(): void {
    this.subscription = this.overlayService.achievement.subscribe((achievement: Achievement) => {
      if (achievement) {
        this.achievement = achievement;
        this.display = false;
        
        if (this.displayTimeoutHandler) {
          clearTimeout(this.displayTimeoutHandler);
        }

        this.displayTimeoutHandler = setTimeout(() => {
          this.display = true;
        }, 100);

        if (this.hideTimeoutHandler) {
          clearTimeout(this.hideTimeoutHandler);
        }
  
        this.hideTimeoutHandler = setTimeout(() => {
          this.display = false;
        }, 10000);
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
