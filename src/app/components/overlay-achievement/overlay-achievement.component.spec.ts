import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayAchievementComponent } from './overlay-achievement.component';

describe('OverlayAchievementComponent', () => {
  let component: OverlayAchievementComponent;
  let fixture: ComponentFixture<OverlayAchievementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlayAchievementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayAchievementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
