import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OverlayService } from 'src/app/services/overlay.service';

@Component({
  selector: 'app-overlay-confetti',
  templateUrl: './overlay-confetti.component.html',
  styleUrls: ['./overlay-confetti.component.scss']
})
export class OverlayConfettiComponent implements OnInit, OnDestroy {

  display: boolean;
  subscription: Subscription = null;

  displayTimeoutHandler = null;
  hideTimeoutHandler = null;

  constructor(
    private overlayService: OverlayService,
  ) { }

  ngOnInit(): void {
    this.subscription = this.overlayService.confetti.subscribe(display => {
      if (display) {
        this.display = false;

        if (this.displayTimeoutHandler) {
          clearTimeout(this.displayTimeoutHandler);
        }

        this.displayTimeoutHandler = setTimeout(() => {
          this.display = true;
        }, 100);

        if (this.hideTimeoutHandler) {
          clearTimeout(this.hideTimeoutHandler);
        }
  
        this.hideTimeoutHandler = setTimeout(() => {
          this.display = false;
        }, 10000);
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
