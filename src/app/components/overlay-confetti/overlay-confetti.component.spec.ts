import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayConfettiComponent } from './overlay-confetti.component';

describe('OverlayConfettiComponent', () => {
  let component: OverlayConfettiComponent;
  let fixture: ComponentFixture<OverlayConfettiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlayConfettiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayConfettiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
