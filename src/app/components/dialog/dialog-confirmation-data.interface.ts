export interface DialogConfirmationData {
    title: string;
    description: string;
}
