import { Component, OnInit, ViewChild, EventEmitter, Output, AfterViewInit } from '@angular/core';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { ChatbotInteractionService } from 'src/app/modules/module-chatbot/services/chatbot-interaction.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.service';
import { ColorThemeService, ColorTheme } from '../../services/theme.service';
import { DialogConfirmationComponent } from '../dialog/dialog-confirmation/dialog-confirmation.component';
import { SessionService } from 'src/app/services/session.service';
import { Router } from '@angular/router';
import { AchievementsSharedService } from 'src/app/services/achievements-shared.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @ViewChild('slideToggle', { static: true })
  slideToggle: MatSlideToggle;

  constructor(
    private chatbotService: ChatbotInteractionService,
    private dialog: MatDialog,
    private themeService: ColorThemeService,
    private authService: AuthService,
    private router: Router,
    private sessionService: SessionService,
    private achievementsSharedService: AchievementsSharedService,
  ) { }

  ngOnInit(): void {
    this.slideToggle.checked = true;
    this.themeService.setColorTheme(ColorTheme.APP_LIGHT);
  }

  /**
   * Cambia el esquema de colores entre claro y oscuro
   */
  toggleColorTheme() {
    this.themeService.setColorTheme(this.slideToggle.checked ? ColorTheme.APP_LIGHT : ColorTheme.APP_DARK);
    this.achievementsSharedService.grantAchievementToUser("Quien apago la luz?");
  }

  /**
   * Envía un mensaje de ayuda al chatbot
   */
  help() {
    this.chatbotService.toggleChatbot('¿Necesitas ayuda? Pregúntame lo que quieras');
  }

  /**
   * Cierra la sesión del usuario actual
   */
  signOut() {
    const dialogRef: MatDialogRef<DialogConfirmationComponent> = this.dialog.open(DialogConfirmationComponent, {
      data: {
        title: 'Cerrar sesión',
        description: 'Se cerrará la sesión ¿Desea continuar?'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.authService.logout();
      }
    });
  }

  /**
   * Abre el componente de cambio de contraseña
   */
  userChangePassword() {
    let user = this.sessionService.getCurrentUser();
    if (user) {
      this.router.navigate(['home/change-password'], { queryParams: { userId: user.id }});
    }
  }
}
