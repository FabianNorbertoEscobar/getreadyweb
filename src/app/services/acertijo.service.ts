import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable()
export class AcertijoService {

  constructor(private http: HttpService) { }

  public add(value: any): Observable<any> {
    return this.http.post('acertijo', value);
  }

  public update(id: number, value: any): Observable<any> {
    return this.http.put('acertijo/' + id, value);
  }

  public get(id: number): Observable<any> {
    return this.http.get('acertijo/' + id);
  }

  public finished(id: number, idUser: number, ok: boolean, score: number): Observable<any> {
    let body = {
      id: id,
      idUser: idUser,
      ok: ok,
      score: score
    }
    return this.http.post('acertijo/finished', body);
  }

  public quit(id: number, idUser: number) {
    let body = {
      id: id,
      idUser: idUser
    }
    return this.http.post('acertijo/quit', body);
  }

}
