import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable()
export class TriviasService {

  constructor(private http: HttpService) { }

  public add(value: any): Observable<any> {
    return this.http.post('trivia', value);
  }

  public update(id: number, value: any): Observable<any> {
    return this.http.put('trivia/' + id, value);
  }

  public get(id: number): Observable<any> {
    return this.http.get('trivia/' + id);
  }

  public finished(value: any): Observable<any> {
    return this.http.post('trivia/finished', value);
  }
}
