import { Injectable } from "@angular/core";
import { NavbarItem } from '../interfaces/navbar-item.interface';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { SessionService } from './session.service';

@Injectable({
    providedIn: 'root'
})
export class NavbarService {

    private navigationSource: BehaviorSubject<NavbarItem> = new BehaviorSubject<NavbarItem>(null);
    navigationEvent: Observable<NavbarItem> = this.navigationSource.asObservable();

    navbarItems: NavbarItem[] = [];

    constructor(private sessionService: SessionService) {  
    }

    /**
     * Devuelve los items de navegación del usuario actual 
     * @returns `NavbarItem[]`
     */
    getNavbarItems(): NavbarItem[] {
        if (this.navbarItems.length == 0) {
            let currentUser = this.sessionService.getCurrentUser();

            this.navbarItems.push({
                description: 'Perfil',
                icon: 'fas fa-user fa-lg fa-fw',
                route: 'home',
                checked: false
            });

            this.navbarItems.push({
                description: 'Organización',
                icon: 'fas fa-sitemap fa-lg fa-fw',
                route: 'organization',
                checked: false
            });

            if (currentUser.roleType == "ADMIN") {
                this.navbarItems.push({
                    description: 'Administrar sectores',
                    icon: 'fas fa-globe fa-lg fa-fw',
                    route: 'manage-users/sectors',
                    checked: false
                });
            } else {
                this.navbarItems.push({
                    description: 'Sector',
                    icon: 'fas fa-globe fa-lg fa-fw',
                    route: 'manage-users/sector',
                    checked: false
                });
            }

            if (currentUser.roleType != "USUARIO") {
                this.navbarItems.push({
                    description: 'Administrar usuarios',
                    icon: 'fas fa-users-cog fa-lg fa-fw',
                    route: 'manage-users',
                    checked: false
                });
                this.navbarItems.push({
                    description: 'Administrar competencias',
                    icon: 'fas fa-list-ul fa-lg fa-fw',
                    route: 'competencias',
                    checked: false,
                });
            }

            this.navbarItems.push({
                description: 'Cursos',
                icon: 'fas fa-graduation-cap fa-lg fa-fw',
                route: 'courses',
                checked: false
            });
            this.navbarItems.push({
                description: 'Logros',
                icon: 'fas fa-trophy fa-lg fa-fw',
                route: 'achievements',
                checked: false
            });
            this.navbarItems.push({
                description: 'Juegos',
                icon: 'fas fa-gamepad fa-lg fa-fw',
                route: 'games',
                checked: false
            });
            this.navbarItems.push({
                description: 'Evaluaciones de desempeño',
                icon: 'fas fa-calendar-check fa-lg fa-fw',
                route: 'appraisals',
                checked: false
            });
            this.navbarItems.push({
                description: 'Desarrollo',
                icon: 'fas fa-bug fa-lg fa-fw',
                route: 'development',
                checked: false
            });
            this.navbarItems.push({
                description: environment.version,
                icon: '',
                route: '',
                checked: false
            });
        }
        return this.navbarItems;
    }

    /**
     * Navega a un item de la barra de navegación por descripción
     * @param description la descripción del item
     */
    highlightByDescription(description: string): void {
        this.navigationSource.next(this.navbarItems.find(item => item.description == description));
    }

    /**
     * Navega a un item de la barra de navegación por ruta
     * @param description la ruta del item
     */
    highlightByRoute(route: string): void {
        this.navigationSource.next(this.navbarItems.find(item => item.route == route));
    }

    /**
     * Limpia la lista. Se utiliza para contemplar cambio de sesión.
     */
    cleanNavbar(): void {
        while (this.navbarItems.length > 0) { 
            this.navbarItems.pop(); 
        }
    } 
}
