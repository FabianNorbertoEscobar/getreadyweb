import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface FileS3 {
    fileUrl: string;
}

@Injectable()
export class S3Service {

    constructor(private http: HttpClient) {}

    /**
     * Sube un archivo al bucket de S3
     * @param thefile el archivo a subir
     * @param folderPath el path de la carpeta destino
     */
    upload(thefile: File, folderPath: string): Observable<any> {
        const formData: FormData = new FormData();
        formData.append('file', thefile);
        return this.http.post(`${environment.apiUrl}s3/upload/${folderPath}`, formData);
    }

    /**
     * Borra un archivo del bucket de S3
     * @param filePath el path del archivo
     */
    delete(filePath: string): Observable<any> {
        return this.http.delete(`${environment.apiUrl}s3/delete/${filePath}`);
    }
}