import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import {ProfileData} from '../modules/module-profile/ProfileData';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private baseUrl = 'profile/';
  constructor(private http: HttpService) {
  }

  public getProfileById(id: number): Observable<any> {
    return this.http.get(this.baseUrl + id);
  }

  public getProfileImageById(id: number): Observable<any> {
    return this.http.get(this.baseUrl + '/image/' + id);
  }

  public createProfile(profile: ProfileData): Observable<any> {
    return this.http.post(this.baseUrl, profile);
  }

  public updateProfile(profile: ProfileData): Observable<any> {
    return this.http.put(this.baseUrl, profile);
  }

  public deleteProfile(id: number): Observable<any> {
    return this.http.delete(this.baseUrl + id);
  }

}
