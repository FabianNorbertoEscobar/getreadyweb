import { Injectable } from '@angular/core';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {

  constructor(private sessionService: SessionService) { }

  public canCreateOrEditContent(): boolean {
    return this.sessionService.getCurrentUser()?.puedeEditar;
  }
}
