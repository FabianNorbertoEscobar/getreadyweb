import { Injectable } from '@angular/core';
import { Observable, ConnectableObservable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { publish, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  private warnError(response: any): void {
    if (response && response.ok != undefined && !response.ok) {
      console.warn(response.message);
    }
  }

  public post(urlAction: string, value: any, makeConnectable: boolean = false): Observable<any> | ConnectableObservable<any> {
    let obs = this.http.post(`${environment.apiUrl}${urlAction}`, value);
    if (makeConnectable) {
      obs = obs.pipe(publish()) as ConnectableObservable<any>;
      obs.subscribe(r => this.warnError(r));
      return obs;
    } else {
      return obs.pipe(tap(r => this.warnError(r)));
    }
  }

  public get<T>(urlAction: string): Observable<any> {
    return this.http.get<T>(`${environment.apiUrl}${urlAction}`);
  }

  public put(urlAction: string, body: any): Observable<any> {
    return this.http.put(`${environment.apiUrl}${urlAction}`, body);
  }

  public delete(urlAction: string): Observable<any> {
    return this.http.delete(`${environment.apiUrl}${urlAction}`);
  }
}
