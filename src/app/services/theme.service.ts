import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from 'rxjs';

export enum ColorTheme {
    APP_LIGHT = 'app-light-theme',
    APP_DARK = 'app-dark-theme',
}

@Injectable()
export class ColorThemeService {

    private themeSource: BehaviorSubject<string> = new BehaviorSubject<string>(ColorTheme.APP_LIGHT);
    themeEvent: Observable<string> = this.themeSource.asObservable();

    constructor(){}

    setColorTheme(theme: string) {
        this.themeSource.next(theme);
    }
}