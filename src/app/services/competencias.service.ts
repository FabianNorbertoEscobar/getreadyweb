import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { Competencia } from '../modules/module-competencias/competencia';

@Injectable({
  providedIn: 'root'
})
export class CompetenciasService {

  private baseUrl = 'competencia/';
  constructor(private http: HttpService) { 
  }

  public getCompetencias(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  public getCompetenciasByOrganizacion(id: number): Observable<any> {
    return this.http.get(this.baseUrl + id);
  }

  public agregar(competencia: Competencia): Observable<any> {
    return this.http.post(this.baseUrl, competencia);
  }

  public modificar(competencia: Competencia): Observable<any> {
    console.log(competencia);
    return this.http.put(this.baseUrl + competencia.id, competencia);
  }

  public eliminar(id: number): Observable<any> {
    return this.http.delete(this.baseUrl + id);
  }  

}
