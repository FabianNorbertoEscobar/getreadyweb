import { Injectable } from "@angular/core";
import { Router, NavigationEnd, RouterEvent } from '@angular/router';
import { HEADER_ROUTES } from './header-routes';
import { BehaviorSubject, Observable } from 'rxjs';

export interface HeaderRoute {
    route: string;
    description: string;
}

@Injectable({
    providedIn: 'root',
})
export class HeaderService {

    private currentTitleSource: BehaviorSubject<string> = new BehaviorSubject<string>("Perfil");
    private headerRoutes: HeaderRoute[] = HEADER_ROUTES;
    public currentTitle: Observable<string> = this.currentTitleSource.asObservable();

    constructor(
        private router: Router,
    ) {
        this.router.events.subscribe((event: RouterEvent) => {
            if (event instanceof NavigationEnd) {
                this.currentTitleSource.next(this.getDescriptionForRoute(event.url));
            }
        });
    }

    private getDescriptionForRoute(url: string): string {
        for (let i = 0 ; i < this.headerRoutes.length ; i++) {
            if (url.includes(this.headerRoutes[i].route)) {
            return this.headerRoutes[i].description;
            }
        }

        return '<description unavailable>';
    }

    public setDescriptionForRoute(description: string) {
        console.log(description);
        this.currentTitleSource.next(description);
    }
}
