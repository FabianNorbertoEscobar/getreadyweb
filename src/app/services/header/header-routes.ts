import { HeaderRoute } from './header.service';

export const HEADER_ROUTES: HeaderRoute[] = [
    {
        route: '/home/user',
        description: 'Editar perfil'
    },
    {
        route: '/home/change-password',
        description: 'Cambiar contraseña'
    },
    {
        route: '/home/notifications',
        description: 'Notificaciones'
    },
    {
        route: '/home',
        description: 'Perfil'
    },
    {
        route: '/organization',
        description: 'Organización'
    },
    {
        route: '/organization/edit',
        description: 'Configurar detalles de la organización'
    },
    {
        route: '/manage-users/sectors',
        description: 'Sectores'
    },
    {
        route: '/manage-users/sector',
        description: 'Sector'
    },
    {
        route: '/manage-users/edit',
        description: 'Editar usuario'
    },
    {
        route: '/manage-users/create',
        description: 'Crear usuario'
    },
    {
        route: '/manage-users',
        description: 'Usuarios'
    },
    {
        route: '/competencias/modificar',
        description: 'Modificar competencia'
    },
    {
        route: '/competencias/agregar',
        description: 'Crear competencia'
    },
    {
        route: '/competencias',
        description: 'Competencias'
    },
    {
        route: '/sections/new-section',
        description: 'Crear sección'
    },
    {
        route: '/sections',
        description: 'Secciones'
    },
    {
        route: '/new-course',
        description: 'Crear curso'
    },
    {
        route: '/courses/stats',
        description: 'Estadísticas cursos'
    },
    {
        route: '/courses',
        description: 'Cursos'
    },
    {
        route: '/achievements/custom-achievement',
        description: 'Logro personalizado'
    },
    {
        route: '/achievements',
        description: 'Logros'
    },
    {
        route: '/play/trivia',
        description: 'Jugando trivia'
    },
    {
        route: '/play/acertijo',
        description: 'Jugando acertijo'
    },
    {
        route: '/new-game/trivia?',
        description: 'Editar trivia'
    },
    {
        route: '/new-game/trivia',
        description: 'Crear trivia'
    },
    {
        route: '/new-game/acertijo',
        description: 'Crear acertijo'
    },
    {
        route: '/new-game',
        description: 'Crear juego'
    },
    {
        route: 'games/history',
        description: 'Histórico juegos'
    },
    {
        route: 'games/stats',
        description: 'Estadísticas juegos'
    },
    {
        route: '/games',
        description: 'Juegos'
    },
    {
        route: '/appraisals/statistics',
        description: 'Estadísticas evaluaciones'
    },
    {
        route: '/appraisals/new-appraisal',
        description: 'Crear evaluación'
    },
    {
        route: '/appraisals',
        description: 'Evaluaciones'
    },
    {
        route: '/development',
        description: 'Desarrollo'
    },
    {
        route: '/',
        description: 'Perfil'
    }
];