import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import {PraiseDTO, TotalPraiseDTO} from '../components/praise-buttons/praise.utils';

@Injectable({
  providedIn: 'root'
})
export class PraiseService{
  private baseUrl = 'praise/';
  constructor(private http: HttpService) {
  }

  public grantPraise(praise: PraiseDTO): Observable<any>{
    return this.http.post(this.baseUrl, praise);
  }

  public getPraiseAvailability(id: number): Observable<any>{
    return this.http.get(this.baseUrl + id);
  }

  getTotalPraise(id: number): Observable<TotalPraiseDTO> {
    return this.http.get(this.baseUrl + 'count/' + id);
  }
}
