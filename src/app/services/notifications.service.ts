import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { HttpService } from './http.service';

@Injectable({
    providedIn: 'root'
})
export class NotificationsService {
    private allRead: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public allRead$: Observable<boolean> = this.allRead.asObservable();

    constructor(private httpService: HttpService) {}

    getTop(idUser: number) {
        return this.httpService.get('notification/getTopNotifications/'+idUser);
    }

    get(idUser: number) {
        return this.httpService.get('notification/getNotifications/'+idUser);
    }

    setAsRead(idNotification: number) {
        return this.httpService.post('notification/setAsRead/'+idNotification, null);
    }

    setAllAsRead(idUser: number) {
        return this.httpService.post('notification/setAllAsRead/'+idUser, null).pipe(tap(r => {
            if (r.ok) {
                this.allRead.next(true);
            }
        }));
    }    
}
