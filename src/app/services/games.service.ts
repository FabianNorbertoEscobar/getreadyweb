import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';
import { GameStatsFromDB } from '../interfaces/game-stats-db.interface';

@Injectable()
export class GamesService {

  constructor(private http: HttpService) { }

  public getAll(idUser: number): Observable<any> {
    return this.http.get('games/' + idUser);
  }

  public getHistoric(): Observable<any> {
    return this.http.get('games/getallhistory');
  }

  public getStats(idSector: number): Observable<GameStatsFromDB> {
    return this.http.post('games/stats', { idSector });
  }
}
