import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Organization } from '../interfaces/organization.interface';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  private baseUrl = 'organizaciones/';
  constructor(private http: HttpService) { 
  }

  public get(id: number): Observable<any> {
    return this.http.get(this.baseUrl + id);
  }

  public edit(organization: Organization): Observable<any> {
    return this.http.put(this.baseUrl, organization);
  }
  
}
