import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Achievement } from '../modules/module-achievements/utils/achievement.utils';
import { AchievementsService } from '../modules/module-achievements/services/achievements.service';
import { OverlayService } from './overlay.service';

@Injectable({
    providedIn: 'root',
})
export class AchievementsSharedService {

    /** Todos los logros de la plataforma */
    achievements: BehaviorSubject<Achievement[]> = new BehaviorSubject<Achievement[]>([]);
    /** Los logros obtenidos por el usuario */
    userAchievements: BehaviorSubject<Achievement[]> = new BehaviorSubject<Achievement[]>([]);

    constructor(
        private achievementsService: AchievementsService,
        private overlayService: OverlayService,
    ) {}

    grantAchievementToUser(achievementTitle: string): void {
        let achievement: Achievement = this.getAchievementByTitle(achievementTitle);
        if (achievement && !this.userAchievements.value.find(_achievement => _achievement.id == achievement.id)) {
            this.achievementsService.grantAchievementToUser(achievement.id).subscribe(result => {
                if (result && result.ok) {
                  this.fetchUserAchievements().subscribe((achievements: Achievement[]) => {
                    this.overlayService.displayAchievement(achievement);
                  });
                }
            });
        }
    }

    getAchievementByTitle(title: string): Achievement {
        return this.achievements.value.find(achievement => achievement.title == title);
    }

    getAchievementById(id: number): Achievement {
        return this.achievements.value.find(achievement => achievement.id == id);
    }

    fetchAchievements(): Observable<Achievement[]> {
        return this.achievementsService.getAchievements().pipe(
            tap(
                (achievements: Achievement[]) => {
                    this.achievements.next(achievements.sort((a, b) => { return (new Date(a.date)).getTime()- (new Date(b.date)).getTime() }));
                }
            ),
        );
    }

    fetchUserAchievements(): Observable<Achievement[]> {
        return this.achievementsService.getUserAchievements().pipe(
            tap(
                (achievements: Achievement[]) => {
                    this.userAchievements.next(achievements.sort((a, b) => { return (new Date(a.date)).getTime()- (new Date(b.date)).getTime() }));
                }
            ),
        );
    }
}