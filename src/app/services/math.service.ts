import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class MathService {
    constructor() {}

    /**
     * Retorna el porcentaje relativo entre dos números, como un número del 0 al 100
     * @param number1 el primero número
     * @param number2 el segundo número
     */
    getPercentage(number1: number, number2: number): number {
        return Math.round((number2 * 100) / number1);
    }
}