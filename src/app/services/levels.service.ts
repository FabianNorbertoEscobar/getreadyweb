import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

export interface LevelUp {
    idUsuario: number;
    experiencia: number;
}

export interface CreateUserLevel {
    idUsuario: number;
}

@Injectable()
export class LevelsService {

    constructor(
        private httpService: HttpService,
    ) {}

    /**
     * Obtiene todos los niveles
     */
    getLevels(): Observable<any> {
        return this.httpService.get('nivel');
    }

    /**
     * Obtiene el nivel del usuario
     * @param userId el id del usuario
     */
    getUserLevel(userId: number): Observable<any> {
        return this.httpService.get(`usergetready/${userId}`)
    }

    /**
     * Crea el nivel del usuario
     * @param userId el id del usuario
     */
    createUserLevel(userId: number): Observable<any> {
        let payLoad: CreateUserLevel = {
            idUsuario: userId,
        }

        return this.httpService.post('usergetready', payLoad);
    }

    /**
     * Agrega experiencia al usuario
     * @param userId el id del usuario
     * @param experience la experiencia
     */
    levelUp(userId: number, experience: number) {
        let payload: LevelUp = {
            idUsuario: userId,
            experiencia: experience,
        }
        return this.httpService.post('usergetready/levelup', payload);
    }
}