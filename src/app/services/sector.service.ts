import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SectorService {

    constructor(private http: HttpService) { }

    findAllByUser(idUser: number): Observable<any> {
        return this.http.get('sector/findAllByUser/'+idUser);
    }

    getAll(): Observable<any>{
        return this.http.get('sector');
    }

    save(nombre: string, idUser: number): Observable<any> {
        let body = {
            nombre: nombre,
            idUser: idUser
        };
        return this.http.post('sector/', body);
    }

    edit(dto: any): Observable<any> {
        return this.http.put('sector/', dto);
    }

    delete(id: number): Observable<any> {
        return this.http.delete('sector/'+id);
    }

    get(id: number): Observable<any> {
        return this.http.get('sector/'+id);
    }

}
