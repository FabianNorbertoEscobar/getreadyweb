type RoleType = 'ADMIN' | 'ADMIN_SECTOR' | 'USUARIO';

/**
 * Usuario
 * Debería estar alineado con UserDTO.java
 */
export class User {
    public id: number;
    public roleType: RoleType;
    public puedeEditar: boolean;
    public id_sector: number;
    public idOrganizacion: number;
}
