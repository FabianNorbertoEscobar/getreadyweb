import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppraisalAverageEmployeeScores, AppraisalAverageSkillScores } from '../interfaces/appraisal-average-scores.interface';
import { AppraisalFromDB } from '../interfaces/appraisal-db.interface';
import { HttpService } from './http.service';

interface ErrorResponse {
  ok: boolean;
  message: string;
}

@Injectable({providedIn: 'root'})
export class AppraisalsService {
  allowedScore = [1, 2, 3, 4, 5];

  CREATED_APPRAISAL_STATUS_ID = 1;
  STARTED_APPRAISAL_STATUS_ID = 2;
  FINISHED_APPRAISAL_STATUS_ID = 3;

  constructor(
    private http: HttpService
  ) { }

  addAppraisal(appraisal: Omit<AppraisalFromDB, 'id' | 'fechaCreacion' | 'fechaEnCurso' | 'fechaFinalizacion'>) {
    return this.http.post('evaluacion', appraisal);
  }

  getAppraisalsEmployee(idEvaluado: number): Observable<Array<AppraisalFromDB>> {
    return this.http.get(`evaluacion/evaluado/${idEvaluado}`);
  }

  getAppraisalsEvaluator(idEvaluador: number): Observable<Array<AppraisalFromDB>> {
    return this.http.get(`evaluacion/evaluador/${idEvaluador}`);
  }

  getAppraisalsByOrganization(idOrganizacion: number): Observable<Array<AppraisalFromDB>> {
    return this.http.get(`evaluacion/organizacion/${idOrganizacion}`);
  }

  getAppraisal(appraisalId: number): Observable<AppraisalFromDB> {
    return this.http.get(`evaluacion/${appraisalId}`);
  }

  updateAppraisal(appraisal: AppraisalFromDB) {
    return this.http.put(`evaluacion/${appraisal.id}`, appraisal);
  }

  getAverageEmployeeScoresByEvaluator(idEvaluador: number): Observable<Array<AppraisalAverageEmployeeScores>> {
    return this.http.get(`promedio/evaluado/evaluador/${idEvaluador}`);
  }

  getAverageSkillScoresByEvaluator(idEvaluador: number): Observable<Array<AppraisalAverageSkillScores>> {
    return this.http.get(`promedio/competencia/evaluador/${idEvaluador}`);
  }

  getAverageEmployeeScoresByOrganization(idOrganizacion: number): Observable<Array<AppraisalAverageEmployeeScores>> {
    return this.http.get(`promedio/evaluado/organizacion/${idOrganizacion}`);
  }

  getAverageSkillScoresByOrganization(idOrganizacion: number): Observable<Array<AppraisalAverageSkillScores>> {
    return this.http.get(`promedio/competencia/organizacion/${idOrganizacion}`);
  }
}
