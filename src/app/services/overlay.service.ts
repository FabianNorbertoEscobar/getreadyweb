import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Achievement } from '../modules/module-achievements/utils/achievement.utils';
import { OverlayData } from '../interfaces/overlay.interface';

@Injectable({
    providedIn: "root",
})
export class OverlayService {
    private transparentSource: BehaviorSubject<OverlayData> = new BehaviorSubject<OverlayData>(null);
    transparent: Observable<OverlayData> = this.transparentSource.asObservable();

    private loadingSource: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    loading: Observable<boolean> = this.loadingSource.asObservable();

    private confettiSource: Subject<boolean> = new Subject<boolean>();
    confetti: Observable<boolean> = this.confettiSource.asObservable();

    private achievementSource: Subject<Achievement> = new Subject<Achievement>();
    achievement: Observable<Achievement> = this.achievementSource.asObservable();
  
    constructor() {}

    displayTransparentOverlay(componentRef: any, functionName: string): void {
        this.transparentSource.next({
            componentRef: componentRef,
            functionName: functionName,
        });
    }

    hideTransparentOverlay(): void {
        this.transparentSource.next(null);
    }

    displayLoadingOverlay(): void {
        this.loadingSource.next(true);
    }

    hideLoadingOverlay(): void {
        this.loadingSource.next(false);
    }

    displayConfettiOverlay(): void {
        this.confettiSource.next(true);
    }

    displayAchievement(achievement: Achievement): void {
        this.achievementSource.next(achievement);
        this.displayConfettiOverlay();
    }
}