import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './http.service';
import { map, withLatestFrom, tap } from 'rxjs/operators';
import { Section } from '../interfaces/sections.interface';

@Injectable({providedIn: 'root'})
export class SectionsService {
  constructor(
    private http: HttpService
  ) { }
  sections = new BehaviorSubject<Array<Section>>([]);
  sections$ = this.sections.asObservable();
  currentSectionIndex = new BehaviorSubject<number>(0);
  currentSectionIndex$ = this.currentSectionIndex.asObservable();
  currentSection = new BehaviorSubject<Section>(null);
  currentSection$ = this.currentSection.asObservable();

  isThereNext$ = this.currentSectionIndex$.pipe(
    withLatestFrom(this.sections$),
    map(([currentSectionIndex, sections]) => currentSectionIndex < sections.length - 1)
  );

  isTherePrev$ = this.currentSectionIndex$.pipe(
    map((currentSectionIndex) => currentSectionIndex > 0)
  );

  getAllSections() {
    return this.http.get('seccion');
  }

  getSectionsByCourse(courseId: number): Observable<Array<any>> {
    return this.http.get(`seccion/curso/${courseId}`).pipe(
      tap(sections => this.sections.next(sections))
    );
  }

  getSection(sectionId: number) {
    return this.http.get(`seccion/${sectionId}`);
  }

  addSectionToCourse(section: Omit<Section, 'id'>) {
    return this.http.post('seccion', section);
  }

  updateSection(section: Omit<Section, 'id'>) {
    return this.http.put('seccion', section);
  }

  deleteSection(id: number) {
    return this.http.delete('seccion/'+id);
  }

  selectSection(section: Section, sectionIndex: number) {
    this.currentSection.next(section);
    this.currentSectionIndex.next(sectionIndex);
  }

  changeSection(offset: number) {
    this.currentSectionIndex.next(this.currentSectionIndex.value + offset);
    this.currentSection.next(this.sections.value[this.currentSectionIndex.value]);
  }
}
