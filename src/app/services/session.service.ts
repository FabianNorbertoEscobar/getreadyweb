import { Injectable } from '@angular/core';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private readonly userKey = 'user';
  private readonly authenticatedKey = 'authenticated';

  constructor() { }

  public getCurrentUser(): User {
    return JSON.parse(localStorage.getItem(this.userKey));
  }

  public setCurrentUser(user: User) {
    if (user === undefined) {
      localStorage.removeItem(this.userKey);
    }
    localStorage.setItem(this.userKey, JSON.stringify(user));
  }

  public getAuthenticated(): boolean {
    return !!localStorage.getItem(this.authenticatedKey);
  }

  public setAuthenticated(value: string): void {
    if (value === undefined) {
      localStorage.removeItem(this.authenticatedKey);
    }
    localStorage.setItem(this.authenticatedKey, 'true');
  }
}
