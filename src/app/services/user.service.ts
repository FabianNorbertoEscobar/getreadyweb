import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';
import { User } from '../modules/module-manage-users/UserDatabase';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseUrl = 'user/';
  constructor(private http: HttpService) { 
  }

  public getUsers(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  public getUserById(id: number): Observable<any> {
    return this.http.get(this.baseUrl + id);
  }

  public createUser(user: User): Observable<any> {
    return this.http.post(this.baseUrl, user);
  }

  public updateUser(user: User): Observable<any> {
    return this.http.put(this.baseUrl + user.id, user);
  }

  public deleteUser(id: number): Observable<any> {
    return this.http.delete(this.baseUrl+id);
  }  

  public changePassword(data: any): Observable<any> {
    return this.http.post(this.baseUrl+'changePassword', data);
  }
}
