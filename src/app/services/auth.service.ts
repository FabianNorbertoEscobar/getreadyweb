import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ConnectableObservable, of, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { User } from './user';
import { HttpService } from './http.service';
import { NavbarService } from './navbar.service';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  constructor(
    private router: Router,
    private http: HttpService,
    private navbarService: NavbarService,
    private sessionService: SessionService
  ) { }

  public checkAuthenticated(): boolean {
    return this.sessionService.getAuthenticated() && this.sessionService.getCurrentUser() != null;
  }

  /**
   * Llama al server para hacer las validaciones de login
   * @param username 
   * @param password 
   * @returns observable para suscribirse y hacerle connect cuando este todo listo
   */
  public login(username: string, password: string): ConnectableObservable<any> {
    const data = { username, password }    
    let obs = this.http.post('auth/login', data, true) as ConnectableObservable<any>;
    obs.pipe(take(1)).subscribe(response => {
      if (response.validCredentials) {
        this.sessionService.setAuthenticated('true');
        this.sessionService.setCurrentUser(response.user);
        this.router.navigate(['']);  
      }
    });
    return obs;
  }

  public signUp(value: any): Observable<any> {
    return this.http.post('auth/signUp', value);
  }

  public getCurrentUser(): User {
    return this.sessionService.getCurrentUser();
  }

  public resetPassword(value: string): Observable<any> {
    return this.http.post('auth/resetPassword', value);
  }

  public logout(): void {
    this.sessionService.setAuthenticated(null);
    this.sessionService.setCurrentUser(null);
    this.navbarService.cleanNavbar();

    this.router.navigate(['/sign/in']);
    console.log(!!localStorage.getItem('authenticated'));
  }

}
