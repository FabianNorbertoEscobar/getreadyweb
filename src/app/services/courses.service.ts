import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

export interface DatabaseCourse {
  id: number;
  nombre: string;
  descripcion: string;
  idSector: number;
  progreso: number;
  dioFeedback: boolean;
}

@Injectable({providedIn: 'root'})
export class CoursesService {
  
  constructor(
    private http: HttpService
  ) { }

  getCourses(): Observable<Array<DatabaseCourse>> {
    return this.http.get('curso');
  }

  findByUser(idUser: number): Observable<Array<DatabaseCourse>> {
    return this.http.get('curso/user/'+idUser);
  }

  getCourse(courseId: number) {
    return this.http.get(`curso/${courseId}`);
  }

  addCourse(course: Omit<DatabaseCourse, 'id'>) {
    return this.http.post('curso', course);
  }

  updateCourse(course: Omit<DatabaseCourse, 'id'>) {
    return this.http.put('curso', course);
  }

  saveProgress(body: any) {
    return this.http.post('curso/save', body);
  }

  giveFeedback(body: any) {
    return this.http.post('curso/give-feedback', body);
  }

  getFeedback(id: number) {
    return this.http.get('curso/feedback/'+id);
  }

  getFeedbackUser(idCurso: number, idUsuario: number) {
    return this.http.get('curso/feedback/'+idCurso+'/'+idUsuario);
  }

  getStats() {
    return this.http.get('curso/stats');
  }
}
