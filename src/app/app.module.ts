import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule, DatePipe } from '@angular/common';
import { MaterialModule } from './modules/shared/material.module';
import { HttpClientModule } from '@angular/common/http';
import { OverlayLoadingComponent } from './components/overlay-loading/overlay-loading.component';
import { ColorThemeService } from './services/theme.service';
import { AchievementsService } from './modules/module-achievements/services/achievements.service';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
    OverlayLoadingComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CommonModule,
    MaterialModule,
    HttpClientModule,
    ChartsModule
  ],
  providers: [
    ColorThemeService,
    AchievementsService,
    DatePipe,
  ],
  bootstrap: [
    AppComponent
  ],
})
export class AppModule { }
