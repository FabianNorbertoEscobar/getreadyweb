export interface Organization {
    id: number,
    nombre: string,
    descripcion: string,
    tipo: string,
    especialidad: string,
    financiacion: string,
    cantidadAproxEmpleados: string,
    fechaRegistro: Date,
    fundacion: number,
    ubicacion: string, 
    website: string,
    email: string,
    linkedin: string,
    facebook: string,
    twitter: string,
    instagram: string,
    telefono: string
}
