export interface UsuarioFromDB {
  id: number;
  username: string;
  nombre: string;
  apellido: string;
  email: string;
  password: string;
  rol: {
    id: string;
    tipo: string;
  };
  puedeEditar: boolean;
  sector: {
    id: number;
    nombre: string;
    organizacion: any;
  };
  organizacion: {
    cantidadAproxEmpleados: string;
    fechaRegistro: Date;
    id: number
    nombre: string;
  };
}
