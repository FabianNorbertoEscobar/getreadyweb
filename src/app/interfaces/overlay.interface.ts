/**
 * Estructura usada para los servicios compartidos de overlay
 */
export interface OverlayData {
    componentRef: any,
    functionName: string,
}