export interface Course {
    id?: number;
    title: string;
    progress: number;
    description?: string;
    idSector?: number;
    rated?: boolean;
}
