interface UserScore {
  username: string;
  maxScore: number;
}

export interface TriviaStats {
  nombre: string;
  idSector: number;
  userScores: UserScore[];
}

interface AcertijoStats {
  nombre: string;
  idSector: number;
}

interface MostPlayedTrivia {
  nombre: string;
  vecesJugada: number;
}

interface MostPlayedAcertijo {
  nombre: string;
  vecesJugado: number;
}

export interface GameStatsFromDB {
  trivias: Array<TriviaStats>;
  acertijos: Array<AcertijoStats>;
  triviaMasJugada: MostPlayedTrivia;
  acertijoMasJugado: MostPlayedAcertijo;
}
