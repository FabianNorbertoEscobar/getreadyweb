import { CompetenciaFromDB } from './competencia-db.interface';
import { UsuarioFromDB } from './usuario-db.interface';

export interface AppraisalAverageEmployeeScores {
  evaluado: UsuarioFromDB;
  promedio: number;
}

export interface AppraisalAverageSkillScores {
  competencia: CompetenciaFromDB;
  promedio: number;
}
