export interface Section {
  id: number;
  nombre: string;
  descripcion: string;
  texto: string;
  orden: number;
  idCurso: number;
  videoUrls: Array<string>;
}
