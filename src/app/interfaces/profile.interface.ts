import { Course } from './course.interface';

/**
 * Esta interfaz representa el perfil de un usuario
 */
export interface Profile {
    id: number;
    username: string;
    displayname: string;
}

/**
 * Usuario con nivel
 */
export interface UserGetReady {
    idUsuario: number;
    experiencia: number;
    nivel: Nivel;
}

/**
 * Esta interfaz representa el nivel del usuario
 */
export interface Nivel {
    id: number;
    nivel: number;
    experiencia: number;
    rango: string;
    mensaje: string;
}

/**
 * Perfil de usuario
 */
export interface UserProfile {
    id: number;
    address: string;
    description: string;
    displayname: string;
    email: string;
    image: string;
    phone: string;
    username: string;
}