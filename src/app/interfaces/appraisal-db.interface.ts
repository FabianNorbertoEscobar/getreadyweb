import { AppraisalStateFromDB } from './appraisal-state-db.interface';
import { CompetenciaFromDB } from './competencia-db.interface';
import { UsuarioFromDB } from './usuario-db.interface';

export interface AppraisalLine {
  id?: number;
  idEvaluacion?: number;
  idCompetencia?: number;
  competencia?: CompetenciaFromDB;
  observaciones?: string;
  puntajeEvaluador?: number;
  puntajeEvaluado?: number;
  puntajeFinal?: number;
  feedback?: string;
}

export interface AppraisalFromDB {
  id: number;
  idEvaluador: number;
  idEvaluado: number;
  evaluador?: UsuarioFromDB;
  evaluado?: UsuarioFromDB;
  idEstado: number;
  estado?: AppraisalStateFromDB;
  fechaTentativa: Date;
  fechaCreacion: Date;
  fechaEnCurso: Date;
  fechaFinalizacion: Date;
  lineasEvaluacion: Array<AppraisalLine>;
}
