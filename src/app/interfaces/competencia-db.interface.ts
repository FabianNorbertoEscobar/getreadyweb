export interface CompetenciaFromDB {
  id: number;
  nombre: string;
  descripcion: string;
  idOrganizacion: number;
}
