/**
 * Esta interfaz representa un item de la barra de navegación
 */
export interface NavbarItem {
  description: string;
  icon: string;
  route: string;
  checked: boolean;
}