import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  {
    path: 'sign',
    loadChildren: () => import('./modules/module-sign/sign.module').then(m => m.SignModule),
  },
  {
    path: '',
    loadChildren: () => import('./modules/global/global.module').then(m => m.GlobalModule),
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
