import { Component, HostBinding, OnInit, OnDestroy } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import { NavbarService } from './services/navbar.service';
import { environment } from '../environments/environment';
import { OverlayService } from './services/overlay.service';
import { Subscription } from 'rxjs';
import { ColorThemeService, ColorTheme } from './services/theme.service';
import { Router, ActivatedRoute, RouterEvent, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  environment = environment;

  @HostBinding('class')
  componentClass: string;

  cdkOverlayRefs: HTMLCollectionOf<Element>;

  title = 'GetReadyWeb';

  subscription: Subscription;

  constructor(
    private containerOverlay: OverlayContainer,
    private navbarService: NavbarService,
    private overlayService: OverlayService,
    private themeService: ColorThemeService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.router.events.subscribe((event: RouterEvent) => {
      if (event instanceof NavigationEnd) {
        this.navbarService.highlightByRoute(event.url.replace('/', ''));
      }
    });
    
    this.subscription = this.overlayService.loading.subscribe(loading => {
      if (loading) {
        document.body.style.overflowY = 'hidden';
      } else {
        document.body.style.overflowY = 'auto';
      }
    });
    
    this.themeService.themeEvent.subscribe(theme => {
      this.setAppTheme(theme);
    });
  }

  setAppTheme($event: string) {
    this.cdkOverlayRefs = document.getElementsByClassName('cdk-overlay-container');

    for (let i = 0 ; i < this.cdkOverlayRefs.length ; i++) {
      this.cdkOverlayRefs.item(i).classList.remove(ColorTheme.APP_LIGHT);
      this.cdkOverlayRefs.item(i).classList.remove(ColorTheme.APP_DARK);
      this.cdkOverlayRefs.item(i).classList.add($event);
    }

    this.containerOverlay.getContainerElement().classList.add($event);
    this.componentClass = $event;
  }

  addComponentClass(klass: string): void {
    if (this.componentClass) {
      this.componentClass += ` ${klass}`;
    } else {
      this.componentClass = klass;
    }
  }

  removeComponentClass(klass: string): void {
    if (this.componentClass) {
      this.componentClass.replace(` ${klass}`, '');
      this.componentClass.replace(`${klass}`, '');
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
