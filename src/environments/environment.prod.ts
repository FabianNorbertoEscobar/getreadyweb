export const environment = {
  version: 'v1.0.0',
  production: true,
  serverUrl: 'https://get-ready-services.herokuapp.com/',
  apiUrl: 'https://get-ready-services.herokuapp.com/api/',
  appUrl: 'https://get-ready-web.herokuapp.com/',
  s3Url: 'https://get-ready-dev.s3-sa-east-1.amazonaws.com/',
};