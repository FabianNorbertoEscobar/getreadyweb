export const environment = {
  version: 'v1.0.0',
  production: false,
  serverUrl: 'http://localhost:4300/',
  apiUrl: 'http://localhost:4300/api/',
  appUrl: 'http://localhost:4200/',
  s3Url: 'https://get-ready-dev.s3-sa-east-1.amazonaws.com/',
};
